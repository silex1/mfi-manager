#!/bin/sh

### User Setting
# Device descriptor Setting
bcd_usb=0x0300 
id_vendor=0x1A98
id_product=0x2081
bcd_device=0x0001
language_id=0x0409
sManufacture="Leica Camera AG"
sProduct="LEICA M11"
sSerialnumber="01234567890123"

# Configuration descriptor Setting
sConfiguration="Config MTP"
bm_attributes=0xC0  # self power
max_power=250

### Configurate MTP Function

### Insert kernel modules
KERNEL_VERSION_CMD='uname -r'
KERNEL_VERSION=$($KERNEL_VERSION_CMD)
echo Linux kernel version = "$KERNEL_VERSION"

insmod /lib/modules/"$KERNEL_VERSION"/kernel/drivers/usb/dwc3/dwc3-sn.ko hostmode=0 || true

# Mounting USB Gadget ConfigFS
modprobe libcomposite

if [ ! -d /sys/kernel/config/usb_gadget ] ; then
mount -t configfs none /sys/kernel/config/
fi
cd /sys/kernel/config/usb_gadget/
mkdir mtp
cd mtp

# Create Gadget Device Instance
echo ${bcd_usb} > bcdUSB
echo ${id_vendor} > idVendor
echo ${id_product} > idProduct
echo ${bcd_device} > bcdDevice 
mkdir -p strings/${language_id}
echo ${sManufacture} > strings/${language_id}/manufacturer 
echo ${sProduct} > strings/${language_id}/product 
echo ${sSerialnumber} > strings/${language_id}/serialnumber

# Create Function Instance
mkdir -p functions/mtp.usb0

# Create Configuration Instance
mkdir -p configs/c.1/strings/${language_id}
echo ${sConfiguration} > configs/c.1/strings/${language_id}/configuration 
echo ${bm_attributes} > configs/c.1/bmAttributes
echo ${max_power} > configs/c.1/MaxPower

# Bind Function Instance to Configuration Instance
ln -s functions/mtp.usb0 configs/c.1/

# Available UDC drivers
echo 19c00000.dwc3 > UDC

mkdir -p /dev/usb-mtp/iap2
mount -t functionfs usb0 /dev/usb-mtp/iap2

# Write file signalise end of script.
if [ ! -d /tmp/lc_startup ] ; then
mkdir /tmp/lc_startup
fi
echo "Started" > /tmp/lc_startup/mtp_usb_config

echo "mtp_usb_config: Done"

