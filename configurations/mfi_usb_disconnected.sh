#!/bin/sh
# this file is used to load wifi module and enable it

#
# shrivel.sh
# Copyright 2017 Leica Camera AG 
# Salahaldeen Altous <Salahaldeen.altous@leica-camera.com>

echo '' > /sys/kernel/config/usb_gadget/mtp/UDC
rm -r /sys/kernel/config/usb_gadget/mtp
rmmod usb_f_mtp
rmmod libcomposite
lsmod

#unload the usb3 driver in device mode
rmmod dwc3-sn.ko 

echo "mtp_usb_disconnected: Done"
