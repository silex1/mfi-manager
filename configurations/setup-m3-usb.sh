#!/bin/sh

set -xe

### Insert kernel modules
KERNEL_VERSION_CMD='uname -r'
KERNEL_VERSION=$($KERNEL_VERSION_CMD)
echo Linux kernel version = "$KERNEL_VERSION"

insmod /lib/modules/"$KERNEL_VERSION"/kernel/drivers/usb/dwc3/dwc3-sn.ko \
 	hostmode=0 || true

modprobe libcomposite

mount -t configfs none /sys/kernel/config

cd /sys/kernel/config/usb_gadget
mkdir g1
cd g1

echo 0x05c6 > idVendor
echo 0x901d > idProduct

(
mkdir strings/0x409
cd strings/0x409
echo 'gcx' > manufacturer
echo 'iap test' > product
echo 1234 > serialnumber
)

(
cd functions
mkdir ffs.1
cd ..
)

(
cd configs
mkdir c.1
cd c.1/strings/
mkdir 0x409
cd 0x409
echo 'default config' > configuration
)

ln -s functions/ffs.1 configs/c.1

mkdir -p /dev/usb-ffs/iap2
mount -t functionfs 1 /dev/usb-ffs/iap2