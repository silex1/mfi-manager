# This is a hackish way of preparing an USB gadget on a OrangePi Zero such that
# the USB demo could be run and the paths match the default configuration on
# the Qualcom devboard. This will probably also work on other platforms as
# well.

set -xe

### User Setting
# Device descriptor Setting
bcd_usb=0x0310 
id_vendor=0x1A98
id_product=0x901d
bcd_device=0x0001
language_id=0x0409
sManufacture="Leica-Camera AG"
sProduct="Leica M11"
sSerialnumber="01234567890123"

# Configuration descriptor Setting
sConfiguration="Config iAP2"
bm_attributes=0xC0  # self power
max_power=250


### Insert kernel modules
KERNEL_VERSION_CMD='uname -r'
KERNEL_VERSION=$($KERNEL_VERSION_CMD)
echo Linux kernel version = "$KERNEL_VERSION"

insmod /lib/modules/"$KERNEL_VERSION"/kernel/drivers/usb/dwc3/dwc3-sn.ko hostmode=0 || true

#modprobe libcomposite

### Add Gadget
#if [ ! -d /sys/kernel/config/usb_gadget ] ; then
#mount -t configfs none /sys/kernel/config/
#fi

cd /sys/kernel/config/usb_gadget
#rmdir lc_iap2 || true
#mkdir lc_iap2
cd lc_iap2

# Create Gadget Device Instance
#echo ${bcd_usb} > bcdUSB
#echo ${id_vendor} > idVendor
#echo ${id_product} > idProduct
#echo ${bcd_device} > bcdDevice 
#mkdir -p strings/${language_id}
#echo ${sManufacture} > strings/${language_id}/manufacturer 
#echo ${sProduct} > strings/${language_id}/product 
#echo ${sSerialnumber} > strings/${language_id}/serialnumber

# Create Function Instance
mkdir -p functions/mtp.usb0

# Create Configuration Instance
mkdir -p configs/c.2/strings/${language_id}
echo ${sConfiguration} > configs/c.2/strings/${language_id}/configuration 
echo ${bm_attributes} > configs/c.2/bmAttributes
echo ${max_power} > configs/c.2/MaxPower

# Bind Function Instance to Configuration Instance
ln -s functions/mtp.usb0 configs/c.2

# SNI:
# Available UDC drivers
#echo 19c00000.dwc3 > UDC

#mkdir -p /dev/usb-ffs/mtp
#mount -t functionfs mtp /dev/usb-ffs/mtp
