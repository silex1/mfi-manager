# Start mfi demo-linux program for tests.

set -xe

demo-linux --auth=/dev/i2c-0 --bt-client=34:E2:FD:BE:DB:F0 --usb-ffs=/dev/usb-ffs/iap2 --usb-udc-path=/sys/kernel/config/usb_gadget/lc_iap2/UDC --usb-udc-name=19c00000.dwc3