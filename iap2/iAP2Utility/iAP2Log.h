/*
 *	File: iAP2Log.h
 *	Package: iAP2Utility
 *	Abstract: n/a 
 *
 *	Disclaimer: IMPORTANT: This Apple software is supplied to you, by Apple
 * 	Inc. ("Apple"), in your capacity as a current, and in good standing,
 *	Licensee in the MFi Licensing Program. Use of this Apple software is
 *	governed by and subject to the terms and conditions of your MFi License,
 *	including, but not limited to, the restrictions specified in the provision
 *	entitled “Public Software”, and is further subject to your agreement to
 *	the following additional terms, and your agreement that the use,
 *	installation, modification or redistribution of this Apple software
 * 	constitutes acceptance of these additional terms. If you do not agree with
 * 	these additional terms, please do not use, install, modify or redistribute
 *	this Apple software.
 *
 *	In consideration of your agreement to abide by the following terms, and
 *	subject to these terms, Apple grants you a personal, non-exclusive
 *	license, under Apple's copyrights in this original Apple software (the
 *	"Apple Software"), to use, reproduce, and modify the Apple Software in
 *	source form, and to use, reproduce, modify, and redistribute the Apple
 *	Software, with or without modifications, in binary form. While you may not
 *	redistribute the Apple Software in source form, should you redistribute
 *	the Apple Software in binary form, in its entirety and without
 *	modifications, you must retain this notice and the following text and
 *	disclaimers in all such redistributions of the Apple Software. Neither the
 *	name, trademarks, service marks, or logos of Apple Inc. may be used to
 *	endorse or promote products derived from the Apple Software without
 *	specific prior written permission from Apple. Except as expressly stated
 *	in this notice, no other rights or licenses, express or implied, are
 *	granted by Apple herein, including but not limited to any patent rights
 *	that may be infringed by your derivative works or by other works in which
 *	the Apple Software may be incorporated.
 *	
 *	The Apple Software is provided by Apple on an "AS IS" basis. APPLE MAKES
 *	NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *	IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A
 *	PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND OPERATION
 *	ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 *
 *	IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL OR
 *	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *	SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *	INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 *	MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED AND
 *	WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT
 *	LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE POSSIBILITY
 *	OF SUCH DAMAGE.
 *
 *	Copyright (C) 2012 Apple Inc. All Rights Reserved.
 *
 */

#ifndef iAP2Utility_iAP2Log_h
#define iAP2Utility_iAP2Log_h

#include <stdarg.h>
#include <leica/leica_types.h>
#include <leica/hsk_debug_types.h>
#include <leica/hsk_debug_linux_if.h>


#ifdef __cplusplus
extern "C" {
#endif

typedef enum
{
    kiAP2LogTypeError = 0,
    kiAP2LogTypeLog,
    kiAP2LogTypeLogDbg,
    kiAP2LogTypeData,

    kiAP2LogTypeCount

} iAP2LogType_t;

static inline E_LEVEL iAP2LogType2MFiLogLevel(iAP2LogType_t type)
{
    switch (type)
    {
        case kiAP2LogTypeError:     return E_LEVEL_ERROR;
        case kiAP2LogTypeLog:       return E_LEVEL_INFO;
        case kiAP2LogTypeLogDbg:    return E_LEVEL_DEBUG;
        case kiAP2LogTypeData:      return E_LEVEL_DEBUG;
        default:                    return E_LEVEL_WARNING;
    }
    return E_LEVEL_WARNING;
}

static inline char iAP2LogDataChar (char byte)
{
    if (byte >= 0x20 && byte < 0x7f)
    {
        return byte;
    }
    return '.';
}

static inline void iAP2LogEnable (iAP2LogType_t type) { }
static inline void iAP2LogDisable (iAP2LogType_t type) { }

#define iAP2LogType(type, format, ...) hsk_debug_internal_print(iAP2LogType2MFiLogLevel(type), format, ##__VA_ARGS__)
#define iAP2LogTypeRaw(type, path, func, line, format, ...) hsk_debug_internal_print(iAP2LogType2MFiLogLevel(type), format, ##__VA_ARGS__)
#define iAP2LogTypeNL iAP2LogType

#define iAP2Log(format, ...) iAP2LogType(kiAP2LogTypeLog, format, ##__VA_ARGS__)
#define iAP2LogNL iAP2Log

#define iAP2LogDbg(format, ...) iAP2LogType(kiAP2LogTypeLogDbg, format, ##__VA_ARGS__)
#define iAP2LogDbgNL iAP2LogDbg

#define iAP2LogError(format, ...) iAP2LogType(kiAP2LogTypeError, format, ##__VA_ARGS__)
#define iAP2LogErrorNL iAP2LogError

#define iAP2LogStart()
#define iAP2LogStop()

#define iAP2LogPrintData(data, dataLen, tag, format, ...)       ///< TODO JW HIGH: Mit Leben fuellen
//#define iAP2LogPrintData(data, dataLen, tag, format, ...) do { \
//    iAP2LogType(kiAP2LogTypeData, format, ##__VA_ARGS__); \
//    _hexdump(data, dataLen, __FILE__, __func__, __LINE__); \
//} while(0)
#define iAP2LogPrintDataNL iAP2LogPrintData

#ifdef __cplusplus
}
#endif

#endif
