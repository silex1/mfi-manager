/**
 *		@ingroup	mfi-manager
 *		@brief		Base for MFI Communication
 *		@details	This class is singelton
 * 
 *		@file		Mfi.h
 *		@date		Dec 1, 2020
 *		@author 	D. Borsdorf [borsdorfdo]
 *		@version	V0.1
 *
 *		@b	Copyright:
 *			Leica Camera AG (Wetzlar)
 *
 *		@b	Change history:
 *
 *		- Dec 1, 2020	D. Borsdorf		File created
 **/

#pragma once

#include "MfiHandle.h"

class Mfi
{
public:
    static Mfi* Instance();
    ~Mfi();

    bool start();
    bool terminate();
    bool isReady()
      { return m_isReady; }
    void setConfiguredCallback(void (*callback)(void))
      { cb_configured = callback; }

    pthread_attr_t m_RunThreadAttr;     ///< Attributes for the read request thread. Used to detect thread state.
    pthread_t m_RunThread;              ///< Thread handle.
    pthread_mutex_t m_RunMutex;         ///< Mutex to re-enter reading on interface.
    pthread_cond_t m_RunCondition;      ///< Condition to re-enter reading on interface.

private:
    static Mfi* _instance;
    static MfiHandle m_mfiHandle;
    static bool m_stop;
    static bool m_isReady;

    static char* m_authDev;
    static char* m_udcName;
    static char* m_udcPath;
    static char* m_ffsDir;
    static char* m_btClientAddress;
    static struct iap2_accessory *m_acc;

    static void (*cb_configured)(void);

    Mfi();
    Mfi( const Mfi& );

    void setDefaults();
    static void configured();
    static void* run(void *_parameter);
    static int mfiRun();
    static void newLinkInit(struct iap2_link *link);
    static ssize_t recv_sync(int sockfd, void *buf, size_t len, int flags);

    class InstanceGuard
    {
    public:
       ~InstanceGuard()
       {
          if( NULL != Mfi::_instance )
          {
             delete Mfi::_instance;
             Mfi::_instance = NULL;
          }
       }
    };
};

