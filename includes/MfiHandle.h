/**
 *		@ingroup	mfi-manager
 *		@brief		This class handles the mfi communication abstact
 *		@details	We have a thread running to make sure async handling of communication
 * 
 *		@file		MfiHandle.h
 *		@date		Dec 1, 2020
 *		@author 	D. Borsdorf [borsdorfdo]
 *		@version	V0.1
 *
 *		@b	Copyright:
 *			Leica Camera AG (Wetzlar)
 *
 *		@b	Change history:
 *
 *		- Dec 1, 2020	D. Borsdorf		File created
 **/

#pragma once

#include "iap2.h"

class MfiHandle
{
public:
    MfiHandle();
    ~MfiHandle();
    struct iap2_accessory *create_demo(const char *auth_path, const char *_cameradir, void (*new_link_cb)(struct iap2_link *link));
    void common_new_link(struct iap2_link *link);
    void common_new_link_init(struct iap2_link *link);

private:
    static const char *cameradir;
    static const char *bundleIdentifier;
    static struct iap2_accessory_description desc;

    static int wifi_credentials(struct iap2_link *link, const uint8_t *data, size_t len, void *arg);
    static int sendall(int fd, const void *buf, size_t len);
    static int tx_sof(int fd);
    static int tx(int fd, const uint8_t *data, size_t len);
    static ssize_t read_timeout(int fd, uint8_t *buf, size_t len, int timeout);
    static ssize_t rx(int fd, uint8_t *buf, size_t len, int timeout);
    static void md5_buf(uint8_t *out, const uint8_t *data, size_t data_size);
    static int send_image(int fd, const char *name, const uint8_t *data, size_t data_size);
    static uint8_t *slurp_file(const char *name, size_t *len);
    static bool is_suffix(const char *string, const char *suffix);
    static void *eap_thread(void *arg);
    static int new_eap_cb(struct iap2_link *link, const char *proto, int fd);
    static int bt_conn_update(struct iap2_link *link, const uint8_t *data, size_t len, void *arg);

};

