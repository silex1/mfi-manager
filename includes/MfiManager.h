/**
*       @ingroup    con_mfi_grp
*       @brief      MFI Manager
*       @details     This class is responsible for controlling the communication
*                    via the MFI and the OS.
*
*       @file        MfiManager.h
*       @date        24.11.2020
*       @author      J. Walter
*       @version     0.1
*
*       @b Copyright:
*          Leica Camera AG (Wetzlar)
*
*       @b Change history:
*
*     - 24.11.2020   J. Walter       File created.
**/
#pragma once

/**************************************************************************/
//INCLUDES
/**************************************************************************/
#include <leica/leica_core_if.h>
#include <leica/linux_manager_task_if.h>
#include <leica/ConnectivityManager_if.h>
#include <leica/RTOSMessageSender.h>
#include <leica/LeicaMessageQueueService.h>

#include "Mfi.h"

// TODO: Hier die MFI Protokoll-Klasse includieren. Ggf auch eine 2. für MTP/IP Protokoll?!
//#include "MtpUsb.h"

/**************************************************************************/
//ENUMERATIONS
/**************************************************************************/

/**************************************************************************/
//NAMESPACES
/**************************************************************************/
using namespace leica::common;

/**************************************************************************/
//DECLARATIONS
/**************************************************************************/
/**    @brief      Class to control MFI protocol.
 *     @details    This class is responsible for controlling the communication
 *                  via the MFI and the OS.
 **/
class MfiManager
{

public:
    /**    @brief      Constructor.
     *     @param[in]  -
     *     @param[out] -
     **/
    MfiManager();
    ~MfiManager();               ///< Destructor.

    /**    @brief      Linux signal handler.
     *     @param[in]  _sig Linux signal.
     *     @param[out] -
     *     @return     -
     **/
    static void SignalHandler(int _sig);

    /**    @brief      Initialise MFI device and send signal to CON-Manager.
     *     @param[in]  -
     *     @param[out] -
     *     @return     -
     **/
    void initialise();

    /**    @brief      Main loop of MFI Manager.
     *     @param[in]  -
     *     @param[out] -
     *     @return     -
     **/
    void run();

protected:
    /**    @brief      Message wrapper for protocol class.
     *     @details    This message wrapper is given to the protocol class to be able
     *                  to receive messages from the protocol.
     *     @param[in]  -
     *     @param[out] -
     *     @return     0
     **/
    static int requestMessageWrapper(void);

private:
    static Mfi *m_pMFI;
    static LeicaMessageQueueService *m_mqTask;
    static LeicaMessageQueueService *m_mqData;

    bool        m_ShouldRun;                            ///< Flag is set to true while main loop should run.
    E_CON_SYS_MESSAGE   m_USB_ConnectionState;          ///< Current USB connection state.
    bool        m_EnableMFI_Protocol;                   ///< If set to true the MFI protocol class will be started on cable connect.

    /**    @brief      Copy Constructor.
     *     @param[in]  _data    Object to copy.
     *     @param[out] -
     **/
    MfiManager(const MfiManager &_data);
    /**    @brief      Handle received Linux system messages.
     *     @details    Here all messages from Linux or the Linux wrapper module are handled.
     *     @param[in]  _receivedMessage Message to handle.
     *     @param[out] -
     *     @return     bool     \e true:  Message handled successful.
     *                          \e false: Message not found or handled with error.
     **/
    bool handleConCtrlMsg(const T_LEICA_CORE_MESSAGE *_receivedMessage);
    /**    @brief      Handle received driver messages.
     *     @details    Here all messages from USB or other drivers are handled.
     *     @param[in]  _receivedMessage Message to handle.
     *     @param[out] -
     *     @return     bool     \e true:  Message handled successful.
     *                          \e false: Message not found or handled with error.
     **/
    bool handleDriverCmdMsg(const T_LEICA_CORE_MESSAGE *_receivedMessage);
    /**    @brief      Handle received MTP Request packages.
     *     @details    This function handles the received MTP Request packages from the MTP tunnel.
     *     @param[in]  _receivedMessage Message to handle.
     *     @param[out] -
     *     @return     bool     \e true:  Message handled successful.
     *                          \e false: Message not found or handled with error.
     **/
    bool handleMTPCmdMsg_Requests(void);
    /**    @brief      Handle received MTP Event packages.
     *     @details    This function sends the received MTP Event data via MTP tunnel.
     *     @param[in]  -
     *     @param[out] -
     *     @return     bool     \e true:  Message handled successful.
     *                          \e false: Message not found or handled with error.
     **/
    bool handleMTPEvtMsg(const T_LEICA_CORE_MESSAGE *_receivedMessage);
    /**    @brief      Handle received RTOS statemachine messages.
     *     @details    Here all messages from statemachine are handled.
     *     @param[in]  _receivedMessage Message to handle.
     *     @param[out] -
     *     @return     bool     \e true:  Message handled successful.
     *                          \e false: Message not found or handled with error.
     **/
    bool handleStatemachineMsg(const T_LEICA_CORE_MESSAGE *_receivedMessage);

    /**    @brief      Create new MFI protocol instance.
     *     @details    In this function a new instance of the MFI protocol will be created.
     *                  If an instance is still available this old one will be deleted first.
     *     @param[in]  -
     *     @param[out] -
     *     @return     bool     \e true:  Instance created.
     *                          \e false: Error.
     **/
    bool createMfiInstance(void);
    /**    @brief      Delete current MFI protocol instance.
     *     @details    In this function an existing instance of the MFI protocol will be deleted.
     *     @param[in]  -
     *     @param[out] -
     *     @return     bool     \e true:  Instance deleted.
     *                          \e false: Error.
     **/
    static bool deleteMQInstance(void);
    /**    @brief      Delete current MQ instance.
     *     @details    In this function an existing instance of the MFI protocol will be deleted.
     *     @param[in]  -
     *     @param[out] -
     *     @return     bool     \e true:  Instance deleted.
     *                          \e false: Error.
     **/
    static bool deleteMfiInstance(void);

    /**    @brief      Callback function from deep project, to send configured message to rtos
     **/
    static void configuredCallback(void);
    /**    @brief      Send message to LMT and wait for answer message.
     *     @details    In this function sends a message via the MFI data message queue and waits until an answer message was received.
     *     @param[in]  _pSendMessage        Message to send to the LMT.
     *     @param[out] _pReceivedMessage    Answer message from the LMT.
     *     @return     E_LC_RETURN  \e E_LC_RETURN_OK: Messages sent and received.
     *                              \e Other: Error.
     **/
    E_LC_RETURN sendReceiveFromLmtDataQueue(const T_CON_PTP_OPERATION_MSG *_pSendMessage, T_LEICA_CORE_MESSAGE **_pReceivedMessage);

    /**    @brief      Send statemachine event.
     *     @details    In this function sends an STM event message to RTOS STM.
     *     @param[in]  _Event        Statemachine Event.
     *     @param[out] -
     *     @return     E_LC_RETURN  \e E_LC_RETURN_OK: Messages sent and received.
     *                              \e Other: Error.
     **/
    E_LC_RETURN sys_SendEvent(const E_SYS_EVENT _Event);

};
