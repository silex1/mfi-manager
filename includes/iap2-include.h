/**
 *		@ingroup	mfi-manager
 *		@brief		This file wraps closed c source from apple to cpp context for leica
 *		@details	
 * 
 *		@file		iap2-include.h
 *		@date		Feb 12, 2021
 *		@author 	D. Borsdorf [borsdorfdo]
 *		@version	V0.1
 *
 *		@b	Copyright:
 *			Leica Camera AG (Wetzlar)
 *
 *		@b	Change history:
 *
 *		- Feb 12, 2021	D. Borsdorf		File created
 **/

#pragma once

/*check if the compiler is of C++*/
#ifdef __cplusplus
extern "C" {
#endif

#include "iap2.h"
#include "md5.h"
#include "demo-common.h"

/*check if the compiler is of C++*/
#ifdef __cplusplus
}
#endif