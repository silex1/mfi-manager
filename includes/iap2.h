/**
 *       @brief      IAP2 Base functions
 *       @details    This file provides high level iap2 cmd set
 *
 *       @file        iap2.h
 *       @date        13.01.2021
 *       @author      D. Borsdorf
 *       @version     0.1
 *
 *       @b Copyright:
 *          Leica Camera AG (Wetzlar)
 *
 *       @b Change history:
 *
 *     - 13.01.2021   D. Borsdorf       File moved.
 **/


/*check if the compiler is of C++*/
#ifdef __cplusplus
extern "C" {
#endif

#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#include "msgid.h"

/**
 * @addtogroup iap2_transport_link
 * @{
 */

struct iap2_transport;

struct iap2_link;

/// 48 bit Bluetooth address
struct iap2_bt_addr {
	uint8_t b[6];
};

enum iap2_transport_type {
	IAP2_TRANSPORT_TYPE_USB = 1,
	IAP2_TRANSPORT_TYPE_BT,
};

/** Increment the reference counter of the link.
 *
 * @param[in] link
 */
void iap2_link_ref(struct iap2_link *link);

/** Decrement the reference counter of the link.
 *
 * @param[in] link
 */
void iap2_link_unref(struct iap2_link *link);

/** Kill link.
 *
 * Tears down a link.
 *
 * @attention The reference on @p link must be dropped manually by calling @ref iap2_ref_unref.
 *
 * @param[in] link
 */
void iap2_link_shutdown(struct iap2_link *link);

/** Get iAP accessory for iAP link.
 *
 * @param[in] link iAP link
 * @return iAP accessory
 */
struct iap2_accessory *iap2_link_get_accessory(struct iap2_link *link);

/** Get iAP transport for iAP link.
 *
 * @param[in] link iAP link
 * @return iAP transport
 */
struct iap2_transport* iap2_link_get_transport(struct iap2_link *link);

/** Tests whether a iAP transport of a certain type is present in an accessory.
 *
 * @param[in] acc iAP accessory
 * @param[in] type iAP transport type
 * @return true if the transport is present, false otherwise.
 */
bool iap2_supports_transport(struct iap2_accessory *acc, enum iap2_transport_type type);

/** Get iAP transport from iAP accessory by type.
 *
 * @code
 * struct iap2_transport *transport;
 * transport = iap2_get_transport_by_type(acc, IAP2_TRANSPORT_TYPE_BT);
 * @endcode
 *
 * @param[in] acc iAP accessory
 * @param[in] type iAP transport type
 * @return iAP transport on success, NULL otherwise.
 */
struct iap2_transport* iap2_get_transport_by_type(struct iap2_accessory *acc, enum iap2_transport_type type);

/** Get iAP transport identifier.
 *
 * See MFi Spec §90.5
 *
 * @param[in] transport iAP transport
 * @return unique ID
 */
uint16_t iap2_transport_get_id(struct iap2_transport *transport);

/** Get a transport's type.
 *
 * @param[in] transport iAP transport.
 * @return transport type
 */
enum iap2_transport_type iap2_transport_get_type(struct iap2_transport *transport);

/** Convert BT addr to 12 digit hexadecimal representation.
 *
 * @see iap2_bt_addr2str
 *
 * @param[in] a BT address
 * @param[out] str Hexadecimal string address,  e.g. "18:AF:61:D2:F8:6C".
 * @param[in] maxlen Max size of @p str buffer
 * @return 0 on success, -1 otherwise.
 */
int iap2_bt_addr2str(const struct iap2_bt_addr *a, char *str, size_t maxlen);

/** Parse 12 digit hexadecimal representation to BT addr.
 *
 * @see iap2_bt_str2addr
 *
 * @param[in] str Hexadecimal BT address, e.g. "18:AF:61:D2:F8:6C".
 * @param[out] a 48 bit BT address
 * @return 0 on success, -1 otherwise.
 */
int iap2_bt_str2addr(const char *str, struct iap2_bt_addr *a);

/** Get BT address of connected device.
 *
 * Get the BT address of the connected peer of a link.
 * Fails if the link can not be established via a iAP BT transport.
 *
 * @param[in] link iAP link
 * @param[out] addr BT peer address
 * @return 0 on success, -1 otherwise.
 */
int iap2_bt_get_peer_addr(struct iap2_link *link, struct iap2_bt_addr *addr);

/** Attempt to connect to BT device.
 *
 * Requires the presence of a BT iAP transport in the accessory.
 *
 * @see iap2_bt_listen
 *
 * @param[in] acc iAP accessory
 * @param[in] bdaddr BT device address
 * @return iAP link on success, NULL otherwise.
 */
struct iap2_link *iap2_bt_connect(struct iap2_accessory *acc, const struct iap2_bt_addr *bdaddr);

/**
 * @}
 */

/**
 * @addtogroup iap2_ctrl_session
 * @{
 */

/**
 * Control Session Message Parameters.
 *
 * @see iap2_ctrl_parse, iap2_ctrl_send
 *
 * See MFi Spec §51.3.2.3 _Parameter Types_.
 */
enum iap2_ctrl_param_type {
	IAP2_CTRL_PARAM_ENUM = 0,
	IAP2_CTRL_PARAM_INT8 = 0,
	IAP2_CTRL_PARAM_INT16,
	IAP2_CTRL_PARAM_INT32,
	IAP2_CTRL_PARAM_INT64,
	IAP2_CTRL_PARAM_BLOB, //!< Typically, most blobs are arrays of custom data structures.
	IAP2_CTRL_PARAM_UTF8, //!< UTF-8 String
	IAP2_CTRL_PARAM_NONE, //!< The mere existence or absence of the parameter in the message has meaning.
	IAP2_CTRL_PARAM_GROUP, //!< Parameters maybe grouped together as a group

	// for internal use
	IAP2_CTRL_PARAM_NUMERIC, //!< Internal type only
};

/** Control Parameter Variant
 *
 * @internal
 *
 * Should not be used directly.
 *
 * @see iap2_ctrl_param_int, iap2_ctrl_param_string, iap2_ctrl_param_group, iap2_ctrl_param_blob, iap2_ctrl_param_none,
 *      iap2_ctrl_parse, iap2_ctrl_send
 *
 */
union iap2_ctrl_data {
		struct {
			uint16_t l;
			const uint8_t *p;
		} blob;
		struct {
			uint64_t v;
			uint8_t l;
		} num;
		struct {
			const struct iap2_ctrl_param *p;
			int n;
		} group;
};

/** Outbound Control Message Parameter.
 *
 * @see iap2_ctrl_send, iap2_ctrl_param_int, iap2_ctrl_param_string, iap2_ctrl_param_group, iap2_ctrl_param_blob,
 *      iap2_ctrl_param_none
 */
struct iap2_ctrl_param {
	uint16_t id;
	enum iap2_ctrl_param_type type;

	union iap2_ctrl_data data;
};

/** Control Message Parsing Parameter Output.
 *
 * @see iap2_ctrl_parse, iap2_ctrl_policy, IAP2_CTRL_POLICY, IAP2_CTRL_POLICY_SIZED
 */
struct iap2_ctrl_out_param {
	bool present;
	enum iap2_ctrl_param_type type;

	union iap2_ctrl_data data;
};

/** Inbound Control Message Parameter Policy.
 *
 * @see iap2_ctrl_parse, iap2_ctrl_out_param, IAP2_CTRL_POLICY, IAP2_CTRL_POLICY_SIZED
 */
struct iap2_ctrl_policy {
	enum iap2_ctrl_param_type type;
	size_t size;

	bool valid;
};

/** Shorthand for defining policies.
 *
 * @see iap2_ctrl_parse
 *
 * @code
 * enum {
 *     START_SESSION_PROTO_ID = 0,
 *     START_SESSION_SESSION_ID = 1,
 *     __START_SESSION_MAX
 * };
 *
 * static const struct iap2_ctrl_policy start_session_policy[__START_SESSION_MAX] = {
 *     IAP2_CTRL_POLICY(START_SESSION_PROTO_ID, INT8),
 *     IAP2_CTRL_POLICY(START_SESSION_SESSION_ID, INT16)
 * };
 * @endcode
 */
#define IAP2_CTRL_POLICY(id, ptype) [id] = { .type = IAP2_CTRL_PARAM_ ## ptype, .valid = true }

/** Shorthand for defining policies with exact size requirements.
 *
 * @see iap2_ctrl_parse
 *
 * @code
 * enum {
 *    CHALLENGE,
 *    __CHALLENGE_MAX
 * };
 * static const struct iap2_ctrl_policy policy[] = {
 *     IAP2_CTRL_POLICY_SIZED(CHALLENGE, BLOB, MFI_CHALLENGE_SIZE)
 * };
 * @endcode
 *
 */
#define IAP2_CTRL_POLICY_SIZED(id, ptype, psize) [id] = { .type = IAP2_CTRL_PARAM_ ## ptype, .size = (psize), .valid = true }

/** Accessory to Device Power Delivering Capabilities.
 *
 * Reports whether an accessory provides power to a device.
 *
 * @see iap2_accessory_description
 *
 * See MFi Spec §21.2.3 _Power Capabilities_, §34 _Device Power_, §34.1.1.1 _Declaring Capability_,
 * §34.1.3 _Lightning Power Passthrough_ and Table 90-10.
 */
enum iap2_power_providing_capability {
	IAP2_POWER_PROVIDING_CAPABILITY_NONE = 0, //!< Accessory does not provide power to the device
	IAP2_POWER_PROVIDING_CAPABILITY_PASSTHROUGH, //!< Lightning Power Passthrough
	IAP2_POWER_PROVIDING_CAPABILITY_ADVANCED //!< Use to declare power providing caps along with USBHostTransportComponent, USBDeviceTransportComponent, UARTTransportComponent
};

/// MatchAction see MFi Spec Table 90-12
enum iap2_ea_match_action {
	IAP2_EA_MATCH_ACTION_NONE, //!< No Action: No prompt, no "Find App" button
	IAP2_EA_MATCH_ACTION_OPTIONAL, //!< Optional: The device decides whether to prompt the user to search for Apps. "Find App" button is shown.
	IAP2_EA_MATCH_ACTION_NO_ALERT, //!< No Alert: No prompt, but "Find App" button is shown.
	IAP2_EA_MATCH_ACTION_NO_COMMS, //!< No Comm Proto: Protocol not intended for communication, but user may be prompted and button is shown.
};

/// App Launch Method see MFi Spec 93.3
enum iap2_app_launch_action {
    IAP2_APP_LAUNCH_ACTION_ALERT, //!< Launch with user alert, <default>
    IAP2_APP_LAUNCH_ACTION_NO_ALERT //!< Launch app without user alert
};

/** Accessory identification information.
 *
 * The accessory description is used to populate the *IdentificationInformation* iAP2 Control Session Message.
 * The message is used to identify the accessory while the control channel is being established.
 * The freely selectable fields must match the markings of the accessory.
 *
 * If a valid @ref app_match_team_id is provided, only apps from the specified vendor will be suggested to the user following the
 * initial pairing of the accessory (see _App Match_).
 *
 * See MFi Spec §21 (_Accessory Identification_), §26 (_App Match_), §37 (_External Accessory Protocol_)
 * and §90.2.2 (_iAP2 Control Session Messages_).
 *
 * @see iap2_new, iap2_register_ea_protocol
 *
 * **Usage Example:**
 * @code
 * const static struct iap2_accessory_description desc = {
 *   .name = "Shiny Accessory POC",
 *   .model_identifier = "demo",
 *   .manufacturer = "grandcentrix",
 *   .serial_number = "2342",
 *   .firmware_version = "123",
 *   .hardware_version = "A",
 *   .current_language = "en",
 *   .supported_language = "en\0de\0",
 *   .power_providing_capability = IAP2_POWER_PROVIDING_CAPABILITY_NONE,
 *   .maximum_current_drawn_from_device = 0
 * };
 * @endcode
 */
struct iap2_accessory_description {
	const char *name; //!< Non-empty name matching the markings of the accessory.
	const char *model_identifier; //!< Non-empty model matching the markings of the accessory
	const char *manufacturer; //!< Non-empty manufacturer matching the markings of the accessory
	const char *serial_number; //!< Non-empty serial number matching the markings of the accessory
	const char *firmware_version; //!< Non-empty firmware version
	const char *hardware_version; //!< Non-empty hardware version
	const char *app_match_team_id; //!< Optional valid iOS Developer Program Team ID, see MFi Spec §26.1-26.2 (App Match)
	const char *current_language; //!< Current active language on the accessory (ISO-639 language designator).
	const char *supported_language; //!< List of NUL separated ISO-639-1 or ISO-639-2 language designators.

	enum iap2_power_providing_capability power_providing_capability; //!< Set 'None' if the accessory does not provide power to the device
	uint16_t maximum_current_drawn_from_device; //!< Maximum current drawn by the accessory in mA
};

/** Message handler for control session messages.
 *
 * @attention The handler is called in the context of the link thread. Processing of traffic for that link
 *			  is blocked while a handler runs. It should thus not block extended periods of time.
 *
 * @param[in] link Transport link origin of the message.
 * @param[in] payload Message payload.
 * @param[in] len Payload length.
 * @param[in] arg Handler context, see @ref iap2_ctrl_add_cb
 * @return 0 on success, -1 otherwise.
 */
typedef int (*iap2_ctrl_cb)(struct iap2_link *link, const uint8_t *payload, size_t len, void *arg);

/** Register a control message handler.
 *
 * An exhaustive list of messages can be found in MFi Spec §90 (*iAP2 Control Session Messages*).
 *
 * Some messages are handled by the library to perform administrative tasks such as session management.
 * These messages can not be handled by the user. Messages affected:
 *  * RequestAuthenticationCertificate
 *  * RequestAuthenticationChallengeResponse
 *  * AuthenticationSucceeded
 *  * StartIdentification
 *  * IdentificationAccepted
 *  * IdentificationRejected
 *  * StartExternalAccessoryProtocolSession
 *  * StopExternalAccessoryProtocolSession
 *
 * @see iap2_new
 *
 * @attention All handlers must be registered before the link to a device is established. Registering a callback often
 * implies that the device requires us to also declare other messages as being handled by us (through this same function)
 * or as being sent by us (through @ref iap2_ctrl_register_sent). If we don't do this, the device will reject the identification.
 *
 * @code
 * static int bt_conn_update(struct iap2_link *link, const uint8_t *data, size_t len, void *arg) {
 *    mlog(DEBUG, "%s", __func__);
 *
 *    return 0;
 * }
 *
 * iap2_ctrl_add_cb(acc, iAP2Msg_Dev_BluetoothConnectionUpdate, bt_conn_update, NULL);
 * @endcode
 *
 * @param[in] acc iAP2 accessory.
 * @param[in] id Control session message ID
 * @param[in] cb Handler for @ref msg_id, see @ref iap2_ctrl_cb.
 * @param[in] arg Context passed to @ref cb.
 * @return 0 on success, -1 otherwise.
 */
int iap2_ctrl_add_cb(struct iap2_accessory *acc, enum iap2_msg_id id, iap2_ctrl_cb cb, void *arg);

/** Register control session messages **sent** by the accessory.
 *
 * According to MFi Spec §21.2.2 an accessory must exhaustively enumerate all iAP2 control session messages that it
 * can send or receive. This function is used to register control session messages send by an accessory.
 * The types of messages an accessory can receive is determined by the registered control session message callbacks, see
 * @ref iap2_ctrl_add_cb.
 *
 * @note Since we are using control session version 1, we must not register message types regarding authentification and
 * 		 identification of an accessory.
 *
 * @see iap2_ctrl_send, iap2_ctrl_add_cb
 *
 * @param[in] acc iAP2 accessory.
 * @param[in] id Control session message ID.
 * @return 0 on success, -1 otherwise.
 */
int iap2_ctrl_register_sent(struct iap2_accessory *acc, enum iap2_msg_id id);

/** Send a control session message to a device.
 *
 * The content of the message is composed of a number of parameters. These parameters are populated by the
 * iap2_ctrl_param_* functions.
 *
 * @see iap2_ctrl_param_int, iap2_ctrl_param_string, iap2_ctrl_param_blob, iap2_ctrl_param_group, iap2_ctrl_param_none
 *
 * @param[in] link Target link.
 * @param[in] id Control session message ID.
 * @param[in] params Array of parameters.
 * @param[in] n_params Number of parameters.
 * @return 0 on success, -1 otherwise.
 *
 * **Usage Example:**
 * @code
 * struct iap2_ctrl_param group_params[2];
 * iap2_ctrl_param_int(&group_params[0], 0, 2, false);
 * iap2_ctrl_param_int(&group_params[1], 1, 1, true);
 *
 * struct iap2_ctrl_param params[1];
 * iap2_ctrl_param_group(&params[0], 0, g1_params, ARRAY_SIZE(g1_params));
 * iap2_ctrl_send(link, iAP2Msg_Acc_BluetoothComponentInformation, params, ARRAY_SIZE(params));
 * @endcode
 */
int iap2_ctrl_send(struct iap2_link *link, enum iap2_msg_id id, const struct iap2_ctrl_param *params, int n_params);

/** Set integer parameter.
 *
 * @see iap2_ctrl_send
 *
 * @param[in] param Parameter to be populated.
 * @param[in] id Parameter ID according to message spec.
 * @param[in] size Parameter payload size in bytes (e.g. 1 for uint8, 2 for uint16, ...)
 * @param[in] val Value
 */
void iap2_ctrl_param_int(struct iap2_ctrl_param *param, uint16_t id, int size, uint64_t val);

/** Set enum parameter.
 *
 * @see iap2_ctrl_send
 *
 * @param[in] param Parameter to be populated.
 * @param[in] id Parameter ID according to message spec.
 * @param[in] val Value
 */
void iap2_ctrl_param_enum(struct iap2_ctrl_param *p, uint16_t id, uint8_t val);

/** Set string parameter.
 *
 * UTF-8 encoded string.
 *
 * @see iap2_ctrl_send
 *
 * @param[int] param Parameter to be populated.
 * @param[int] id Parameter ID according to message spec.
 * @param[int] str Value
 */
void iap2_ctrl_param_string(struct iap2_ctrl_param *param, uint16_t id, const char *str);

/** Set blob parameter.
 *
 * Typically, most blobs are arrays of custom data structures.
 *
 * @attention  The lifetime of the group has to be at least as long as the one of the parameter
 *             set it is part of. (i.e., the user cannot reuse the params array after this call).
 *
 * @see iap2_ctrl_send
 *
 * @param[in] param Parameter to be populated.
 * @param[in] id Parameter ID according to message spec.
 * @param[in] data Blob
 * @param[in] len Blob length in bytes.
 */
void iap2_ctrl_param_blob(struct iap2_ctrl_param *param, uint16_t id, const uint8_t *data, size_t len);

/** Set group parameter.
 *
 * @see iap2_ctrl_send
 *
 * @param[in] param Parameter to be populated.
 * @param[in] id Parameter ID according to message spec.
 * @param[in] params Array of parameters to group.
 * @param[in] n_params Number of parameters in group.
 */
void iap2_ctrl_param_group(struct iap2_ctrl_param *param, uint16_t id, const struct iap2_ctrl_param *params, int n_params);

/** Set None parameter.
 *
 * The mere existence or absence of the parameter in the message has meaning.
 *
 * @see iap2_ctrl_send
 *
 * @param[in] param Parameter to be populated.
 * @param[in] id Parameter ID according to message spec.
 */
void iap2_ctrl_param_none(struct iap2_ctrl_param *param, uint16_t id);

/** Parse received iAP2 control session message.
 *
 * Processes a message payload and extracts the contained parameters.
 * Parameter processing is constrained by a list of policies describing the valid parameters by id, type and size.
 *
 * @see iap2_ctrl_out_param, iap2_ctrl_policy, IAP2_CTRL_POLICY, IAP2_CTRL_POLICY_SIZED
 *
 * @param[out] params Processed parameters.
 * @param[in] policy List of valid parameter types and maximum parameter length, see iap2_ctrl_policy.
 * @param[in] n_policy Number of policies.
 * @param[in] data Message payload to be evaluated.
 * @param[in] len Payload length in bytes.
 * @return 0 on success, -1 otherwise.
 *
 * **Usage Example:**
 * @code
 * enum {
 *  START_SESSION_PROTO_ID = 0,
 *  START_SESSION_SESSION_ID = 1,
 *  __START_SESSION_MAX
 * };
 *
 * static const struct iap2_ctrl_policy start_session_policy[__START_SESSION_MAX] = {
 *   IAP2_CTRL_POLICY(START_SESSION_PROTO_ID, INT8),
 *   IAP2_CTRL_POLICY(START_SESSION_SESSION_ID, INT16)
 * };
 *
 * struct iap2_ctrl_out_param params[__START_SESSION_MAX];
 *
 * if (iap2_ctrl_parse(params, start_session_policy, __START_SESSION_MAX, payload, len) < 0) {
 *   mlog(ERR, "parsing StartExternalAccessoryProtocolSession message failed");
 *   return -1;
 * }
 * @endcode
 */
int iap2_ctrl_parse(struct iap2_ctrl_out_param *params, const struct iap2_ctrl_policy *policy, size_t n_policy, const uint8_t *data, size_t len);

/**@}*/

/**
 * @addtogroup iap2
 * @{
 */

struct iap2_accessory;

/** Create a new iAP2 Accessory object.
 *
 * Allocates an @ref iap2_accessory object that manages iAP2 connections. An iAP2 connection is composed of
 * an *iAP2 link* over an *iAP2 transport* and one or more *iAP2 sessions*. See MFi Spec §51.
 *
 * @attention The lifetime of the resulting @ref iap2_accessory is governed by *reference counting* and thus must not be
 *         freed manually. If a new reference to the object is created, the reference counter must be incremented
 *         by calling @ref iap2_ref. If a reference is dropped, the counter must be decremented by @ref iap2_unref.
 *         All reference counted objects are created with a reference count of 1.
 *         I.e., it is not necessary to call @c iap2_ref after @c iap2_new.
 *
 * @see iap2_ref, iap2_unref, iap2_get_ctx
 *
 * @param[in] desc Accessory description, must be statically allocated because the @ref iap2_accessory will be ref-counted.
 * @param[in] auth_dev Path to i2c bus the auth co-processor is connected to, e.g. /dev/i2c-4
 * @param[in] new_link Called whenever a new iAP2 link is established. The reference is already accounted for.
 * @param[in] new_eap_session Called whenever an additional iAP2 session is established.
 * @param[in] ctx Context, see @ref iap2_get_ctx.
 * @return iap2_accessory object.
 */
struct iap2_accessory *iap2_new(struct iap2_accessory_description *desc,
		const char *auth_dev,
		void (*new_link)(struct iap2_link *link),
		int (*new_eap_session)(struct iap2_link *link, const char *proto, int fd),
		void *ctx);

/** Increment reference counter of accessory.
 *
 * Increments the reference counter of an iAP2 accessory object.
 *
 * @see iap2_new
 *
 * @param[in] acc The accessory.
 */
void iap2_ref(struct iap2_accessory *acc);

/** Decrement reference counter of accessory.
 *
 * Decrements the reference counter of an iAP2 accessory object.
 *
 * @see iap2_new
 *
 * @param[in] acc The accessory.
 */
void iap2_unref(struct iap2_accessory *acc);

/** Get user context from accessory.
 *
 * @see iap2_new
 *
 * @param[in] acc The accessory.
 * @return Context provided to the accessory during initialization in @ref iap2_new
 */
void *iap2_get_ctx(struct iap2_accessory *acc);

int iap2_run(struct iap2_accessory *acc);
int iap2_stop(struct iap2_accessory *acc);

/** Register External Accessory Protocol.
 *
 * The External Accessory Protocol (EA) allows the accessory to communicate with one or more apps on one or more devices
 * via EA sessions.
 *
 * Protocol names must be reverse-DNS format.
 *
 * By specifying a @p match_action, the protocol can request the device to search for a suitable companion app.
 *
 * See MFi Spec §37 (_External Accessory Protocol_) and §26 (_App Match_).
 *
 * @see iap2_accessory_description
 *
 * @attention All handlers must be registered before the link to a device is established.
 *
 * @param acc iAP2 accessory.
 * @param name Protocol name in reverse-DNS format, e.g. "com.bluegiga.iwrap".
 * @param match_action Action to be executed on device on _App Match_.
 * @return 0 on success, -1 otherwise.
 */
int iap2_register_ea_protocol(struct iap2_accessory *acc, const char *name, enum iap2_ea_match_action match_action);

/**
* @}
*/

/*check if the compiler is of C++*/
#ifdef __cplusplus
}
#endif
