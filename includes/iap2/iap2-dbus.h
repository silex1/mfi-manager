#pragma once

#include <dbus/dbus.h>

struct iap2_dbus_connection;

struct iap2_dbus_connection* iap2_dbus_register(int epfd, DBusConnection *conn);
void iap2_dbus_remove(struct iap2_dbus_connection *priv);

void iap2_dbus_append_basic_variant(DBusMessageIter *iter, int type, void *data);
void iap2_dbus_append_basic_variant_dict_entry(DBusMessageIter *dict_iter, const char *key, int type, void *data);
int iap2_dbus_extract_from_variant(DBusMessageIter *iter, int type, void *data);

int iap2_dbus_property_set(DBusConnection *conn, const char *service, const char *path,
                           const char *interface, const char *name, int type, void *value);
int iap2_dbus_property_get(DBusConnection *conn, const char *service, const char *path,
                           const char *interface, const char *name, int type,
                           int (*cb)(void *data, void *arg), void *arg);
