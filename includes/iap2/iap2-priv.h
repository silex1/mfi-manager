#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <pthread.h>
#include <limits.h>
#include <sys/epoll.h>

#include "iAP2LinkRunLoop.h"

#include "iap2.h"
#include "mfi-auth.h"
#include "ringbuf.h"
#include "list.h"
#include "ref.h"
#include "vec.h"

#define IAP2_CTRL_SESSION_ID 1
#define IAP2_EAP_SESSION_ID 2

struct iap2_transport_ops
{
    void (*send_packet)(struct iap2_link *link, const uint8_t *data, size_t len);
    void (*cleanup)(struct iap2_link *link);

    void (*setup_syn_params)(struct iap2_link *link, iAP2PacketSYNData_t *syn);

    int (*bt_get_peer_addr)(struct iap2_link *link, struct iap2_bt_addr *addr);
};

struct iap2_link *iap2_link_new(struct iap2_accessory *acc,
                                struct iap2_transport_ops *ops,
                                struct iap2_transport *transport,
                                void *priv,
                                bool use_callback);
int iap2_link_start(struct iap2_link *link);

struct iap2_ctrl_cb
{
    uint16_t msg_id;
    iap2_ctrl_cb cb;
    void *arg;
};

struct iap2_epoll
{
    void (*cb)(struct iap2_epoll *ctx, uint32_t events);
    int fd;
    void *arg;
};

struct iap2_transport
{
    struct list_node node;
    enum iap2_transport_type type; //!< Transport Type
    uint16_t id; //!< Unique TransportComponentIdentifier

    int (*provide_identification)(struct iap2_transport *t, struct iap2_ctrl_param *slot);
    struct iap2_link* (*bt_connect)(struct iap2_transport *t, const struct iap2_bt_addr *addr);
};

struct iap2_accessory
{
    void *user_ctx;

    struct refcnt ref;

    int epfd;
    bool running;

    void (*new_link_cb)(struct iap2_link *link);
    int (*new_eap_session_cb)(struct iap2_link *link, const char *proto, int fd);

    vec_t ctrl_cb;
    vec_t sent_msgs;
    vec_t ea_protocols;
    struct list_node transports;

    char auth_dev[PATH_MAX];
    uint8_t auth_cert[MFI_MAX_CERT_SIZE];
    size_t auth_cert_size;
    pthread_mutex_t auth_mutex;

    struct iap2_accessory_description *description;
};

enum iap2_link_state
{
    IAP2_LINK_IDLE,
    IAP2_LINK_CONNECTING,
    IAP2_LINK_CONNECTED,
    IAP2_LINK_DEAD
};

struct iap2_timer;

struct iap2_ea_session
{
    struct iap2_link *link;

    bool alive;

    uint8_t proto_id;
    uint16_t session_id;

    listnode_t list;

    struct iap2_epoll epoll_ctx;
    uint32_t events;

    // preallocated buffer to avoid large stack allocations/heap
    // reallocations during transmission
    uint8_t *tx_buf;
    size_t tx_buf_pending_off;
    uint32_t tx_buf_pending_sz;
    ringbuf_t rx_buf;

    // this mutex protects the rx_buf data structure and also ensures that
    // only one party is writing to the application-facing socket at a time
    pthread_mutex_t rx_mutex;
};

struct iap2_link
{
    struct iap2_accessory *acc;
    struct iap2_transport *transport;

    struct refcnt ref;

    enum iap2_link_state state;
    pthread_mutex_t state_mutex;
    pthread_cond_t state_cond;

    iAP2LinkRunLoop_t *run_loop;
    pthread_t thread;
    pthread_mutex_t iap2_mutex;

    iAP2PacketSYNData_t iap_syn;

    int epfd;

    struct iap2_timer *timer;

    int event_fd;
    struct iap2_epoll event_ctx;

    struct iap2_transport_ops *transport_ops;
    void *transport_priv;

    iAP2Packet_t *current_packet;

    void (*connect_cb)(struct iap2_link *link);

    listnode_t ea_sessions;

    int connect_timeout;
};

struct iap2_ea_protocol
{
    const char *name;
    enum iap2_ea_match_action match;
};

int iap2_register_fd(int epfd, int fd, uint32_t events, void (*cb)(struct iap2_epoll *ctx, uint32_t events), struct iap2_epoll *ctx, void *arg);
int iap2_unregister_fd(int epfd, int fd);
int iap2_update_fd_events(int epfd, struct iap2_epoll *ctx, uint32_t events);

void iap2_ll_submit_data(struct iap2_link *link, const uint8_t *data, size_t len);
void iap2_ll_signal_connected(struct iap2_link *link, bool connected);
static inline void *iap2_ll_priv(struct iap2_link *link)
{
    return link->transport_priv;
}

int iap2_ctrl_handle(struct iap2_link *link, const uint8_t *data, size_t len);
int iap2_ctrl_RequestAuthenticationCertificate(struct iap2_link *link, const uint8_t *payload, size_t len, void *arg);
int iap2_ctrl_RequestAuthenticationChallengeResponse(struct iap2_link *link, const uint8_t *data, size_t len, void *arg);
int iap2_ctrl_StartExternalAccessoryProtocolSession(struct iap2_link *link, const uint8_t *payload, size_t len, void *arg);
int iap2_ctrl_StopExternalAccessoryProtocolSession(struct iap2_link *link, const uint8_t *payload, size_t len, void *arg);

int iap2_timer_init(struct iap2_link *link);
void iap2_timer_destroy(struct iap2_link *link);

int iap2_loop_init(struct iap2_link *link);
void iap2_loop_destroy(struct iap2_link *link);

void iap2_link_set_state(struct iap2_link *link, enum iap2_link_state state);
enum iap2_link_state iap2_link_get_state(struct iap2_link *link);
enum iap2_link_state iap2_link_wait_state_change(struct iap2_link *link, enum iap2_link_state last_state);
void iap2_link_runloop_updated(struct iap2_link *link);

void iap2_ea_device_data(struct iap2_link *link, uint8_t *data, size_t len);
void iap2_ea_cleanup_session(struct iap2_ea_session *sess);
bool iap2_ea_is_configured(struct iap2_accessory *acc);
void iap2_ea_runloop_updated(struct iap2_ea_session *sess);

/** Register an iAP Transport with an accessory.
 *
 * The transport can be used to establish new links to devices.
 *
 * Assigns a unique id to the transport.
 *
 * @see iap2_bt_listen, iap2_usb_listen
 *
 * @param[in] acc iAP accessory
 * @param[in] transport iAP transport (BT, USB)
 */
void iap2_register_transport(struct iap2_accessory *acc, struct iap2_transport *transport);

/** Remove iAP Transport from accessory.
 *
 * @attention Removing a transport once a accessory has been started is not safe.
 *
 * @param[in] acc iAP accessory
 * @param[in] transport iAP transport
 */
void iap2_remove_transport(struct iap2_accessory *acc, struct iap2_transport *transport);
