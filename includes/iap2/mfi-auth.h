/**
 *       @brief      MFI Authentication interface
 *       @details     Interface for MFI authentication and cert.
 *
 *       @file        mfi-auth.h
 *       @date        11.03.2020
 *       @author      J. Walter
 *       @version     0.1
 *
 *       @b Copyright:
 *          Leica Camera AG (Wetzlar)
 *
 *       @b Change history:
 *
 *     - 11.03.2020   J. Walter       File created.
 **/

/**************************************************************************/
//INCLUDES
/**************************************************************************/

#pragma once

#include <stdint.h>
#include <sys/types.h>

#ifdef __cplusplus
extern "C"
{
#endif

/**************************************************************************/
//Defines
/**************************************************************************/
#define MFI_MAX_CERT_SIZE (5*128)
#define MFI_MAX_SERIAL_SIZE (32 + 1)
#define MFI_CHALLENGE_SIZE 32
#define MFI_RESPONSE_SIZE 64

/**************************************************************************/
//ENUMERATIONS
/**************************************************************************/
/**    Authentication commands.
 **/
typedef enum E_MFI_CMD_ID
{
    E_MFI_CMD_ID_AUTH = 0,
    E_MFI_CMD_ID_CERT,
} E_MFI_CMD_ID;

/**    Command structure.
 **/
typedef struct T_MFI_CMD_HDR
{
    uint32_t cmdid;
    uint32_t len;
}__attribute__ ((__packed__)) T_MFI_CMD_HDR;


struct auth_handle;

struct auth_handle *mfi_auth_open(const char *path);
void mfi_auth_close(struct auth_handle *auth);

ssize_t mfi_get_cert(struct auth_handle *auth, uint8_t *cert);
int mfi_auth(struct auth_handle *auth, const uint8_t *challenge, uint8_t *response);
int mfi_get_serial(struct auth_handle *auth, char *out, size_t n);

int mfi_auth_open_remote(const char *peer);
ssize_t mfi_get_cert_remote(int fd, uint8_t *cert);
int mfi_auth_remote(int fd, const uint8_t *challenge, uint8_t *response);

#ifdef __cplusplus
}
#endif
