#pragma once

#include <jni.h>

#include "iap2.h"

struct iap2_jni_bt_listener;

typedef void (*iap2_jni_bt_intf_iter_cb_t)(void*, uint8_t *);

struct iap2_jni_bt_listener *iap2_jni_bt_listen(struct iap2_accessory *acc, JNIEnv *env);
void iap2_jni_bt_listen_stop(JNIEnv *env, struct iap2_jni_bt_listener *listener);

void JNICALL Java_net_grandcentrix_linmfi_Native_iap2_1ll_1submit_1data(JNIEnv *env, jclass clazz, jlong link_, jbyteArray jdata, jint jlen);
void JNICALL Java_net_grandcentrix_linmfi_Native_iap2_1link_1shutdown(JNIEnv *env, jclass clazz, jlong link_);
void JNICALL Java_net_grandcentrix_linmfi_Native_iap2_1new_1bt(JNIEnv *env, jclass clazz, jlong acc_, jlong listener_, jobject socket, jstring jaddr);
