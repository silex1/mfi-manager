/*check if the compiler is of C++*/
#ifdef __cplusplus
extern "C" {
#endif

#pragma once

#include <stdint.h>

#include "iap2.h"

/**
 * @addtogroup iap2_transport_link
 * @{
 */

struct iap2_bt_listener;

/** Adds a BT iAP transport to an accessory.
 *
 * Sets up a BT iAP transport using the bluez bluetooth stack.
 *
 * The following actions take place:
 * * Opens dbus system bus
 * * Register our dbus endpoint as an agent in NoInputNoOutput mode (for interactionless pairing)
 * * Set our agent as default agent
 * * Register a server profile with RFCOMM channel @p chan
 *
 * Relevant bluez documentation:
 *  * bluez/doc/adapter-api.txt
 *  * bluez/doc/agent-api.txt
 *  * bluez/doc/profile-api.txt
 *
 *  @see iap2_bt_listen_stop
 *
 * @param[in] acc iAP accessory
 * @param[in] chan RFCOMM channel
 * @return BT listener on success, NULL otherwise.
 */
struct iap2_bt_listener *iap2_bt_listen(struct iap2_accessory *acc, uint8_t chan);

/** Tear down BT transport.
 *
 * @param listener BT listener
 */
void iap2_bt_listen_stop(struct iap2_bt_listener *listener);

/** Make accessory discoverable and pairable.
 *
 * @see iap2_bt_connect
 *
 * @return 0 on success, -1 otherwise.
 */
int iap2_bt_enter_pairing(void);

/**
* @}
*/

/*check if the compiler is of C++*/
#ifdef __cplusplus
}
#endif
