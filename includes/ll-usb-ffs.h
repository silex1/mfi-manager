/*check if the compiler is of C++*/
#ifdef __cplusplus
extern "C" {
#endif

#pragma once

#include <stdint.h>

#include "iap2.h"

/**
 * @addtogroup iap2_transport_link
 * @{
 */

struct iap2_usb_listener;

/** Adds an USB iAP transport to an accessory.
 *
 * Sets up a USB iAP transport using the Linux USB function file system (ffs).
 *
 * The functionfs must be set up and mounted. See scripts/setup-usb-ffs.sh for example.
 *
 *
 *
 * Relevant USB ffs documentation:
 * * linux/Documentation/usb/functionfs.txt
 * * linux/tools/usb/ffs-aio-example
 *
 * @see iap2_usb_close
 *
 * @param[in] acc iAP accessory
 * @param[in] ffs_dir FunctionFS mount point
 * @return USB listener on success, NULL otherwise.
 */
struct iap2_usb_listener *iap2_usb_listen(struct iap2_accessory *acc, const char *ffs_dir);

/** Tear down USB transport.
 *
 * @param listener USB listener.
 */
void iap2_usb_close(struct iap2_usb_listener *listener);

/**
 * @}
 */

/*check if the compiler is of C++*/
#ifdef __cplusplus
}
#endif
