#pragma once

enum iap2_msg_id {
	iAP2Msg_Dev_RequestAuthenticationCertificate = 0xAA00,
	iAP2Msg_Acc_AuthenticationCertificate = 0xAA01,
	iAP2Msg_Dev_RequestAuthenticationChallengeResponse = 0xAA02,
	iAP2Msg_Acc_AuthenticationResponse = 0xAA03,
	iAP2Msg_Dev_AuthenticationFailed = 0xAA04,
	iAP2Msg_Dev_AuthenticationSucceeded = 0xAA05,
	iAP2Msg_Acc_AccessoryAuthenticationSerialNumber = 0xAA06,

	iAP2Msg_Dev_StartIdentification = 0x1D00,
	iAP2Msg_Acc_IdentificationInformation = 0x1D01,
	iAP2Msg_Dev_IdentificationAccepted = 0x1D02,
	iAP2Msg_Dev_IdentificationRejected = 0x1D03,
	iAP2Msg_Acc_CancelIdentification = 0x1D05,
	iAP2Msg_Acc_IdentificationInformationUpdate = 0x1D06,

	iAP2Msg_Acc_RequestAppLaunch = 0xEA02,

	iAP2Msg_Acc_StartAssistiveTouch = 0x5400,
	iAP2Msg_Acc_StopAssistiveTouch = 0x5401,
	iAP2Msg_Acc_StartAssistiveTouchInformation = 0x5402,
	iAP2Msg_Dev_AssistiveTouchInformation = 0x5403,
	iAP2Msg_Acc_StopAssistiveTouchInformation = 0x5404,

	iAP2Msg_Acc_BluetoothComponentInformation = 0x4E01,
	iAP2Msg_Acc_StartBluetoothConnectionUpdates = 0x4E03,
	iAP2Msg_Dev_BluetoothConnectionUpdate = 0x4E04,
	iAP2Msg_Acc_StopBluetoothConnectionUpdates = 0x4E05,

	iAP2Msg_Dev_StartBluetoothPairing = 0x0B00,
	iAP2Msg_Acc_BluetoothPairingAccessoryInformation = 0x0B01,
	iAP2Msg_Acc_BluetoothPairingStatus = 0x0B02,
	iAP2Msg_Dev_StopBluetoothPairing = 0x0B03,

	iAP2Msg_Acc_StartCallStateUpdates = 0x4154,
	iAP2Msg_Dev_CallStateUpdate = 0x4155,
	iAP2Msg_Acc_StopCallStateUpdates = 0x4156,
	iAP2Msg_Acc_StartCommunicationsUpdates = 0x4157,
	iAP2Msg_Dev_CommunicationsUpdate = 0x4158,
	iAP2Msg_Acc_StopCommunicationsUpdates = 0x4159,
	iAP2Msg_Acc_InitiateCall = 0x415A,
	iAP2Msg_Acc_AcceptCall = 0x415B,
	iAP2Msg_Acc_EndCall = 0x415C,
	iAP2Msg_Acc_SwapCalls = 0x415D,
	iAP2Msg_Acc_MergeCalls = 0x415E,
	iAP2Msg_Acc_HoldStatusUpdate = 0x415F,
	iAP2Msg_Acc_MuteStatusUpdate = 0x4160,
	iAP2Msg_Acc_SendDTMF = 0x4161,

	iAP2Msg_Dev_DeviceInformationUpdate = 0x4E09,
	iAP2Msg_Dev_DeviceLanguageUpdate = 0x4E0A,
	iAP2Msg_Dev_DeviceTimeUpdate = 0x4E0B,
	iAP2Msg_Dev_DeviceUUIDUpdate = 0x4E0C,

	iAP2Msg_Dev_StartExternalAccessoryProtocolSession = 0xEA00,
	iAP2Msg_Dev_StopExternalAccessoryProtocolSession = 0xEA01,
	iAP2Msg_Acc_StatusExternalAccessoryProtocolSession = 0xEA03,

	iAP2Msg_Acc_StartHID = 0x6800,
	iAP2Msg_Dev_DeviceHIDReport = 0x6801,
	iAP2Msg_Acc_AccessoryHIDReport = 0x6802,
	iAP2Msg_Acc_StopHID = 0x6803,
	iAP2Msg_Dev_StartNativeHID = 0x6806,

	iAP2Msg_Dev_StartLocationInformation = 0xFFFA,
	iAP2Msg_Acc_LocationInformation = 0xFFFB,
	iAP2Msg_Dev_StopLocationInformation = 0xFFFC,

	iAP2Msg_Acc_StartMediaLibraryInformation = 0x4C00,
	iAP2Msg_Dev_MediaLibraryInformation = 0x4C01,
	iAP2Msg_Acc_StopMediaLibraryInformation = 0x4C02,
	iAP2Msg_Acc_StartMediaLibraryUpdates = 0x4C03,
	iAP2Msg_Dev_MediaLibraryUpdate = 0x4C04,
	iAP2Msg_Acc_StopMediaLibraryUpdates = 0x4C05,
	iAP2Msg_Acc_PlayMediaLibraryCurrentSelection = 0x4C06,
	iAP2Msg_Acc_PlayMediaLibraryItems = 0x4C07,
	iAP2Msg_Acc_PlayMediaLibraryCollection = 0x4C08,
	iAP2Msg_Acc_PlayMediaLibrarySpecial = 0x4C09,

	iAP2Msg_Acc_StartNowPlayingUpdates = 0x5000,
	iAP2Msg_Dev_NowPlayingUpdate = 0x5001,
	iAP2Msg_Acc_StopNowPlayingUpdates = 0x5002,
	iAP2Msg_Acc_SetNowPlayingInformation = 0x5003,

	iAP2Msg_Acc_StartPowerUpdates = 0xAE00,
	iAP2Msg_Dev_PowerUpdate = 0xAE01,
	iAP2Msg_Acc_StopPowerUpdates = 0xAE02,
	iAP2Msg_Acc_PowerSourceUpdate = 0xAE03,

	iAP2Msg_Acc_StartUSBDeviceModeAudio = 0xDA00,
	iAP2Msg_Dev_USBDeviceModeAudioInformation = 0xDA01,
	iAP2Msg_Acc_StopUSBDeviceModeAudio = 0xDA02,

	iAP2Msg_Acc_StartVoiceOver = 0x5612,
	iAP2Msg_Acc_StopVoiceOver = 0x5613,
	iAP2Msg_Acc_RequestVoiceOverMoveCursor = 0x5601,
	iAP2Msg_Acc_RequestVoiceOverActivateCursor = 0x5602,
	iAP2Msg_Acc_RequestVoiceOverScrollPage = 0x5603,
	iAP2Msg_Acc_RequestVoiceOverSpeakText = 0x5606,
	iAP2Msg_Acc_RequestVoiceOverPauseText = 0x5608,
	iAP2Msg_Acc_RequestVoiceOverResumeText = 0x5609,
	iAP2Msg_Acc_StartVoiceOverUpdates = 0x560B,
	iAP2Msg_Dev_VoiceOverUpdate = 0x560C,
	iAP2Msg_Acc_StopVoiceOverUpdates = 0x560D,
	iAP2Msg_Acc_RequestVoiceOverConfiguration = 0x560E,
	iAP2Msg_Acc_StartVoiceOverCursorUpdates = 0x560F,
	iAP2Msg_Dev_VoiceOverCursorUpdate = 0x5610,
	iAP2Msg_Acc_StopVoiceOverCursorUpdates = 0x5611,

	iAP2Msg_Acc_RequestWiFiInformation = 0x5700,
	iAP2Msg_Dev_WiFiInformation = 0x5701
};

enum iap2_msg_type {
    iap2Msg_Acc = 1,
    iap2Msg_Dev
};
