#pragma once

#include <stdint.h>
#include <stddef.h>

int cloexec(int fd);
int nonblock(int fd);
int wait_for_poll_events(int fd, short events, int timeout);

int writeall(int fd, const uint8_t *data, size_t len);
int write_file(const char *path, const uint8_t *data, size_t len, int flags);

/**
 *     @brief      Write BigEndian value into BigEndian array.
 *     @param[in]  _InputVal    Value to write.
 *     @param[out] _pBEpointer  Allocated destination pointer.
 *     @return     -
 **/
void writeU16toBEpointer(const uint16_t _InputVal, void *_pBEpointer);

/**
 *     @brief      Read BigEndian value from BigEndian array.
 *     @param[in]  _pBEpointer  Allocated source pointer.
 *     @param[out] -
 *     @return      Valur of uint16_t.
 **/
uint16_t readU16fromBEpointer(const void *_pBEpointer);

