#pragma once

#include <stddef.h>
#include <stdatomic.h>

struct refcnt
{
    volatile atomic_int cnt;
    void (*drop)(struct refcnt *refcnt);

#ifndef NDEBUG
    const char *class;
    size_t offset;
#endif
};

#define ref_init(instance, struct, member, drop) \
	_ref_init(&(instance->member), #struct, offsetof(struct, member), drop)

#define ref_up(refcnt) _ref_up((refcnt), __FILE__, __func__, __LINE__)
#define ref_down(refcnt) _ref_down((refcnt), __FILE__, __func__, __LINE__)

void _ref_init(struct refcnt *refcnt, const char *class, size_t offset, void (*drop)(struct refcnt *refcnt));
void _ref_up(struct refcnt *refcnt, const char *file, const char *func, int line);
void _ref_down(struct refcnt *refcnt, const char *file, const char *func, int line);
