#pragma once

#include <stdint.h>
#include <stddef.h>

struct vec
{
    size_t space, nmemb;
    size_t elem_size;

    uint8_t *data;
};

typedef struct vec *vec_t;

vec_t vec_new(size_t elem_size, size_t initial_space);
void vec_free(vec_t vec);
int vec_append(vec_t vec, void *data);

static inline void *vec_index(vec_t vec, size_t idx)
{
    return &vec->data[idx * vec->elem_size];
}

static inline size_t vec_nmemb(vec_t vec)
{
    return vec->nmemb;
}

#define vec_foreach(vec, type, cursor) for (type cursor = (type)(vec)->data; cursor < ((type)(vec)->data) + vec->nmemb; cursor++)
#define vec_foreach_until(vec, type, cursor, last) for (type cursor = (type)(vec)->data; cursor < ((type)(vec)->data) + vec->nmemb && cursor < (last); cursor++)
