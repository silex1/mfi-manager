#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <poll.h>

#include <leica/leica_types.h>
#include <leica/hsk_debug_linux_if.h>

#include "io.h"

void writeU16toBEpointer(const uint16_t _InputVal, void *_pBEpointer)
{
    ((uint8_t *)_pBEpointer)[0]= (uint8_t)((_InputVal >> 8) & 0x00FF);
    ((uint8_t *)_pBEpointer)[1]= (uint8_t)(_InputVal & 0x00FF);
}

uint16_t readU16fromBEpointer(const void *_pBEpointer)
{
    return ((((uint16_t)((uint8_t *)_pBEpointer)[0]) << 8) + ((uint16_t)((uint8_t *)_pBEpointer)[1]));
}

int cloexec(int fd)
{
    int flags;

    flags = fcntl(fd, F_GETFD);
    if ( flags == -1 )
        return -1;

    if ( flags & FD_CLOEXEC )
        return 0;

    flags |= FD_CLOEXEC;
    if ( fcntl(fd, F_SETFD, flags) == -1 )
        return -1;

    return 0;
}

int nonblock(int fd)
{
    return fcntl(fd, F_SETFL, O_NONBLOCK);
}

int wait_for_poll_events(int fd, short events, int timeout)
{
    struct pollfd pfd;
    int ret;

    pfd.fd = fd;
    pfd.events = events;
    pfd.revents = 0;

    poll_restart:
    ret = poll(&pfd, 1, timeout);
    if ( ret < 0 )
    {
        if ( errno == EINTR )
            goto poll_restart;
        else
            return -1;
    }

    return 0;
}

int writeall(int fd, const uint8_t *data, size_t len)
{
    while ( len )
    {
        ssize_t ret;

        ret = write(fd, data, len);
        if ( ret < 0 )
        {
            if ( errno == EINTR )
            {
                continue;
            }
            else if ( errno == EAGAIN || errno == EWOULDBLOCK )
            {
                ret = wait_for_poll_events(fd, POLLOUT, -1);
                if ( ret )
                    return -1;
                continue;
            }

            return -1;
        }

        data += ret;
        len -= ret;
    }

    return 0;
}

int write_file(const char *path, const uint8_t *data, size_t len, int flags)
{
    int fd, ret;

    fd = open(path, O_WRONLY | O_CLOEXEC | flags);
    if ( fd < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "opening file %s failed: %s", path, strerror(errno));
        return -1;
    }

    ret = writeall(fd, data, len);
    if ( ret < 0 )
    hsk_print(E_LEVEL_ERROR, "writing to file %s failed: %s", path, strerror(errno));

    close(fd);

    return ret;
}
