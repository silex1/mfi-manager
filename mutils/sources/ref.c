#include <stdlib.h>
#include <stdatomic.h>

#include "ref.h"

void _ref_init(struct refcnt *refcnt, const char *class, size_t offset, void (*drop)(struct refcnt *refcnt))
{
    atomic_init(&refcnt->cnt, 1);
    refcnt->drop = drop;
}

void _ref_up(struct refcnt *refcnt, const char *file, const char *func, int line)
{
    int old;
    (void)file;
    (void)func;
    (void)line;
    (void)old;

    old = atomic_fetch_add(&refcnt->cnt, 1);
}

void _ref_down(struct refcnt *refcnt, const char *file, const char *func, int line)
{
    int old;

    (void)file;
    (void)func;
    (void)line;

    old = atomic_fetch_sub(&refcnt->cnt, 1);
    if ( old == 1 )
    {
        refcnt->drop(refcnt);
    }
}
