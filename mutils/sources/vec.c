#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>

#include "vec.h"

vec_t vec_new(size_t elem_size, size_t initial_space)
{
    size_t size;

    if ( __builtin_mul_overflow(elem_size, initial_space, &size) )
    {
        errno = ENOMEM;
        return NULL;
    }

    vec_t vec = malloc(sizeof(struct vec));
    if ( !vec )
        return NULL;

    vec->nmemb = 0;
    vec->space = initial_space;
    vec->elem_size = elem_size;

    vec->data = malloc(size);
    if ( !vec->data )
    {
        free(vec);
        return NULL;
    }

    return vec;
}

static int vec_extend(vec_t vec, size_t elems)
{
    size_t new_elems;
    size_t size;

    if ( __builtin_add_overflow(vec->nmemb, elems, &new_elems) )
        goto nomem;

    if ( __builtin_mul_overflow(vec->elem_size, new_elems, &size) )
        goto nomem;

    uint8_t *new_data = realloc(vec->data, size);
    if ( !new_data )
        return -1;

    vec->data = new_data;
    vec->space = new_elems;

    return 0;

    nomem:
    errno = ENOMEM;
    return -1;
}

int vec_append(vec_t vec, void *data)
{
    if ( vec->nmemb == vec->space )
        if ( vec_extend(vec, vec->nmemb ? vec->nmemb : 1) < 0 )
            return -1;

    memcpy(&vec->data[vec->nmemb * vec->elem_size], data, vec->elem_size);
    vec->nmemb++;

    return 0;
}

void vec_free(vec_t vec)
{
    if ( !vec )
        return;

    free(vec->data);
    free(vec);
}
