/**
 *      @ingroup    mfi-manager
 *      @brief      Base for MFI Communication
 *      @details    This class is singelton
 * 
 *      @file       Mfi.cpp
 *      @date       Dec 1, 2020
 *      @author     D. Borsdorf [borsdorfdo]
 *      @version    V0.1
 *
 *      @b  Copyright:
 *          Leica Camera AG (Wetzlar)
 *
 *      @b  Change history:
 *
 *      - Dec 1, 2020   D. Borsdorf     File created
 **/

#include <csignal>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "Mfi.h"
#include "hsk_debug_linux_if.h"
#include "iap2.h"
#include "ll-bt-linux.h"
#include "ll-usb-ffs.h"

#include "mfi-auth.h"

#define D_I2C_DEV "/dev/i2c-0"
#define D_UDC_DEV_NAME "19c00000.dwc3"
#define D_UDC_PATH "/sys/kernel/config/usb_gadget/lc_m11_gadget/UDC"
#define D_FFS_PATH "/dev/usb-ffs/iap2"
#define D_BT_C_ADDR "55:55:55:55:55:55"

Mfi* Mfi::_instance = 0;
MfiHandle Mfi::m_mfiHandle;
void (*Mfi::cb_configured)(void);
bool Mfi::m_stop = false;
bool Mfi::m_isReady = false;
char* Mfi::m_authDev = NULL;
char* Mfi::m_udcName = NULL;
char* Mfi::m_udcPath = NULL;
char* Mfi::m_ffsDir = NULL;
char* Mfi::m_btClientAddress = NULL;
struct iap2_accessory *Mfi::m_acc = NULL;

Mfi::Mfi()
{
    m_stop = false;
    m_isReady = false;
    hsk_print(E_LEVEL_INFO, "Mfi::Mfi: Create Instance(%d)");

    //start();
    setDefaults();
}

Mfi::~Mfi()
{
//    hsk_print(E_LEVEL_INFO, "Mfi::~Mfi: Destructor called");
    terminate();
    hsk_print(E_LEVEL_INFO, "<%s>: free memory", __func__);
}

Mfi* Mfi::Instance()
{
    static InstanceGuard g;   // Speicherbereinigung
    if (!_instance)
        _instance = new Mfi();

    return _instance;
}

bool Mfi::start()
{
    m_stop = false;
    /* initialized with default attributes */
    u_int32_t retVal = pthread_attr_init(&m_RunThreadAttr);
    pthread_attr_setdetachstate(&m_RunThreadAttr, PTHREAD_CREATE_JOINABLE);
    if ( retVal == 0 )
    {
        /* create the thread */
        retVal = pthread_create(&m_RunThread, &m_RunThreadAttr, &Mfi::run, this);
        if ( retVal )
        {
            hsk_print(E_LEVEL_ERROR, "<%s>: pthread_create() return code(%d)",__func__, retVal);
        }
    }
    else
    {
        hsk_print(E_LEVEL_ERROR, "<%s>: pthread_attr_init() return code(%d)",__func__, retVal);
    }
    return retVal;
}

bool Mfi::terminate()
{
    hsk_print(E_LEVEL_INFO, "Mfi::<%s>", __func__);
    m_stop = true;
    iap2_stop(m_acc);
    sleep(1);

    int detachState = PTHREAD_CREATE_DETACHED;
    int result = pthread_attr_getdetachstate(&m_RunThreadAttr, &detachState);
    if ( result )
    {
        hsk_print(E_LEVEL_WARNING, "<%s>: getdetachstate failed", __func__);
    }
    if ( detachState == PTHREAD_CREATE_JOINABLE )
    {
        result = pthread_cancel(m_RunThread);
        if ( result )
        {
            hsk_print(E_LEVEL_WARNING, "<%s>: pthread_cancel failed", __func__);
        }
        result = pthread_join(m_RunThread, NULL);
        if ( result )
        {
            hsk_print(E_LEVEL_WARNING, "<%s>: pthread_join failed", __func__);
        }
    } else {
        hsk_print(E_LEVEL_WARNING, "<%s>: Thread is detachted, cannot join to destroy!", __func__, result);
    }
    pthread_attr_destroy(&m_RunThreadAttr);

    hsk_print(E_LEVEL_WARNING, "Mfi::terminate: result(%s)", result ? "fail" : "okay");

    return result;
}

void Mfi::setDefaults()
{
    //lc_mfi-testapp --tcmd=mfi --auth=/dev/i2c-1 --usb-ffs=/dev/usb-ffs/iap2 --usb-udc-path=/sys/kernel/config/usb_gadget/lc_m11_gadget/UDC --usb-udc-name=19c00000.dwc3
    size_t len = 0;
    len = strlen(D_I2C_DEV) + 1;
    m_authDev = static_cast<char*>( malloc(len) );
    strncpy(m_authDev, D_I2C_DEV, len);

    len = strlen(D_UDC_DEV_NAME) + 1;
    m_udcName = static_cast<char*>( malloc(len) );
    strncpy(m_udcName, D_UDC_DEV_NAME, len);

    len = strlen(D_UDC_PATH) + 1;
    m_udcPath = static_cast<char*>( malloc(len) );
    strncpy(m_udcPath, D_UDC_PATH, len);

    len = strlen(D_FFS_PATH) + 1;
    m_ffsDir = static_cast<char*>( malloc(len) );
    strncpy(m_ffsDir, D_FFS_PATH, len);

//    len = strlen(D_BT_C_ADDR);
//    m_btClientAddress = static_cast<char*>( malloc(len) );
//    strncpy(m_btClientAddress, D_BT_C_ADDR);
}

void Mfi::configured()
{
    if (cb_configured)
        cb_configured();
}


void *Mfi::run(void *_parameter)
{
    while (!m_stop) {
        hsk_print(E_LEVEL_INFO, "<%s>: doing stuff", __func__);
        if (mfiRun() != 0)
            usleep(500);
    }

    hsk_print(E_LEVEL_INFO, "<%s>: exit mfi thread now!", __func__);
    pthread_exit((void*) _parameter);
}

int Mfi::mfiRun()
{
    int retVal = 0;
    struct iap2_link *link;
    struct iap2_bt_listener *listener_bt;
    struct iap2_usb_listener *listener_usb;

    if (!m_authDev) {
        hsk_print(E_LEVEL_ERROR, "<%s>: auth-dev required", __func__);
        goto work_fail;
    }

    if (!!m_udcName != !!m_udcPath) {
        hsk_print(E_LEVEL_ERROR, "<%s>: usb-udc-path and usb-udc-name have to be set in combination", __func__);
        goto work_fail;
    }

    if (m_udcPath && !m_ffsDir) {
        hsk_print(E_LEVEL_ERROR, "<%s>: usb-udc-path and usb-udc-name only work when usb-ffs is set", __func__);
        goto work_fail;
    }

    m_acc = m_mfiHandle.create_demo(m_authDev, ".", newLinkInit);
    if (!m_acc)
        goto work_fail;

    if (m_btClientAddress) {
        hsk_print(E_LEVEL_INFO,"<%s>: starting bt listener", __func__);
        listener_bt = iap2_bt_listen(m_acc, 0);
        if (!listener_bt) {
            hsk_print(E_LEVEL_ERROR, "<%s>: starting listener failed", __func__);
            goto work_fail;
        }

        hsk_print(E_LEVEL_INFO,"starting bt client", __func__);
        struct iap2_bt_addr addr;
        if (iap2_bt_str2addr(m_btClientAddress, &addr)) {
            hsk_print(E_LEVEL_ERROR, "<%s>: invalid bluetooth addr: %s", m_btClientAddress);
            goto work_fail;
        }
        do {
            link = iap2_bt_connect(m_acc, &addr);
            if (!link) {
                hsk_print(E_LEVEL_WARNING, "<%s>: bluetooth connection failed, retrying", __func__);
                sleep(1);
            }
        } while (!link);

        m_mfiHandle.common_new_link(link);
        iap2_link_unref(link);

        retVal = iap2_bt_enter_pairing();
        if (retVal) {
            hsk_print(E_LEVEL_ERROR, "<%s>: can't enter pairing mode", __func__);
            goto work_fail;
        }
    }

    if (m_ffsDir) {
        hsk_print(E_LEVEL_INFO,"<%s>: starting usb listener", __func__);

        listener_usb = iap2_usb_listen(m_acc, m_ffsDir);
        if (!listener_usb) {
            hsk_print(E_LEVEL_ERROR, "<%s>: can't listen on USB transport", __func__);
            goto work_fail;
        }

        hsk_print(E_LEVEL_INFO,"<%s>: open udc device", __func__);
        FILE *f = fopen(m_udcPath, "w");
        if (!f) {
            hsk_print(E_LEVEL_ERROR, "<%s>: fopen on UDC path", __func__);
            iap2_usb_close(listener_usb);
            goto work_fail;
        }

        fputs(m_udcName, f);
        fclose(f);
    }

    configured();
    m_isReady = true;

    hsk_print(E_LEVEL_INFO,"<%s>: run iap2", __func__);
    retVal = iap2_run(m_acc);

    iap2_unref(m_acc);
    goto work_ok;

work_fail:
    m_isReady = false;
    retVal = EXIT_FAILURE;

work_ok:
    return retVal;
}

void Mfi::newLinkInit(struct iap2_link *link)
{
    m_mfiHandle.common_new_link_init(link);
}

ssize_t Mfi::recv_sync(int sockfd, void *buf, size_t len, int flags)
{
    size_t ret = 0;

    while ( len )
    {
        ssize_t rc = recv(sockfd, buf, len, flags);
        if ( rc == -1 )
        {
            if ( errno == EAGAIN || errno == EWOULDBLOCK )
            {
                continue;
            }
            return rc;
        }
        if ( rc == 0 )
            return rc;

        buf = (uint8_t *)buf + rc;
        len -= rc;
        ret += rc;
    }

    return (ssize_t)ret;
}
