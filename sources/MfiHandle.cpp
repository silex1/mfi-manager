/**
 *      @ingroup    mfi-manager
 *      @brief      This class handles the mfi communication abstact
 *      @details    We have a thread running to make sure async handling of communication
 * 
 *      @file       MfiHandle.cpp
 *      @date       Dec 1, 2020
 *      @author     D. Borsdorf [borsdorfdo]
 *      @version    V0.1
 *
 *      @b  Copyright:
 *          Leica Camera AG (Wetzlar)
 *
 *      @b  Change history:
 *
 *      - Dec 1, 2020   D. Borsdorf     File created
 **/

/**************************************************************************/
//INCLUDES
/**************************************************************************/
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sched.h>
#include <inttypes.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/inotify.h>
#include <sys/poll.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <limits.h>

#include <leica/leica_types.h>
#include <leica/leica_core_if.h>
#include <leica/hsk_debug_linux_if.h>
#include <leica/ludwig_messaging_if_sys.h>

#include "MfiHandle.h"
#include "md5.h"


/**************************************************************************/
//DEFINITIONS
/**************************************************************************/
#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a) (sizeof(a)/sizeof(a[0]))
#endif
#define BLOCKSIZE       1024
#define BLOCK_COUNT     8
/**************************************************************************/
const char *MfiHandle::cameradir = NULL;
//const char *MfiHandle::bundleIdentifier = "com.leica-camera.leicaCamera";
const char *MfiHandle::bundleIdentifier = "net.grandcentrix.leica-mfi";

struct iap2_accessory_description MfiHandle::desc = {
    .name = "Leica MXX aka Ludi",//"Leica POC",
    .model_identifier = "Leica Ludwig Test",//"demo",
    .manufacturer = "Leica-Camera AG",//"grandcentrix",
    .serial_number = "88998877662",//"2342",
    .firmware_version = "1.0.0.1",
    .hardware_version = "1A7C",
//    .app_match_team_id = "com.leica-camera.leicaCamera",
    .current_language = "en",
    .supported_language = "en\0de\0it\0",
    .power_providing_capability = IAP2_POWER_PROVIDING_CAPABILITY_NONE,
    .maximum_current_drawn_from_device = 100
};


MfiHandle::MfiHandle()
{
     // TODO Auto-generated constructor stub
}

MfiHandle::~MfiHandle()
{
    // TODO Auto-generated destructor stub
}


int MfiHandle::wifi_credentials(struct iap2_link *link, const uint8_t *data, size_t len, void *arg)
{
    enum
    {
        WIFI_REQUEST_STATUS = 0,
        WIFI_SSID = 2,
        WIFI_PASSPHRASE = 3,
        __WIFI_MAX
    } wifi_idx;

    // static const struct iap2_ctrl_policy wifi_policy[__WIFI_MAX] =
    // {
    //     IAP2_CTRL_POLICY(WIFI_REQUEST_STATUS, INT8),
    //     IAP2_CTRL_POLICY(WIFI_SSID, UTF8),
    //     IAP2_CTRL_POLICY(WIFI_PASSPHRASE, UTF8)
    // };

    static const struct iap2_ctrl_policy wifi_policy[__WIFI_MAX] = {
        { .type = IAP2_CTRL_PARAM_INT8, .valid = true },
        { .type = IAP2_CTRL_PARAM_UTF8, .valid = true },
        { .type = IAP2_CTRL_PARAM_UTF8, .valid = true }
    };

    struct iap2_ctrl_out_param params[__WIFI_MAX ];

    hsk_print(E_LEVEL_INFO, "Got WiFi credentials");

    if ( iap2_ctrl_parse(params, wifi_policy, __WIFI_MAX, data, len) < 0 )
        return -1;

    if ( params[WIFI_REQUEST_STATUS].present )
        hsk_print(E_LEVEL_INFO, "status: %"PRIu64, params[WIFI_REQUEST_STATUS].data.num.v);

    if ( params[WIFI_SSID].present )
        hsk_print(E_LEVEL_INFO,"SSID: %s", params[WIFI_SSID].data.blob.p);
    
    if ( params[WIFI_PASSPHRASE].present )
        hsk_print(E_LEVEL_INFO,"passphrase: %s", params[WIFI_PASSPHRASE].data.blob.p);

    return 0;
}

int MfiHandle::sendall(int fd, const void *buf, size_t len)
{
    while ( len )
    {
        ssize_t ret;

        ret = send(fd, buf, len, MSG_NOSIGNAL);
        if ( ret < 0 )
        {
            if ( errno == EINTR )
                continue;

            return -1;
        }

        buf += ret;
        len -= ret;
    }

    return 0;
}

int MfiHandle::tx_sof(int fd)
{
    uint8_t sof = 0x7e;

    if ( send(fd, &sof, 1, MSG_NOSIGNAL) != 1 )
    {
        hsk_print(E_LEVEL_ERROR, "failed to send SOF");
        return -1;
    }

    return 0;
}

int MfiHandle::tx(int fd, const uint8_t *data, size_t len)
{
    uint8_t buf[128];
    size_t src_idx, dst_idx;

    src_idx = 0;

    while ( src_idx < len )
    {
        dst_idx = 0;

        while ( src_idx < len )
        {
            if ( data[src_idx] == 0x7e || data[src_idx] == 0x7d )
            {
                if ( dst_idx >= sizeof(buf) - 1 )
                    break;

                buf[dst_idx++] = 0x7d;
                buf[dst_idx++] = data[src_idx++] ^ 0x20;
            }
            else
            {
                if ( dst_idx >= sizeof(buf) )
                    break;

                buf[dst_idx++] = data[src_idx++];
            }
        }

        if ( sendall(fd, buf, dst_idx) < 0 )
        {
            hsk_print(E_LEVEL_ERROR, "sending escaped data failed");
            return -1;
        }
    }

    return 0;
}

ssize_t MfiHandle::read_timeout(int fd, uint8_t *buf, size_t len, int timeout)
{
    struct pollfd pfd;
    int ret;

    pfd.fd = fd;
    pfd.events = POLLIN | POLLERR;
    pfd.revents = 0;

    poll_restart:
    ret = poll(&pfd, 1, timeout);
    if ( ret < 0 )
    {
        if ( errno == EINTR )
            goto poll_restart;
        else
            return -1;
    }

    if ( ret == 0 )
        return -2;

    if ( pfd.revents & POLLERR )
        return -1;

    if ( pfd.revents & POLLIN )
        return recv(fd, buf, len, 0);

    return -1;
}

ssize_t MfiHandle::rx(int fd, uint8_t *buf, size_t len, int timeout)
{
    uint8_t *p = buf;
    bool escaped = false;

    while ( 1 )
    {
        size_t size = p - buf;
        if ( size >= len )
            return size;

        /*
         * This is incredibly inefficient, but it's the easiest way to
         * ensure we don't consume more than one message in this
         * context (we cannot push back on the fd and have no other way
         * of queueing here).
         *
         * As we're only reading a few bytes, this should be okay.
         */

        ssize_t ret;
        uint8_t c;
        ret = read_timeout(fd, &c, 1, timeout);
        if ( ret < 0 )
            return ret;

        if ( c == 0x7e )
        {
            if ( size > 0 )
                return size;
            else
                continue; // skip empty messages
        }

        if ( c == 0x7d )
        {
            escaped = true;
            continue;
        }

        if ( escaped )
            c ^= 0x20;

        escaped = false;

        *p++ = c;
    }
}

void MfiHandle::md5_buf(uint8_t *out, const uint8_t *data, size_t data_size)
{
    MD5_CTX ctx;

    MD5_Init(&ctx);
    MD5_Update(&ctx, data, data_size);
    MD5_Final(out, &ctx);
}

int MfiHandle::send_image(int fd, const char *name, const uint8_t *data, size_t data_size)
{
    uint8_t buf[64];
    size_t len;

    if ( data_size == 0 )
    {
        hsk_print(E_LEVEL_ERROR, "file is empty, not sending anything");
        return 0;
    }

    if ( tx_sof(fd) < 0 )
        return -1;

    if ( tx(fd, (uint8_t*)name, strlen(name) + 1) < 0 )
        return -1;

    len = 0;
    buf[len++] = data_size >> 24;
    buf[len++] = data_size >> 16;
    buf[len++] = data_size >> 8;
    buf[len++] = data_size;

    size_t md5_offset = len;
    md5_buf(&buf[md5_offset], data, data_size);
    len += MD5_DIGEST_LENGTH;

    if ( tx(fd, (uint8_t*)buf, len) < 0 )
        return -1;

    if ( tx(fd, data, data_size) < 0 )
        return -1;

    if ( tx_sof(fd) < 0 )
        return -1;

    hsk_print(E_LEVEL_INFO,"pushed out data, waiting for ack");
    // await ACK
    uint8_t ackbuf[1 + MD5_DIGEST_LENGTH];
    ssize_t ret = rx(fd, ackbuf, sizeof(ackbuf), 200);

    if ( ret < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "reading to ackbuf failed, error is %zd", ret);
        return -2;
    }

    hsk_print(E_LEVEL_DEBUG,"got ack data (%p / %zu):", ackbuf, ret);

    if ( ackbuf[0] != 0x01 )
    {
        hsk_print(E_LEVEL_ERROR, "status byte is 0x%02x, transfer failed", ackbuf[0]);
        return -2;
    }

    if ( memcmp(&buf[md5_offset], &ackbuf[1], MD5_DIGEST_LENGTH) != 0 )
    {
        hsk_print(E_LEVEL_ERROR, "checksum mismatch, transfer failed");
        return -2;
    }

    hsk_print(E_LEVEL_INFO,"transfer successful");

    return 0;
}

uint8_t * MfiHandle::slurp_file(const char *name, size_t *len)
{
    struct stat s;

    if ( stat(name, &s) < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "stat() failed");
        return NULL;
    }

    uint8_t *data = static_cast<uint8_t *> (malloc(s.st_size));
    if ( !data )
        return NULL;

    FILE *f = fopen(name, "r");
    if ( !f )
    {
        free(data);
        return NULL;
    }

    size_t ret;
    ret = fread(data, 1, s.st_size, f);

    if ( ret != (size_t)s.st_size )
    {
        hsk_print(E_LEVEL_ERROR, "short read");

        fclose(f);
        free(data);

        return NULL;
    }

    if ( len )
        *len = s.st_size;

    return data;
}

bool MfiHandle::is_suffix(const char *string, const char *suffix)
{
    size_t str_len = strlen(string);
    size_t suffix_len = strlen(suffix);

    if ( str_len < suffix_len )
        return false;

    return strcmp(&string[str_len - suffix_len], suffix) == 0;
}

void* MfiHandle::eap_thread(void *arg)
{
    int sock = (int)(uintptr_t)arg;
    int cnt = 0;
    uint8_t *data;

    hsk_print(E_LEVEL_DEBUG,"thread starting");

    hsk_print(E_LEVEL_DEBUG,"wait a little bit");
    sleep(10);

    data = static_cast<uint8_t *>( malloc( sizeof(uint8_t) * BLOCKSIZE * BLOCK_COUNT ) );
    if ( !data ) {
        hsk_print(E_LEVEL_ERROR,"<%s> can not allocate memmory!", __func__);
        goto out;
    }

    memset(data, 0xDB, (BLOCKSIZE * BLOCK_COUNT));

    while ( 1 )
    {
        int ret;
        do
        {
            hsk_print(E_LEVEL_INFO,"sending data");
            ret = send_image(sock, "TESTFILE-LEICA-CAMERA\0", data, (BLOCKSIZE * BLOCK_COUNT));
        }
        while ( ret == -2 ); // timeout, retry

        cnt+= BLOCK_COUNT;
        hsk_print(E_LEVEL_INFO,"transfered <%d>KB", cnt);

        if ( ret )
        {
            // other failure
            hsk_print(E_LEVEL_ERROR, "critical failure");
            break;
        }
    }

    out:
    close(sock);

    free(data);

    hsk_print(E_LEVEL_DEBUG,"thread exiting");

    return NULL;
}

int MfiHandle::new_eap_cb(struct iap2_link *link, const char *proto, int fd)
{
    hsk_print(E_LEVEL_INFO,"new '%s' session on fd %d", proto, fd);

    pthread_t thread;
    if ( pthread_create(&thread, NULL, eap_thread, (void*)(uintptr_t)fd) < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "creating thread");
        return -1;
    }

    if ( pthread_detach(thread) < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "detaching thread");
        return -1;
    }

    return 0;
}

void MfiHandle::common_new_link(struct iap2_link *link)
{
    hsk_print(E_LEVEL_INFO,"New link, requesting WiFi credentials");
    iap2_ctrl_send(link, iAP2Msg_Acc_RequestWiFiInformation, NULL, 0);

    struct iap2_accessory *acc = iap2_link_get_accessory(link);
    struct iap2_transport *transport_current = iap2_link_get_transport(link);
    if ( transport_current && iap2_transport_get_type(transport_current) == IAP2_TRANSPORT_TYPE_USB )
    {
        struct iap2_transport *transport_bt = iap2_get_transport_by_type(acc, IAP2_TRANSPORT_TYPE_BT);

        if ( transport_bt )
        {
            hsk_print(E_LEVEL_INFO,"sending btci");
            uint16_t bt_id = iap2_transport_get_id(transport_bt);

            struct iap2_ctrl_param g1_params[2];
            iap2_ctrl_param_int(&g1_params[0], 0, 2, bt_id);
            iap2_ctrl_param_int(&g1_params[1], 1, 1, true);

            struct iap2_ctrl_param params[1];
            iap2_ctrl_param_group(&params[0], 0, g1_params, ARRAY_SIZE(g1_params));
            iap2_ctrl_send(link, iAP2Msg_Acc_BluetoothComponentInformation, params, ARRAY_SIZE(params));
        }
    }
}

void MfiHandle::common_new_link_init(struct iap2_link *link)
{
    hsk_print(E_LEVEL_INFO,"New link, requesting app launch");
    struct iap2_ctrl_param g1_params[2];
    iap2_ctrl_param_string(&g1_params[0], 0, bundleIdentifier);
    iap2_ctrl_param_enum(&g1_params[1], 1, IAP2_APP_LAUNCH_ACTION_ALERT);

    iap2_ctrl_send(link, iAP2Msg_Acc_RequestAppLaunch, g1_params, ARRAY_SIZE(g1_params));

    struct iap2_accessory *acc = iap2_link_get_accessory(link);
    struct iap2_transport *transport_current = iap2_link_get_transport(link);
    if ( transport_current && iap2_transport_get_type(transport_current) == IAP2_TRANSPORT_TYPE_USB )
    {
        struct iap2_transport *transport_bt = iap2_get_transport_by_type(acc, IAP2_TRANSPORT_TYPE_BT);

        if ( transport_bt )
        {
            hsk_print(E_LEVEL_INFO,"sending btci");
            uint16_t bt_id = iap2_transport_get_id(transport_bt);

            struct iap2_ctrl_param g2_params[2];
            iap2_ctrl_param_int(&g2_params[0], 0, 2, bt_id);
            iap2_ctrl_param_int(&g2_params[1], 1, 1, true);

            struct iap2_ctrl_param params2[1];
            iap2_ctrl_param_group(&params2[0], 0, g2_params, ARRAY_SIZE(g2_params));
            iap2_ctrl_send(link, iAP2Msg_Acc_BluetoothComponentInformation, params2, ARRAY_SIZE(params2));
        }
    }
}

int MfiHandle::bt_conn_update(struct iap2_link *link, const uint8_t *data, size_t len, void *arg)
{
    hsk_print(E_LEVEL_DEBUG,"%s", __func__);

    return 0;
}

struct iap2_accessory *MfiHandle::create_demo(const char *auth_path, const char *_cameradir,
                                   void (*new_link_cb)(struct iap2_link *link))
{
    struct iap2_accessory *acc;
    hsk_print(E_LEVEL_INFO, "[create_demo] creating accessory for demo");

    // XXX: this makes this code unable to support more than one instance
    cameradir = _cameradir;

    acc = iap2_new(&desc, auth_path, new_link_cb, new_eap_cb, NULL);
    if ( !acc )
    {
        hsk_print(E_LEVEL_ERROR, "creating accessory failed");
        return NULL;
    }

    


    // See MFi Spec §46
    iap2_ctrl_add_cb(acc, iAP2Msg_Dev_WiFiInformation, MfiHandle::wifi_credentials, NULL);
//    iap2_ctrl_add_cb(acc, iAP2Msg_Dev_BluetoothConnectionUpdate, bt_conn_update, NULL);

    iap2_ctrl_register_sent(acc, iAP2Msg_Acc_RequestAppLaunch);
    iap2_ctrl_register_sent(acc, iAP2Msg_Acc_RequestWiFiInformation);
//    iap2_ctrl_register_sent(acc, iAP2Msg_Acc_BluetoothComponentInformation);
//    iap2_ctrl_register_sent(acc, iAP2Msg_Acc_StartBluetoothConnectionUpdates);
//    iap2_ctrl_register_sent(acc, iAP2Msg_Acc_StopBluetoothConnectionUpdates);


    //iap2_register_ea_protocol(acc, "com.bluegiga.iwrap", IAP2_EA_MATCH_ACTION_OPTIONAL);
    //old mfi poc
    iap2_register_ea_protocol(acc, "net.grandcentrix.leica-mfi", IAP2_EA_MATCH_ACTION_OPTIONAL);
    //iap2_register_ea_protocol(acc, "com.leica-camera.leica-mfi", IAP2_EA_MATCH_ACTION_OPTIONAL);

    return acc;
}
