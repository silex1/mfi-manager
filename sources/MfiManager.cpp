/**
*       @ingroup    con_mfi_grp
*       @brief      MFI Manager
*       @details     This class is responsible for controlling the communication
*                    via the MFI and the OS.
*
*       @file        MfiManager.cpp
*       @date        24.11.2020
*       @author      J. Walter
*       @version     0.1
*
*       @b Copyright:
*          Leica Camera AG (Wetzlar)
*
*       @b Change history:
*
*     - 24.11.2020   J. Walter       File created.
**/


/**************************************************************************/
//INCLUDES
/**************************************************************************/
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <string>

#include "MfiManager.h"

#include <leica/lmt_common_if.h>
#include <leica/ludwig_messaging_if_sys.h>
#include <leica/hsk_debug_linux_if.h>

/**************************************************************************/
//NAMESPACES
/**************************************************************************/
/**************************************************************************/
//ENUMERATIONS
/**************************************************************************/

/**************************************************************************/
//DEFINITIONS
/**************************************************************************/
Mfi* MfiManager::m_pMFI = NULL;
LeicaMessageQueueService* MfiManager::m_mqTask = NULL;
LeicaMessageQueueService* MfiManager::m_mqData = NULL;


MfiManager::MfiManager()
    : m_ShouldRun(true), m_USB_ConnectionState(E_CON_SYS_MESSAGE_USB_CONNECTION_UNDEFINED), m_EnableMFI_Protocol(false)
{
    hsk_print(E_LEVEL_INFO, "MfiManager created.");
    m_mqTask = new LeicaMessageQueueService(E_LEICA_CORE_ENDPOINT_CON_MFI_TASK);
    m_mqData = new LeicaMessageQueueService(E_LEICA_CORE_ENDPOINT_CON_MFI_TASK_DATA);
}

MfiManager::~MfiManager()
{
    // remove data
    hsk_print(E_LEVEL_INFO, "<%s>", __func__);

    deleteMfiInstance();
    deleteMQInstance();
}

void MfiManager::run(void)
{
    hsk_print(E_LEVEL_INFO, "MfiManager runs");

    while ( m_ShouldRun )
    {
        T_LEICA_CORE_MESSAGE *received_message = NULL;

        hsk_print(E_LEVEL_INFO, "MfiManager::run() -> Wait for msg");

        E_LC_RETURN SystemError = leica_core_ReceiveMessage(&received_message, E_LEICA_CORE_ENDPOINT_CON_MFI_TASK);
        if ( SystemError == E_LC_RETURN_OK )
        {
            bool bRetVal = false;
            const u_int32_t messageId = *((const u_int32_t*)received_message->pData);
            hsk_print(E_LEVEL_INFO, "MfiManager: messageId (0x%08X) received_message->pData(%p)", messageId, received_message->pData);
            switch ( messageId & (D_CON_BASE_MASK | D_CON_TASK_MASK) )
            {
                // TODO: Das hier an mfi anpassen. handleMTPCmdMsg ist ggf. auch unpassend ---
                case CON_DRIVER_COMMAND_BASE:       // USB driver messages              0xBB030000
                {
                    bRetVal = handleDriverCmdMsg(received_message);
                    break;
                }
                case LINUX_MANAGER_MESSAGE_BASE:    // LMT STM messages                 0xBB000000
                {
                    bRetVal = handleStatemachineMsg(received_message);
                    break;
                }
                case CON_CON_COMMAND_BASE:          // Connectivity Manager messages    0xBB040000
                {
                    bRetVal = handleConCtrlMsg(received_message);
                    break;
                }
                default:
                {
                    hsk_print(E_LEVEL_ERROR, "Unknown E_LC_CMD_BASE_CON command (0x%08x) received", messageId);
                }
            }

            if ( !bRetVal )
            {
                hsk_print(E_LEVEL_WARNING, "MfiManager: message(0x%08X) not handled", messageId);
            }
            leica_core_FreeMessage(received_message, E_LEICA_CORE_ENDPOINT_CON_MFI_TASK);
        }
        else
        {
            hsk_print(E_LEVEL_ERROR, "SystemError(%d)", SystemError);
            break;
        }
        sleep (1);
    }
}

bool MfiManager::createMfiInstance(void)
{
    bool bRetVal = false;

    hsk_print(E_LEVEL_INFO, "MfiManager: createMfiInstance");

    try
    {
        m_pMFI = Mfi::Instance();
        m_pMFI->setConfiguredCallback(&MfiManager::configuredCallback);
        bRetVal = true;
    }
    catch(const std::string& ex)
    {
        hsk_print(E_LEVEL_ERROR, "handleLinuxMsg: Caught error (%s)", ex.c_str());
    }
    catch ( ... )
    {
        hsk_print(E_LEVEL_ERROR, "handleLinuxMsg: Creation of Mfi failed: catch called!!!");
        m_pMFI = nullptr;
    }
    return bRetVal;
}

bool MfiManager::deleteMQInstance(void)
{
    bool bRetValTask = false;
    bool bRetValData = false;

    try
    {
        if ( m_mqTask != NULL )       // Delete MTP-USB protocol class if still available
        {
            hsk_print(E_LEVEL_ERROR, "<%s>: m_mqTask != NULL. Delete.",__func__);
            delete m_mqTask;
            m_mqTask = NULL;
            bRetValTask = true;
        }
        if ( m_mqData != NULL)       // Delete MTP-USB protocol class if still available
        {
            hsk_print(E_LEVEL_ERROR, "<%s>: m_mqData != NULL. Delete.",__func__);
            delete m_mqData;
            m_mqData = NULL;
            bRetValData = true;
        }
    }
    catch ( ... )
    {
        hsk_print(E_LEVEL_ERROR, "<%s>: Delete of MQ Instance failed: catch called!!!",__func__);
        m_mqTask = NULL;
        m_mqData = NULL;
    }
    return (bRetValTask & bRetValData);
}

bool MfiManager::deleteMfiInstance(void)
{
    bool bRetVal = false;

    try
    {
        if ( m_pMFI != NULL )       // Delete MTP-USB protocol class if still available
        {
            hsk_print(E_LEVEL_ERROR, "deleteMfiInstance: m_pMFI != NULL. Delete.");
            delete m_pMFI;
            m_pMFI = NULL;
            bRetVal = true;
        }
        else
        {
            hsk_print(E_LEVEL_ERROR, "deleteMfiInstance: m_pMFI already deleted.");
        }
    }
    catch ( ... )
    {
        hsk_print(E_LEVEL_ERROR, "deleteMfiInstance: Delete of Mfi failed: catch called!!!");
        m_pMFI = NULL;
    }
    return bRetVal;
}

void MfiManager::configuredCallback(void)
{
    T_SYS_STATE_CHANGE_MESSAGE_DATA_LINUX data = {.MessageId = E_SYS_MESSAGE_ID_EVENT, .Event = E_SYS_EVENT_CON_MFI_ENABLED, .Parameter1 = 0, .Parameter2 = 0};
    leica_core_SendMessage(E_LEICA_CORE_ENDPOINT_STM_TASK, &data, sizeof(data));
}

bool MfiManager::handleConCtrlMsg(const T_LEICA_CORE_MESSAGE *_receivedMessage)
{
    bool bRetVal = false;

    const u_int32_t messageId = *((const u_int32_t*)_receivedMessage->pData);
    switch ( messageId )
    {
        case E_CON_MESSAGE_ID_LINUX_SYSTEM:         //  0xBB040001
        {
            const E_CON_SYS_MESSAGE systemMessage = ((T_CON_CONNECTIONS_MESSAGE_DATA *)_receivedMessage->pData)->SystemMessage;
            if ( m_USB_ConnectionState != systemMessage )
            {
                m_USB_ConnectionState = systemMessage;
                switch ( systemMessage )
                {
                    case E_CON_SYS_MESSAGE_USB_CONNECTION_CONNECTED:
                    {
                        bRetVal = true;
                        hsk_print(E_LEVEL_INFO, "handleConCtrlMsg: USB Connected");
                        //sys_SendEvent(E_SYS_EVENT_CON_CONNECTED_CABLE);
                        break;
                    }
                    case E_CON_SYS_MESSAGE_USB_CONNECTION_CONFIGURED:
                    {
                        bRetVal = true;
                        hsk_print(E_LEVEL_INFO, "handleConCtrlMsg: USB Configured");
                        // if ( m_EnableMFI_Protocol )
                        // {
                        //     hsk_print(E_LEVEL_INFO, "handleConCtrlMsg: Start MFI protocol");
                        //     bRetVal = createMfiInstance();
                        // }
                        // sys_SendEvent(E_SYS_EVENT_CON_MFI_CONFIGURED);
                        sys_SendEvent(E_SYS_EVENT_CON_CONNECTED_CABLE);
                        break;
                    }
                    case E_CON_SYS_MESSAGE_USB_CONNECTION_DISCONNECTED:
                    {
                        bRetVal = true;
                        hsk_print(E_LEVEL_INFO, "handleConCtrlMsg: USB Disconnected");
                        //bRetVal = deleteMfiInstance();
                        sys_SendEvent(E_SYS_EVENT_CON_DISCONNECTED_CABLE);
                        break;
                    }
                    default:
                    {
                        // print error
                        hsk_print(E_LEVEL_ERROR, "handleConCtrlMsg: Unknown LINUX_SYSTEM message(0x%08X)", systemMessage);
                    }
                }
            }
            else
            {
                hsk_print(E_LEVEL_WARNING, "handleConCtrlMsg: Repeated LINUX_SYSTEM message(0x%08X) == m_USB_ConnectionState", systemMessage);
            }
            break;
        }
        default:
        {
            // print error
            hsk_print(E_LEVEL_ERROR, "handleConCtrlMsg: Unknown CON_COMMAND_BASE message(0x%08X)", messageId);
        }
    }
    return bRetVal;
}

bool MfiManager::handleDriverCmdMsg(const T_LEICA_CORE_MESSAGE *_receivedMessage)
{
    bool bRetVal = true;
// TODO: hier kommen Meldungen der MFI Protokoll-Klasse oder des Treibers rein
   const u_int32_t messageId = *((const u_int32_t*)_receivedMessage->pData);
//
//    if ( messageId == E_CON_USB_PTP_COMMAND_READ_USB_PACKET )            // Read MTP Request data and handle it.
//    {
//        bRetVal = handleMTPCmdMsg_Requests();
//    }
//    else
//    {
//        // print error
//        hsk_print(E_LEVEL_ERROR, "(PTP): Unknown USB_COMMAND_BASE message(0x%08X)", messageId);
//        bRetVal = false;
//    }
    hsk_print(E_LEVEL_INFO, "<%s>: Unknown USB_COMMAND_BASE message(0x%08X)", __func__, messageId);
    return bRetVal;
}

bool MfiManager::handleMTPEvtMsg(const T_LEICA_CORE_MESSAGE *_receivedMessage)
{
    bool bRetVal = true;
// TODO: Hier werden MTP-Evens vom RTOS/CON an die MFI Protokoll-Klasse, sprich den MFI-Tunnel weitergegeben. Ist noch nicht 100% Final.
//    const T_CON_PTP_OPERATION_MSG * operationMsg = (const T_CON_PTP_OPERATION_MSG *)_receivedMessage->pData;
//
//    switch ( (E_CON_USB_PTP_MESSAGE_ID)operationMsg->MsgID )
//    {
//        case E_CON_USB_PTP_COMMAND_EVT_OBJECTADDED:
//        {
//            if ( operationMsg->Response.DataLength == 1 )
//            {
//                m_pMTP_USB->PTPStd_Evt_ObjectAdded(operationMsg->Response.Parameter1);
//            }
//            else
//            {
//                hsk_print(E_LEVEL_ERROR, "(PTP): Event ObjectAdded: ObjectHandle missing");
//            }
//            break;
//        }
//        default:
//        {
//            hsk_print(E_LEVEL_ERROR, "handleMTPMsg_Events: Unknown MTP Event message(0x%08X)", operationMsg->MsgID);
//            bRetVal = false;
//        }
//    }
    return bRetVal;
}

bool MfiManager::handleMTPCmdMsg_Requests(void)
{
// TODO: Hier wird die MFI-Protokoll-Klasse aufgerufen. handleOperation() als abgelittene Basisklassenfunktion übernimmt den kompletten Ablauf für ein MTP-Request der App
//    hsk_print(E_LEVEL_DEBUG, "E_CON_USB_PTP_COMMAND_READ_USB_PACKET");
//
//    bool bRetVal = m_pMTP_USB->handleOperation();
//
//    if ( bRetVal == false )
//    {
//        hsk_print(E_LEVEL_ERROR, "(PTP): Unknown MTP operation(0x%04X)", m_pMTP_USB->getOperationCode());
//    }
//    m_pMTP_USB->ReleaseOperationHandlingSem();
//    return bRetVal;
    return true;
}

bool MfiManager::handleStatemachineMsg(const T_LEICA_CORE_MESSAGE *_receivedMessage)
{
    bool bRetVal = true;
    const u_int32_t messageId = *((const u_int32_t*)_receivedMessage->pData);

    switch ( messageId )
    {
        case E_CON_MESSAGE_ID_STATE_CHANGE_REQ:
        {
            const E_SYS_STATE futureState = ((T_CON_STATE_CHANGE_MSG *)_receivedMessage->pData)->FutureState;
            T_CON_STATE_CHANGE_MSG responseMessage = {.MsgID = E_CON_MESSAGE_ID_STATE_CHANGE_ACK, .FutureState = futureState};

            // Do nothing

            hsk_print(E_LEVEL_INFO, "Received STATE_CHANGE State(%d), Send Message FutureState(%d)", futureState, responseMessage.FutureState);
            leica_core_SendMessage(E_LEICA_CORE_ENDPOINT_CON_TASK, (void*)&responseMessage, sizeof(T_CON_STATE_CHANGE_MSG));
            break;
        }
        case E_CON_MESSAGE_ID_PROTOCOL_ACTION:
        {
            const T_CON_PROTOCOL_ACTION_MSG *message = (T_CON_PROTOCOL_ACTION_MSG *)_receivedMessage->pData;
            if ( message->Protocol == E_LMT_PROTOCOL_MFI )
            {
                if ( message->Action == E_LMT_PROTOCOL_ENABLE )
                {
                    m_EnableMFI_Protocol = true;
                    hsk_print(E_LEVEL_INFO, "Enable MTP protocol");
                    // todo DB: Prüfung, ob Verbindung erlaubt ist
                    if ( /* m_pMFI->isReady() */ true )
                    {
                        // if ( m_USB_ConnectionState == E_CON_SYS_MESSAGE_USB_CONNECTION_CONFIGURED )
                        // {
                            hsk_print(E_LEVEL_ERROR, "handleStatemachineMsg: start mfi instance now!");
                            bRetVal = m_pMFI->start();
                        // }
                        // else
                        // {
                        //     hsk_print(E_LEVEL_ERROR, "handleStatemachineMsg: USB connection not configured, it is<%d>", (int)m_USB_ConnectionState);
                        // }
                    }
                    else
                    {
                        hsk_print(E_LEVEL_ERROR, "handleStatemachineMsg: USB gadget not available");
                    }
                }
                else
                {
                    if (m_EnableMFI_Protocol) {
                        hsk_print(E_LEVEL_INFO, "Disable MFI protocol");
                        m_EnableMFI_Protocol = false;
    // todo JW high: Hier eine Info an die App senden, dass die Verbindung von der Kamera her abgebaut wird.
    //                    if ( (m_pMTP_USB != nullptr) && (m_pMTP_USB->isSessionOpen()) )
    //                    {
    //                        // todo JW high: Hier den PTP Event (0x400B) senden. Kein RTOS-Zugriff nötig!
    //                    }
                        bRetVal = m_pMFI->terminate();

                        m_USB_ConnectionState = E_CON_SYS_MESSAGE_USB_CONNECTION_UNDEFINED;     // While MTP is disabled the Connection state is handled by the Conny.
                        sys_SendEvent(E_SYS_EVENT_CON_MFI_DEACTIVATED);
                    } else {
                        hsk_print(E_LEVEL_INFO, "MFI protocol not enabled");
                    }
                }
            }
            else
            {
                hsk_print(E_LEVEL_ERROR, "handleStatemachineMsg: Protocol(%d) not supported.", message->Protocol);
            }
            break;
        }
        default:
        {
            hsk_print(E_LEVEL_ERROR, "handleStatemachineMsg: Unknown statemachine message(0x%08X)", messageId);
            bRetVal = false;
        }
    }
    return bRetVal;
}

void MfiManager::initialise()
{
    T_CON_STATE_CHANGE_MSG message = {.MsgID = E_CON_MESSAGE_ID_STATE_CHANGE_ACK, .FutureState = E_SYS_STATE_prerun};
    leica_core_SendMessage(E_LEICA_CORE_ENDPOINT_CON_TASK, (void*)&message, sizeof(T_CON_STATE_CHANGE_MSG));
    createMfiInstance();
}

int MfiManager::requestMessageWrapper(void)
{
// TODO: Anpassen an MFI treiber
    // Send message from protocol class to manager.
    E_CON_USB_PTP_MESSAGE_ID eMessageID = E_CON_USB_PTP_COMMAND_READ_USB_PACKET;
    leica_core_SendMessage(E_LEICA_CORE_ENDPOINT_CON_MFI_TASK, (void*)&eMessageID, sizeof(E_CON_USB_PTP_MESSAGE_ID));
    return 0;
}

E_LC_RETURN MfiManager::sendReceiveFromLmtDataQueue(const T_CON_PTP_OPERATION_MSG *_pSendMessage, T_LEICA_CORE_MESSAGE **_pReceivedMessage)
{
    E_LC_RETURN eRetVal = leica_core_SendMessageFromSpecificSenderEndpointLinux(E_LEICA_CORE_NODE_CA7_LINUX, E_LEICA_CORE_ENDPOINT_CON_MFI_TASK_DATA, E_LEICA_CORE_ENDPOINT_LMT_TASK, (void*)_pSendMessage,
                                                                                sizeof(T_CON_PTP_OPERATION_MSG));
    if ( eRetVal == E_LC_RETURN_OK )
    {
        int32_t DisconnectPollingTO = 0xFFFFFFFF;

        hsk_print(E_LEVEL_INFO, "MfiManager::sendReceiveFromLmtDataQueue() -> Wait for answer msg from LMT");

        eRetVal = leica_core_ReceiveMessageWithTimeout(_pReceivedMessage, E_LEICA_CORE_ENDPOINT_CON_MFI_TASK_DATA, DisconnectPollingTO);
        if ( eRetVal != E_LC_RETURN_OK )
        {
            hsk_print(E_LEVEL_ERROR, "MfiManager::sendReceiveFromLmtDataQueue: SystemError(%d)", eRetVal);
        }
    }
    else
    {
        hsk_print(E_LEVEL_ERROR, "MfiManager::sendReceiveFromLmtDataQueue(): Send msg to LMT failed");
    }
    return eRetVal;
}

E_LC_RETURN MfiManager::sys_SendEvent(const E_SYS_EVENT _Event)
{
    T_SYS_STATE_CHANGE_MESSAGE_DATA_LINUX data = {.MessageId = E_SYS_MESSAGE_ID_EVENT, .Event = _Event, .Parameter1 = 0, .Parameter2 = 0};
    return leica_core_SendMessage(E_LEICA_CORE_ENDPOINT_STM_TASK, &data, sizeof(data));
}

void MfiManager::SignalHandler(int _sig)
{
    hsk_print(E_LEVEL_ERROR, "<%s>: SIGNAL(%s)",__func__,strsignal(_sig));
    deleteMfiInstance();
    deleteMQInstance();
}
