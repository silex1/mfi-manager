#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <leica/leica_types.h>
#include <leica/hsk_debug_linux_if.h>

#include "iap2.h"
#include "iap2-priv.h"
#include "mfi-auth.h"

static void *get_cert_thread(void *arg)
{
    struct iap2_link *link = (struct iap2_link*)arg;
    struct auth_handle *auth = NULL;
    static uint8_t cert[MFI_MAX_CERT_SIZE];

#if IAP2_DEBUG > 2
    hsk_print(E_LEVEL_INFO, "[get_cert_thread] prepare cert thread");
#endif

    pthread_mutex_lock(&link->acc->auth_mutex);

    if ( link->acc->auth_cert_size )
        goto out;

#if IAP2_DEBUG > 2
    hsk_print(E_LEVEL_INFO, "[get_cert_thread] open auth device");
#endif
    auth = mfi_auth_open(link->acc->auth_dev);
    if ( !auth )
    {
        hsk_print(E_LEVEL_ERROR, "failed to open mfi device");
        iap2_link_shutdown(link);

        goto out;
    }

#if IAP2_DEBUG > 2
    hsk_print(E_LEVEL_INFO, "[get_cert_thread] read cert");
#endif
    ssize_t ret = mfi_get_cert(auth, cert);
    if ( ret < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "failed to get mfi cert");
        iap2_link_shutdown(link);

        goto out;
    }

    memcpy(link->acc->auth_cert, cert, ret);
    __sync_synchronize();
    link->acc->auth_cert_size = ret;
#if IAP2_DEBUG > 2
    hsk_print(E_LEVEL_INFO, "[get_cert_thread] cert<%s>",cert);
#endif

    struct iap2_ctrl_param param;
    iap2_ctrl_param_blob(&param, 0, cert, ret);
    iap2_ctrl_send(link, iAP2Msg_Acc_AuthenticationCertificate, &param, 1);

    out:
    if ( auth )
        mfi_auth_close(auth);

    pthread_mutex_unlock(&link->acc->auth_mutex);
    ref_down(&link->ref);
    return NULL;
}

int iap2_ctrl_RequestAuthenticationCertificate(struct iap2_link *link, const uint8_t *payload, size_t len, void *arg)
{
    if ( link->acc->auth_cert_size )
    {
#if IAP2_DEBUG > 2
    hsk_print(E_LEVEL_INFO, "[iap2_ctrl_RequestAuthenticationCertificate] request cert");
#endif
        struct iap2_ctrl_param param;

        __sync_synchronize();

        iap2_ctrl_param_blob(&param, 0, link->acc->auth_cert, link->acc->auth_cert_size);
        iap2_ctrl_send(link, iAP2Msg_Acc_AuthenticationCertificate, &param, 1);

        return 0;
    }
#if IAP2_DEBUG > 2
    else {
        hsk_print(E_LEVEL_INFO, "[iap2_ctrl_RequestAuthenticationCertificate] wrong vert size");
    }
#endif

    pthread_t thread;

    ref_up(&link->ref);
    if ( pthread_create(&thread, NULL, get_cert_thread, link) < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "Creating cert thread failed");
        ref_down(&link->ref);

        return -1;
    }

    if (pthread_detach(thread) == 0)
    {
        hsk_print(E_LEVEL_ERROR, "Detaching cert thread failed");
    }

    return 0;
}

struct ctrl_challenge
{
    struct iap2_link *link;
    uint8_t challenge[MFI_CHALLENGE_SIZE];
    size_t len;
};

static void *get_challenge_response_thread(void *arg)
{
    struct ctrl_challenge *ctx = (struct ctrl_challenge*)arg;
    uint8_t response[MFI_RESPONSE_SIZE];
#if IAP2_DEBUG > 2
    hsk_print(E_LEVEL_INFO, "[get_challenge_response_thread] calculating challenge response");
#endif

    pthread_mutex_lock(&ctx->link->acc->auth_mutex);

#if IAP2_DEBUG > 2
    hsk_print(E_LEVEL_INFO, "[get_challenge_response_thread] open mfi auth device");
#endif
    struct auth_handle *auth = mfi_auth_open(ctx->link->acc->auth_dev);
    if ( !auth )
    {
        hsk_print(E_LEVEL_ERROR, "failed to open mfi device");
        iap2_link_shutdown(ctx->link);

        goto out;
    }

#if IAP2_DEBUG > 2
    hsk_print(E_LEVEL_INFO, "[get_challenge_response_thread] transfer challenge");
#endif
    ssize_t ret = mfi_auth(auth, ctx->challenge, response);
    if ( ret < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "failed to get response to challenge");
        iap2_link_shutdown(ctx->link);

        goto out;
    }

#if IAP2_DEBUG > 2
    hsk_print(E_LEVEL_INFO, "[get_challenge_response_thread] send response to apple device");
#endif
    struct iap2_ctrl_param param;
    iap2_ctrl_param_blob(&param, 0, response, MFI_RESPONSE_SIZE);
    iap2_ctrl_send(ctx->link, iAP2Msg_Acc_AuthenticationResponse, &param, 1);

out:
    if ( auth )
        mfi_auth_close(auth);

    pthread_mutex_unlock(&ctx->link->acc->auth_mutex);
    ref_down(&ctx->link->ref);
    free(ctx);
    return NULL;
}

int iap2_ctrl_RequestAuthenticationChallengeResponse(struct iap2_link *link, const uint8_t *payload, size_t len, void *arg)
{
    enum
    {
        CHALLENGE,
        __CHALLENGE_MAX
    };

    static const struct iap2_ctrl_policy policy[] = { IAP2_CTRL_POLICY_SIZED(CHALLENGE, BLOB, MFI_CHALLENGE_SIZE) };
#if IAP2_DEBUG > 2
    hsk_print(E_LEVEL_INFO, "[iap2_ctrl_RequestAuthenticationChallengeResponse]");
#endif

    struct iap2_ctrl_out_param param[1];
    if ( iap2_ctrl_parse(param, policy, __CHALLENGE_MAX, payload, len) < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "parsing auth challenge payload failed");
        return -1;
    }

    struct ctrl_challenge *ctx;
    ctx = calloc(sizeof(*ctx), 1);
    if ( !ctx )
        return -1;

    ctx->link = link;
    ctx->len = len;
    memcpy(ctx->challenge, param[0].data.blob.p, MFI_CHALLENGE_SIZE);

    pthread_t thread;

    ref_up(&link->ref);
    if ( pthread_create(&thread, NULL, get_challenge_response_thread, ctx) < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "creating cert thread failed");
        ref_down(&link->ref);

        goto err;
    }

    if (pthread_detach(thread) == 0)
    {
        hsk_print(E_LEVEL_ERROR, "Detaching cert thread failed");
    }

    return 0;

    err:
    free(ctx);
    return -1;
}
