#define _GNU_SOURCE
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <inttypes.h>

#include <leica/leica_types.h>
#include <leica/hsk_debug_linux_if.h>

#include "iap2.h"
#include "iap2-priv.h"
#include "vec.h"
#include "io.h"

static ssize_t ctrl_params_len(const struct iap2_ctrl_param *params, int n_params)
{
    size_t params_len = 0;

    for (int i = 0; i < n_params; i++)
    {
        const struct iap2_ctrl_param *param = &params[i];

        params_len += 4;

        switch ( param->type )
        {
            case IAP2_CTRL_PARAM_BLOB:
                params_len += param->data.blob.l;
                break;
            case IAP2_CTRL_PARAM_ENUM:
            case IAP2_CTRL_PARAM_NUMERIC:
                params_len += param->data.num.l;
                break;
            case IAP2_CTRL_PARAM_GROUP:
                params_len += ctrl_params_len(param->data.group.p, param->data.group.n);
                break;
            default:
                hsk_print(E_LEVEL_ERROR, "Unknown payload type %d", param->type);
                return -1;
        }
    }

    return params_len;
}

static ssize_t ctrl_params_serialize(uint8_t *buf, const struct iap2_ctrl_param *params, int n_params)
{
    uint8_t *p = buf;

    for (int i = 0; i < n_params; i++)
    {
        const struct iap2_ctrl_param *param = &params[i];

        uint16_t param_len;
        switch ( param->type )
        {
            case IAP2_CTRL_PARAM_BLOB:
            {
                if ( param->data.blob.p )
                    memcpy(p + 4, param->data.blob.p, param->data.blob.l);
                param_len = param->data.blob.l;
                break;
            }
            case IAP2_CTRL_PARAM_ENUM:
            case IAP2_CTRL_PARAM_NUMERIC:
            {
                uint64_t val = param->data.num.v;

                for (int j = param->data.num.l - 1; j >= 0; j--)
                {
                    p[4 + j] = val & 0xFF;
                    val >>= 8;
                }

                param_len = param->data.num.l;
                break;
            }
            case IAP2_CTRL_PARAM_GROUP:
            {
                ssize_t ret = ctrl_params_serialize(p + 4, param->data.group.p, param->data.group.n);
                if ( ret < 0 )
                    return -1;

                param_len = ret;
                break;
            }
            default:
                hsk_print(E_LEVEL_ERROR, "Unknown payload type %d", param->type);
                return -1;
        }

        writeU16toBEpointer(param_len + 4, p);
        writeU16toBEpointer(param->id, p + 2);

        p += param_len + 4;
    }

    return p - buf;
}

int iap2_ctrl_send(struct iap2_link *link, enum iap2_msg_id id, const struct iap2_ctrl_param *params,
                               int n_params)
{
    uint8_t *buf, *p;
    unsigned int payload_len;
    IAP2RET iaprc;
    uint32_t actual;

    ssize_t params_len = ctrl_params_len(params, n_params);
    if ( params_len < 0 )
        return -1;

    payload_len = 6 + params_len;

    pthread_mutex_lock(&link->iap2_mutex);
    uint32_t max_payload_len = iAP2LinkGetMaxSendPayloadSize(link->run_loop->link);
    pthread_mutex_unlock(&link->iap2_mutex);

    if ( payload_len > max_payload_len )
    {
        hsk_print(E_LEVEL_ERROR, "Payload length (%d) > max payload length (%d)", payload_len, max_payload_len);
        return -1;
    }

    p = buf = malloc(payload_len);
    if ( !p )
        return -1;

    writeU16toBEpointer(0x4040, p);
    writeU16toBEpointer(payload_len, p + 2);
    writeU16toBEpointer(id, p + 4);
    p += 6;

    if ( ctrl_params_serialize(p, params, n_params) < 0 )
    {
        free(buf);
        return -1;
    }

    pthread_mutex_lock(&link->iap2_mutex);
    iaprc = iAP2LinkQueueSendData(link->run_loop->link, buf, payload_len, IAP2_CTRL_SESSION_ID, NULL, NULL, &actual);
    pthread_mutex_unlock(&link->iap2_mutex);

    free(buf);

    if ( iaprc )
    {
        hsk_print(E_LEVEL_ERROR, "Failed to send ctrl data: rc=%d actual=%d", iaprc, actual);
        return -1;
    }

    return 0;
}

void iap2_ctrl_param_blob(struct iap2_ctrl_param *p, uint16_t id, const uint8_t *blob, size_t len)
{
    p->id = id;
    p->type = IAP2_CTRL_PARAM_BLOB;
    p->data.blob.p = blob;
    p->data.blob.l = len;
}

void iap2_ctrl_param_string(struct iap2_ctrl_param *p, uint16_t id, const char *str)
{
    iap2_ctrl_param_blob(p, id, (uint8_t*)str, strlen(str) + 1);
}

void iap2_ctrl_param_enum(struct iap2_ctrl_param *p, uint16_t id, uint8_t val)
{
    p->id = id;
    p->type = IAP2_CTRL_PARAM_ENUM;
    p->data.num.v = val;
    p->data.num.l = 1;
}

void iap2_ctrl_param_int(struct iap2_ctrl_param *p, uint16_t id, int size, uint64_t val)
{
    p->id = id;
    p->type = IAP2_CTRL_PARAM_NUMERIC;
    p->data.num.v = val;
    p->data.num.l = size;
}

void iap2_ctrl_param_group(struct iap2_ctrl_param *p, uint16_t id, const struct iap2_ctrl_param *params, int n_params)
{
    p->id = id;
    p->type = IAP2_CTRL_PARAM_GROUP;
    p->data.group.p = params;
    p->data.group.n = n_params;
}

void iap2_ctrl_param_none(struct iap2_ctrl_param *p, uint16_t id)
{
    iap2_ctrl_param_blob(p, id, NULL, 0);
}

int iap2_ctrl_add_cb(struct iap2_accessory *acc, enum iap2_msg_id id, iap2_ctrl_cb cb, void *arg)
{
    struct iap2_ctrl_cb ctrl_cb;

    vec_foreach(acc->ctrl_cb, struct iap2_ctrl_cb*, cursor)
    {
        if (cursor->msg_id == id)
        {
            hsk_print(E_LEVEL_ERROR, "Control callback with message id 0x%04x already registered", (unsigned int)id);
            return -1;
        }
    }

    ctrl_cb.msg_id = id;
    ctrl_cb.cb = cb;
    ctrl_cb.arg = arg;

    return vec_append(acc->ctrl_cb, &ctrl_cb);
}

static int iap2_ctrl_StartIdentification(struct iap2_link *link, const uint8_t *payload, size_t len, void *arg);
static int iap2_ctrl_IdentificationAccepted(struct iap2_link *link, const uint8_t *payload, size_t len, void *arg);
static int iap2_ctrl_IdentificationRejected(struct iap2_link *link, const uint8_t *payload, size_t len, void *arg);
static int iap2_ctrl_nop(struct iap2_link *link, const uint8_t *payload, size_t len, void *arg)
{
    return 0;
}

static struct
{
    uint16_t msg_id;
    iap2_ctrl_cb cb;
} control_handlers[] = {
                        {iAP2Msg_Dev_RequestAuthenticationCertificate, iap2_ctrl_RequestAuthenticationCertificate},
                        {iAP2Msg_Dev_RequestAuthenticationChallengeResponse, iap2_ctrl_RequestAuthenticationChallengeResponse},
                        {iAP2Msg_Dev_AuthenticationSucceeded, iap2_ctrl_nop},
                        {iAP2Msg_Dev_StartIdentification, iap2_ctrl_StartIdentification},
                        {iAP2Msg_Dev_IdentificationAccepted, iap2_ctrl_IdentificationAccepted},
                        {iAP2Msg_Dev_IdentificationRejected, iap2_ctrl_IdentificationRejected},
                        {iAP2Msg_Dev_StartExternalAccessoryProtocolSession, iap2_ctrl_StartExternalAccessoryProtocolSession},
                        {iAP2Msg_Dev_StopExternalAccessoryProtocolSession, iap2_ctrl_StopExternalAccessoryProtocolSession},
                        {0, NULL}
};

static int ensure_space(struct iap2_ctrl_param **params, int n_params, int *params_space)
{
    if ( n_params < *params_space )
        return 0;

    int new_size = *params_space * 2;
    struct iap2_ctrl_param *new = realloc(*params, sizeof(*params) * new_size);
    if ( !new )
        return -1;

    *params = new;
    *params_space = new_size;

    return 0;
}

static bool filter_ctrl_msg_id(struct iap2_accessory *acc, uint16_t msg_id)
{
    /*
     * Ignore authentification and identification handlers, as specified in
     * Section 20.2.2 of the spec (R29).
     */
    if ( (msg_id & 0xFF00) == 0xAA00 ||
         (msg_id & 0xFF00) == 0x1D00 )
        return false;

    // We must only advertise EA capabilities if we have at least one protocol registered.
    if ( (msg_id & 0xFF00) == 0xEA00 && !iap2_ea_is_configured(acc) )
        return false;

    return true;
}

static int iap2_ctrl_StartIdentification(struct iap2_link *link, const uint8_t *payload, size_t len, void *arg)
{
    struct iap2_ctrl_param *params;
    int n_params, params_space;
    int ret = -1;

    struct iap2_accessory *acc = link->acc;

    hsk_print(E_LEVEL_INFO, "[iap2_ctrl_StartIdentification] !!!!!!!!!!");


#define ENSURE_SPACE(label) do { if (ensure_space(&params, n_params, &params_space) < 0) goto label; } while (0)

    // there are 12 mandatory parameters, reserve space for them and a couple more
    params_space = 16;

    params = calloc(sizeof(*params), params_space);
    if ( !params )
        return -1;

    n_params = 0;
    iap2_ctrl_param_string(&params[n_params++], 0, acc->description->name);
    iap2_ctrl_param_string(&params[n_params++], 1, acc->description->model_identifier);
    iap2_ctrl_param_string(&params[n_params++], 2, acc->description->manufacturer);
    iap2_ctrl_param_string(&params[n_params++], 3, acc->description->serial_number);
    iap2_ctrl_param_string(&params[n_params++], 4, acc->description->firmware_version);
    iap2_ctrl_param_string(&params[n_params++], 5, acc->description->hardware_version);

    uint8_t *p;

    /* We mustn't include the auth/id messages in the sent messages set,
     * but we don't send any other messages ourselves right now.
     * Thus, we can simply pass on the user-supplied message set.
     */

    uint8_t *sent_msgs = calloc(sizeof(uint16_t), vec_nmemb(acc->sent_msgs));
    if ( !sent_msgs )
        goto err_recvd;

    p = sent_msgs;
    vec_foreach(acc->sent_msgs, uint16_t*, msg)
    {
        writeU16toBEpointer(*msg, p);
        p += 2;
    }

    iap2_ctrl_param_blob(&params[n_params++], 6, sent_msgs, p - sent_msgs);

    // build received messages set
    int n_recvd_msgs = 0;

    // count local handlers
    for (size_t i = 0; control_handlers[i].cb; i++)
    {
        if ( !filter_ctrl_msg_id(acc, control_handlers[i].msg_id) )
            continue;

        n_recvd_msgs++;
    }

    // count user handlers
    n_recvd_msgs += vec_nmemb(acc->ctrl_cb);

    uint8_t *recvd_msgs = calloc(2, n_recvd_msgs);
    p = recvd_msgs;
    if ( !recvd_msgs )
        goto err;

    // add local handlers
    for (size_t i = 0; control_handlers[i].cb; i++)
    {
        if ( !filter_ctrl_msg_id(acc, control_handlers[i].msg_id) )
            continue;

        writeU16toBEpointer(control_handlers[i].msg_id, p);
        p += 2;
    }

    // add user handlers
    vec_foreach(acc->ctrl_cb, struct iap2_ctrl_cb*, cb)
    {
        if ((cb->msg_id & 0xFF00) == 0xAA00 || (cb->msg_id & 0xFF00) == 0x1D00)
        {
            continue;
        }

        writeU16toBEpointer(cb->msg_id, p);
        p += 2;
    }

    ENSURE_SPACE(err_recvd);
    iap2_ctrl_param_blob(&params[n_params++], 7, recvd_msgs, p - recvd_msgs);


    iap2_ctrl_param_int(&params[n_params++], 8, 1, acc->description->power_providing_capability);
    iap2_ctrl_param_int(&params[n_params++], 9, 2, acc->description->maximum_current_drawn_from_device);

    // declare the EA protocols we know about
    struct iap2_ctrl_param *protos = calloc(sizeof(*protos), 3 * vec_nmemb(acc->ea_protocols));
    struct iap2_ctrl_param *proto_slot = protos;
    uint8_t proto_idx = 0;
    if ( !protos )
        goto err_sent;

    vec_foreach(acc->ea_protocols, struct iap2_ea_protocol*, proto)
    {
        iap2_ctrl_param_int(proto_slot, 0, 1, proto_idx++);
        iap2_ctrl_param_string(proto_slot + 1, 1, proto->name);
        iap2_ctrl_param_int(proto_slot + 2, 2, 1, proto->match);

        ENSURE_SPACE(err_ea_protos);
        iap2_ctrl_param_group(&params[n_params++], 10, proto_slot, 3);

        proto_slot += 3;
    }

    if ( acc->description->app_match_team_id )
        iap2_ctrl_param_string(&params[n_params++], 11, acc->description->app_match_team_id);

    iap2_ctrl_param_string(&params[n_params++], 12, acc->description->current_language);

    const char *lang = acc->description->supported_language;
    while ( *lang )
    {
        ENSURE_SPACE(err);

        iap2_ctrl_param_string(&params[n_params++], 13, lang);
        lang += strlen(lang) + 1;
    }

    struct iap2_transport *transport;
    struct iap2_transport *transporttmp;
    list_for_every_entry_safe(&acc->transports, transport, transporttmp, struct iap2_transport, node)
    {
        if ( transport->provide_identification )
        {
            ENSURE_SPACE(err);
            if ( transport->provide_identification(transport, &params[n_params]) >= 0 )
            {
                n_params++;
            }
        }
    }


    iap2_ctrl_send(link, iAP2Msg_Acc_IdentificationInformation, params, n_params);

    ret = 0;

    err_ea_protos:
    free(protos);
    err_sent:
    free(sent_msgs);
    err_recvd:
    free(recvd_msgs);
    err:
    free(params);

    return ret;
#undef ENSURE_SPACE
}

static int iap2_ctrl_IdentificationAccepted(struct iap2_link *link, const uint8_t *payload, size_t len, void *arg)
{
    hsk_print(E_LEVEL_INFO, "[iap2_ctrl_IdentificationAccepted] !!!!!!!!!!");

    iap2_link_set_state(link, IAP2_LINK_CONNECTED);

    if ( link->connect_cb )
        link->connect_cb(link);

    return 0;
}

static int iap2_ctrl_IdentificationRejected(struct iap2_link *link, const uint8_t *payload, size_t len, void *arg)
{
    hsk_print(E_LEVEL_ERROR, "[iap2_ctrl_IdentificationRejected] Device rejected identification");

#if IAP2_DEBUG > 2
    hsk_print(E_LEVEL_INFO, "[iap2_ctrl_IdentificationRejected] len=%d", len);

    const uint8_t *ptr = payload;
    char buf[80];
    for (int i = 0; i < len && i < 80; i+=2,ptr++) {
        char hinibble = ((*ptr & 0xF0) >> 4);
        char lonibble = *ptr & 0x0F;
        hinibble = hinibble < 10 ? (hinibble + 0x30) : ((hinibble-10) + 0x41);
        lonibble = lonibble < 10 ? (lonibble + 0x30) : ((lonibble-10) + 0x41);
        buf[i] = hinibble;
        buf[i+1] = lonibble;
    }

    hsk_print(E_LEVEL_DEBUG, "[iap2_ctrl_IdentificationRejected] PAYLOAD");
    hsk_print(E_LEVEL_DEBUG, "<%s>",buf);
    //hexdump(data, len);
    hsk_print(E_LEVEL_DEBUG, "----------");
#endif

    iap2_link_shutdown(link);

    return 0;
}

int iap2_ctrl_handle(struct iap2_link *link, const uint8_t *data, size_t len)
{
#if IAP2_DEBUG > 1
    hsk_print(E_LEVEL_INFO, "[iap2_ctrl_handle] got packet");
#endif

    // we need at least a complete header, which is 6 bytes
    if ( len < 6 ) {
#if IAP2_DEBUG > 2
    hsk_print(E_LEVEL_ERROR, "[iap2_ctrl_handle] we need at least a complete header, which is 6 bytes");
#endif
        return -1;
    }

    // check magic
    if ( data[0] != 0x40 || data[1] != 0x40 ) {
#if IAP2_DEBUG > 2
    hsk_print(E_LEVEL_ERROR, "[iap2_ctrl_handle] there is no magic...not the right one");
#endif
        return -1;
    }

    uint16_t msg_len = readU16fromBEpointer(&data[2]);
    uint16_t msg_id = readU16fromBEpointer(&data[4]);

#if IAP2_DEBUG > 2
    hsk_print(E_LEVEL_INFO, "[iap2_ctrl_handle] msg_len(%04X) msg_id(%04X)", msg_len, msg_id);
#endif

    // sanity check: does the self-declared packet length match the buffer size?
    if ( msg_len != len ) {
#if IAP2_DEBUG > 2
    hsk_print(E_LEVEL_ERROR, "[iap2_ctrl_handle] msg_len(%04X) != len(%04X)", msg_len, len);
#endif
        return -1;
    }

    const uint8_t *payload = &data[6];
    size_t payload_length = len - 6;

    // first, check internal handlers
    for (size_t i = 0; control_handlers[i].cb; i++)
        if ( control_handlers[i].msg_id == msg_id ) {
#if IAP2_DEBUG > 1
            hsk_print(E_LEVEL_INFO, "[iap2_ctrl_handle] handler found for control message id %04x", msg_id);
#endif
            return control_handlers[i].cb(link, payload, payload_length, NULL);
        }

#if IAP2_DEBUG > 2
            hsk_print(E_LEVEL_ERROR, "[iap2_ctrl_handle] no handler was found");
#endif

    // if no handler was found, look through the user-registered ones
    vec_foreach(link->acc->ctrl_cb, struct iap2_ctrl_cb*, cb)
    if (cb->msg_id == msg_id)
    return cb->cb(link, payload, payload_length, cb->arg);

#if IAP2_DEBUG
    hsk_print(E_LEVEL_ERROR, "No handler found for control message id %04x", msg_id);
#endif

    return -1;
}

int iap2_ctrl_register_sent(struct iap2_accessory *acc, enum iap2_msg_id id)
{
    uint16_t msg_id = (uint16_t)id;
    return vec_append(acc->sent_msgs, &msg_id);
}

int iap2_ctrl_parse(struct iap2_ctrl_out_param *params, const struct iap2_ctrl_policy *policy, size_t n_policy, const uint8_t *data, size_t payload_len)
{
    const uint8_t *p = data;
    const uint8_t *end = data + payload_len;

    /* mark all params as absent now so we can use the flag later to detect
     * multiple params with the same id
     */
    for (size_t i = 0; i < n_policy; i++)
    {
        params[i].present = false;
    }

    while ( p < end )
    {
        if ( end - p < 4 )
        {
            hsk_print(E_LEVEL_ERROR, "Message didn't contain a complete parameter header");
            return -1;
        }

        uint16_t total_len = readU16fromBEpointer(p);
        uint16_t id = readU16fromBEpointer(p + 2);
        p += 4;

        if ( id > n_policy )
        {
            hsk_print(E_LEVEL_ERROR, "Got parameter with id 0x%02x > than the supplied policy (%zu)", id, n_policy);
            return -1;
        }

        if ( total_len < 4 )
        {
            hsk_print(E_LEVEL_ERROR, "Invalid param length (%d), must be greater than 4", total_len);
            return -1;
        }

        size_t len = total_len - 4;

        const struct iap2_ctrl_policy *pol = &policy[id];
        if ( !pol->valid )
            goto next;

        struct iap2_ctrl_out_param *param = &params[id];
        if ( param->present )
            goto next;

        bool size_okay;
        switch ( pol->type )
        {
            case IAP2_CTRL_PARAM_GROUP:
                case IAP2_CTRL_PARAM_BLOB:
                if ( pol->size > 0 )
                    size_okay = len == pol->size;
                else
                    size_okay = true;
                break;
            case IAP2_CTRL_PARAM_UTF8:
                if ( pol->size > 0 )
                    size_okay = (len - 1) == pol->size;
                else
                    size_okay = true;
                break;
            case IAP2_CTRL_PARAM_NONE:
                size_okay = len == 0;
                break;
            default:
                if ( pol->type <= IAP2_CTRL_PARAM_INT64 )
                    size_okay = len == (size_t)(1 << pol->type);
                else
                {
                    hsk_print(E_LEVEL_ERROR, "Unsupported parameter type %d", pol->type);
                    return -1;
                }
        }

        if ( !size_okay )
        {
            hsk_print(E_LEVEL_ERROR, "Unexpected parameter size (%zu) for parameter type %d", len, pol->type);
            return -1;
        }

        switch ( pol->type )
        {
            case IAP2_CTRL_PARAM_GROUP:
                case IAP2_CTRL_PARAM_BLOB:
                case IAP2_CTRL_PARAM_UTF8:
                param->data.blob.p = p;
                param->data.blob.l = len;
                break;
            case IAP2_CTRL_PARAM_NONE:
                break;
            default:
                // numeric type
                if ( pol->type <= IAP2_CTRL_PARAM_INT64 )
                {
                    uint64_t out = 0;

                    for (size_t i = 0; i < len; i++)
                    {
                        out <<= 8;
                        out |= p[i];
                    }

                    param->data.num.v = out;
                    param->data.num.l = len;
                }
        }

        param->present = true;
        next:
        p += len;
    }

    return 0;
}
