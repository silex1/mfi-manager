#include <unistd.h>
#include <sys/eventfd.h>
#include <sys/timerfd.h>

#include <leica/leica_types.h>
#include <leica/hsk_debug_linux_if.h>

#include "iap2-priv.h"
#include "iap2-dbus.h"

struct iap2_dbus_connection
{
    int epfd;
    DBusConnection *conn;

    int eventfd;
    struct iap2_epoll eventfd_ctx;
    DBusDispatchStatus dispatch_status;
};

struct iap2_dbus_watch
{
    struct iap2_epoll epoll;
    dbus_bool_t enabled;
    DBusWatch *watch;

    struct iap2_dbus_connection *priv;
};

struct iap2_dbus_timeout
{
    struct iap2_epoll epoll;
    int fd;
    dbus_bool_t enabled;
    DBusTimeout *timeout;

    struct iap2_dbus_connection *priv;
};

static void iap2_dbus_send_event(struct iap2_dbus_connection *priv)
{
    uint64_t n = 1;
    write(priv->eventfd, &n, sizeof(n));
}

static void on_dispatch_status(DBusConnection *c, DBusDispatchStatus new_status, void *data)
{
    struct iap2_dbus_connection *priv = data;

    switch ( new_status )
    {
        case DBUS_DISPATCH_DATA_REMAINS:    iap2_dbus_send_event(priv); break;
        case DBUS_DISPATCH_COMPLETE:        break;
        default:                            hsk_print(E_LEVEL_ERROR, "Unhandled status: %d", new_status);   break;
    }
}

static void dbus_watch_cb(struct iap2_epoll *ctx, uint32_t events)
{
    struct iap2_dbus_watch *iap2watch = ctx->arg;
    unsigned int flags = 0;

    if ( events & EPOLLIN )
        flags |= DBUS_WATCH_READABLE;
    if ( events & EPOLLOUT )
        flags |= DBUS_WATCH_WRITABLE;
    if ( events & EPOLLERR )
        flags |= DBUS_WATCH_ERROR;
    if ( events & EPOLLHUP )
        flags |= DBUS_WATCH_HANGUP;

    if ( !dbus_watch_handle(iap2watch->watch, flags) )
    {
        hsk_print(E_LEVEL_ERROR, "Watch handler failed");
    }
}

static void on_watch_toggled(DBusWatch *watch, void *data)
{
    struct iap2_dbus_connection *priv = data;
    struct iap2_dbus_watch *iap2watch;

    iap2watch = dbus_watch_get_data(watch);
    if ( !iap2watch )
    {
        hsk_print(E_LEVEL_ERROR, "Watch data is NULL");
        return;
    }

    if ( iap2watch->watch != watch )
    {
        hsk_print(E_LEVEL_ERROR, "Invalid watch passed");
        return;
    }

    dbus_bool_t dbus_watch_enabled = dbus_watch_get_enabled(watch);
    unsigned int dbus_watch_flags = dbus_watch_get_flags(watch);

    if ( dbus_watch_enabled && !iap2watch->enabled )
    {
        uint32_t events = 0;

        if ( dbus_watch_flags & DBUS_WATCH_READABLE )
            events |= EPOLLIN;
        if ( dbus_watch_flags & DBUS_WATCH_WRITABLE )
            events |= EPOLLOUT;
        if ( dbus_watch_flags & DBUS_WATCH_ERROR )
            events |= EPOLLERR;
        if ( dbus_watch_flags & DBUS_WATCH_HANGUP )
            events |= EPOLLHUP;

        if ( iap2_register_fd(priv->epfd, dbus_watch_get_unix_fd(watch), events, dbus_watch_cb, &iap2watch->epoll, iap2watch) )
        {
            hsk_print(E_LEVEL_ERROR, "Can't register dbus watch fd");
            return;
        }
        iap2watch->enabled = TRUE;
    }
    else if ( !dbus_watch_enabled && iap2watch->enabled )
    {
        if ( iap2_unregister_fd(priv->epfd, dbus_watch_get_unix_fd(watch)) )
        {
            hsk_print(E_LEVEL_ERROR, "Can't unregister dbus watch fd");
            return;
        }
        iap2watch->enabled = FALSE;
    }
}

static void on_free_watch(void *memory)
{
    struct iap2_dbus_watch *iap2watch = memory;
    struct iap2_dbus_connection *priv = iap2watch->priv;

    if ( iap2watch->enabled )
    {
        if ( iap2_unregister_fd(priv->epfd, dbus_watch_get_unix_fd(iap2watch->watch)) )
        {
            hsk_print(E_LEVEL_ERROR, "Can't unregister dbus watch fd");
        }
        iap2watch->enabled = FALSE;
    }

    free(iap2watch);
}

static dbus_bool_t on_add_watch(DBusWatch *watch, void *data)
{
    struct iap2_dbus_connection *priv = data;
    struct iap2_dbus_watch *iap2watch;

    iap2watch = calloc(sizeof(*iap2watch), 1);
    if ( !iap2watch )
        return FALSE;
    iap2watch->priv = priv;

    dbus_watch_set_data(watch, iap2watch, on_free_watch);
    iap2watch->watch = watch;

    on_watch_toggled(watch, data);
    return TRUE;
}

static void on_remove_watch(DBusWatch *watch, void *data)
{
    struct iap2_dbus_watch *iap2watch;

    iap2watch = dbus_watch_get_data(watch);
    if ( !iap2watch )
    {
        hsk_print(E_LEVEL_ERROR, "Watch data is NULL");
        return;
    }

    if ( iap2watch->watch != watch )
    {
        hsk_print(E_LEVEL_ERROR, "Invalid watch passed");
        return;
    }

    // this calls on_free_watch for us
    dbus_watch_set_data(watch, NULL, NULL);
}

static void dbus_timer_cb(struct iap2_epoll *ctx, uint32_t events)
{
    struct iap2_dbus_timeout *iap2timeout = ctx->arg;

    if ( !dbus_timeout_handle(iap2timeout->timeout) )
    {
        hsk_print(E_LEVEL_ERROR, "Timeout handler failed");
    }
}

static int timeout_disable(struct iap2_dbus_timeout *iap2timeout)
{
    struct itimerspec its;
    its.it_interval.tv_sec = its.it_interval.tv_nsec = 0;
    its.it_value.tv_sec = its.it_value.tv_nsec = 0;

    if ( timerfd_settime(iap2timeout->fd, 0, &its, NULL) < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "Timerfd_settime");
        return -1;
    }

    iap2timeout->enabled = FALSE;
    return 0;
}

static void on_timeout_toggled(DBusTimeout *timeout, void *data)
{
    struct iap2_dbus_timeout *iap2timeout;

    iap2timeout = dbus_timeout_get_data(timeout);
    if ( !iap2timeout )
    {
        hsk_print(E_LEVEL_ERROR, "Timeout data is NULL");
        return;
    }

    if ( iap2timeout->timeout != timeout )
    {
        hsk_print(E_LEVEL_ERROR, "Invalid timeout passed");
        return;
    }

    dbus_bool_t dbus_timeout_enabled = dbus_timeout_get_enabled(timeout);
    int dbus_timeout_interval = dbus_timeout_get_interval(timeout);

    if ( dbus_timeout_enabled && !iap2timeout->enabled )
    {
        struct itimerspec its;
        its.it_interval.tv_sec = its.it_interval.tv_nsec = 0;
        its.it_value.tv_sec = dbus_timeout_interval / 1000;
        its.it_value.tv_nsec = (dbus_timeout_interval % 1000) * 1000ull * 1000ull;

        if ( timerfd_settime(iap2timeout->fd, 0, &its, NULL) < 0 )
        {
            hsk_print(E_LEVEL_ERROR, "Timerfd_settime");
            return;
        }
        iap2timeout->enabled = TRUE;
    }
    else if ( !dbus_timeout_enabled && iap2timeout->enabled )
    {
        if ( timeout_disable(iap2timeout) )
        {
            hsk_print(E_LEVEL_ERROR, "Can't disable dbus timeout");
            return;
        }
    }
}

static void on_free_timeout(void *memory)
{
    struct iap2_dbus_timeout *iap2timeout = memory;
    struct iap2_dbus_connection *priv = iap2timeout->priv;

    if ( iap2timeout->enabled )
    {
        if ( timeout_disable(iap2timeout) )
        {
            hsk_print(E_LEVEL_ERROR, "Can't disable dbus timeout");
        }
    }

    iap2_unregister_fd(priv->epfd, iap2timeout->fd);
    close(iap2timeout->fd);
    free(iap2timeout);
}

static dbus_bool_t on_add_timeout(DBusTimeout *timeout, void *data)
{
    struct iap2_dbus_connection *priv = data;
    struct iap2_dbus_timeout *iap2timeout;

    iap2timeout = calloc(sizeof(*iap2timeout), 1);
    if ( !iap2timeout )
        return FALSE;
    iap2timeout->priv = priv;

    iap2timeout->fd = timerfd_create(CLOCK_MONOTONIC, TFD_CLOEXEC | TFD_NONBLOCK);
    if ( iap2timeout->fd < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "Timerfd_create failed");
        goto err_free;
    }

    if ( iap2_register_fd(priv->epfd, iap2timeout->fd, EPOLLIN, dbus_timer_cb, &iap2timeout->epoll, iap2timeout) < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "Registering timerfd with epoll failed");
        goto err_fd;
    }

    dbus_timeout_set_data(timeout, iap2timeout, on_free_timeout);
    iap2timeout->timeout = timeout;

    on_timeout_toggled(timeout, data);
    return TRUE;

    err_fd:
    close(iap2timeout->fd);
    err_free:
    free(iap2timeout);
    return FALSE;
}

static void on_remove_timeout(DBusTimeout *timeout, void *data)
{
    struct iap2_dbus_timeout *iap2timeout;

    iap2timeout = dbus_timeout_get_data(timeout);
    if ( !iap2timeout )
    {
        hsk_print(E_LEVEL_ERROR, "Timeout data is NULL");
        return;
    }

    if ( iap2timeout->timeout != timeout )
    {
        hsk_print(E_LEVEL_ERROR, "Invalid timeout passed");
        return;
    }

    // this calls on_free_timeout for us
    dbus_timeout_set_data(timeout, NULL, NULL);
}

static void dbus_eventfd_cb(struct iap2_epoll *epoll_ctx, uint32_t events)
{
    struct iap2_dbus_connection *priv = (struct iap2_dbus_connection*)epoll_ctx->arg;
    int ret;
    uint64_t u;

    ret = read(epoll_ctx->fd, &u, sizeof(u));
    if ( ret < 0 || (size_t)ret != sizeof(u) )
    {
        hsk_print(E_LEVEL_ERROR, "Reading from eventfd failed");
        return;
    }

    while ( dbus_connection_get_dispatch_status(priv->conn) != DBUS_DISPATCH_COMPLETE )
    {
        dbus_connection_dispatch(priv->conn);
    }
}

struct iap2_dbus_connection* iap2_dbus_register(int epfd, DBusConnection *conn)
{
    struct iap2_dbus_connection *priv;

    priv = calloc(sizeof(*priv), 1);
    if ( !priv )
    {
        hsk_print(E_LEVEL_ERROR, "Allocating iap2 dbus connection failed");
        return NULL;
    }

    priv->epfd = epfd;
    priv->conn = conn;

    priv->eventfd = eventfd(0, EFD_CLOEXEC);
    if ( priv->eventfd < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "Unable to open eventfd");
        goto err_free_priv;
    }

    if ( iap2_register_fd(epfd, priv->eventfd, EPOLLIN, dbus_eventfd_cb, &priv->eventfd_ctx, priv) < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "Registering eventfd failed");
        goto err_close_eventfd;
    }

    dbus_connection_set_exit_on_disconnect(conn, false);
    dbus_connection_set_dispatch_status_function(conn, on_dispatch_status, priv, NULL);

    if ( !dbus_connection_set_watch_functions(conn, on_add_watch, on_remove_watch, on_watch_toggled, priv, NULL) )
    {
        hsk_print(E_LEVEL_ERROR, "Can't set watch functions");
        goto err_remove_dispatch_fn;
    }

    if ( !dbus_connection_set_timeout_functions(conn, on_add_timeout, on_remove_timeout, on_timeout_toggled, priv, NULL) )
    {
        hsk_print(E_LEVEL_ERROR, "Can't set timeout functions");
        goto err_remove_watch_fn;
    }

    iap2_dbus_send_event(priv);

    return priv;

err_remove_watch_fn:
    dbus_connection_set_watch_functions(conn, NULL, NULL, NULL, NULL, NULL);

err_remove_dispatch_fn:
    dbus_connection_set_dispatch_status_function(conn, NULL, NULL, NULL);
    iap2_unregister_fd(epfd, priv->eventfd);

err_close_eventfd:
    close(priv->eventfd);
    err_free_priv:
    free(priv);

    return NULL;
}

void iap2_dbus_remove(struct iap2_dbus_connection *priv)
{
    dbus_connection_set_timeout_functions(priv->conn, NULL, NULL, NULL, NULL, NULL);
    dbus_connection_set_watch_functions(priv->conn, NULL, NULL, NULL, NULL, NULL);
    dbus_connection_set_dispatch_status_function(priv->conn, NULL, NULL, NULL);
    iap2_unregister_fd(priv->epfd, priv->eventfd);
    close(priv->eventfd);
    free(priv);
}

static const char *signature_from_basic_type(int type)
{
    switch ( type )
    {
        case DBUS_TYPE_BOOLEAN:     return DBUS_TYPE_BOOLEAN_AS_STRING;
        case DBUS_TYPE_BYTE:        return DBUS_TYPE_BYTE_AS_STRING;
        case DBUS_TYPE_INT16:       return DBUS_TYPE_INT16_AS_STRING;
        case DBUS_TYPE_UINT16:      return DBUS_TYPE_UINT16_AS_STRING;
        case DBUS_TYPE_INT32:       return DBUS_TYPE_INT32_AS_STRING;
        case DBUS_TYPE_UINT32:      return DBUS_TYPE_UINT32_AS_STRING;
        case DBUS_TYPE_INT64:       return DBUS_TYPE_INT64_AS_STRING;
        case DBUS_TYPE_UINT64:      return DBUS_TYPE_UINT64_AS_STRING;
        case DBUS_TYPE_DOUBLE:      return DBUS_TYPE_DOUBLE_AS_STRING;
        case DBUS_TYPE_STRING:      return DBUS_TYPE_STRING_AS_STRING;
        case DBUS_TYPE_OBJECT_PATH: return DBUS_TYPE_OBJECT_PATH_AS_STRING;
        case DBUS_TYPE_SIGNATURE:   return DBUS_TYPE_SIGNATURE_AS_STRING;
        default:                    return "unknown";
    }
}

void iap2_dbus_append_basic_variant(DBusMessageIter *iter, int type, void *data)
{
    DBusMessageIter variant_iter;
    dbus_message_iter_open_container(iter, DBUS_TYPE_VARIANT, signature_from_basic_type(type), &variant_iter);
    dbus_message_iter_append_basic(&variant_iter, type, data);
    dbus_message_iter_close_container(iter, &variant_iter);
}

void iap2_dbus_append_basic_variant_dict_entry(DBusMessageIter *dict_iter, const char *key, int type, void *data)
{
    DBusMessageIter dict_entry_iter;
    dbus_message_iter_open_container(dict_iter, DBUS_TYPE_DICT_ENTRY, NULL, &dict_entry_iter);
    dbus_message_iter_append_basic(&dict_entry_iter, DBUS_TYPE_STRING, &key);
    iap2_dbus_append_basic_variant(&dict_entry_iter, type, data);
    dbus_message_iter_close_container(dict_iter, &dict_entry_iter);
}

int iap2_dbus_extract_from_variant(DBusMessageIter *iter, int type, void *data)
{
    DBusMessageIter sub;

    if ( dbus_message_iter_get_arg_type(iter) != DBUS_TYPE_VARIANT )
        return -1;

    dbus_message_iter_recurse(iter, &sub);
    if ( dbus_message_iter_get_arg_type(&sub) != type )
        return -1;

    dbus_message_iter_get_basic(&sub, data);

    return 0;
}

int iap2_dbus_property_get(DBusConnection *conn, const char *service, const char *path, const char *interface,
                           const char *name, int type, int (*cb)(void *data, void *arg), void *arg)
{
    int ret = -1;
    DBusError error;
    DBusMessage *msg = NULL;
    DBusMessage *reply = NULL;
    DBusMessageIter iter;

    dbus_error_init(&error);

    msg = dbus_message_new_method_call(service, path, "org.freedesktop.DBus.Properties", "Get");
    if ( !msg )
    {
        hsk_print(E_LEVEL_ERROR, "Creating message failed");
        return -1;
    }

    dbus_message_iter_init_append(msg, &iter);
    dbus_message_iter_append_basic(&iter, DBUS_TYPE_STRING, &interface);
    dbus_message_iter_append_basic(&iter, DBUS_TYPE_STRING, &name);

    reply = dbus_connection_send_with_reply_and_block(conn, msg, DBUS_TIMEOUT_USE_DEFAULT, &error);
    if ( !reply )
    {
        hsk_print(E_LEVEL_ERROR, "Can't send dbus message: %s", error.message);
        dbus_error_free(&error);
        goto close_unref;
    }

    if ( !dbus_message_iter_init(reply, &iter) )
    {
        hsk_print(E_LEVEL_ERROR, "Reply has no arguments");
        goto close_unref;
    }

    void *data;
    if ( iap2_dbus_extract_from_variant(&iter, type, &data) != 0 )
    {
        hsk_print(E_LEVEL_ERROR, "Could not extract data from reply");
        goto close_unref;
    }

    ret = cb(data, arg);

    close_unref:
    dbus_message_unref(msg);
    if ( reply )
        dbus_message_unref(reply);

    return ret;
}

int iap2_dbus_property_set(DBusConnection *conn, const char *service, const char *path,
                           const char *interface, const char *name, int type, void *value)
{
    int ret = -1;
    DBusError error;
    DBusMessage *msg = NULL;
    DBusMessage *reply = NULL;
    DBusMessageIter iter;

    dbus_error_init(&error);

    msg = dbus_message_new_method_call(service, path, "org.freedesktop.DBus.Properties", "Set");
    if ( !msg )
    {
        hsk_print(E_LEVEL_ERROR, "Creating message failed");
        return -1;
    }

    dbus_message_iter_init_append(msg, &iter);
    dbus_message_iter_append_basic(&iter, DBUS_TYPE_STRING, &interface);
    dbus_message_iter_append_basic(&iter, DBUS_TYPE_STRING, &name);
    iap2_dbus_append_basic_variant(&iter, type, value);

    reply = dbus_connection_send_with_reply_and_block(conn, msg, DBUS_TIMEOUT_USE_DEFAULT, &error);
    if ( !reply )
    {
        hsk_print(E_LEVEL_ERROR, "Can't send dbus message: %s", error.message);
        dbus_error_free(&error);
        goto close_unref;
    }

    ret = 0;

    close_unref:
    dbus_message_unref(msg);
    if ( reply )
        dbus_message_unref(reply);

    return ret;
}
