#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <sys/socket.h>
#include <unistd.h>
#include <errno.h>

#include <leica/leica_types.h>
#include <leica/hsk_debug_linux_if.h>

#include "iap2.h"
#include "iap2-priv.h"
#include "io.h"

#define RX_BUF_SIZE (1*1024*1024)

static const char *get_ea_protocol(struct iap2_accessory *acc, uint8_t idx)
{
    if ( idx >= vec_nmemb(acc->ea_protocols) )
        return NULL;

    return *(const char**)vec_index(acc->ea_protocols, idx);
}

void iap2_ea_cleanup_session(struct iap2_ea_session *sess)
{
    struct iap2_link *link = sess->link;

    iap2_unregister_fd(link->epfd, sess->epoll_ctx.fd);
    sess->events = 0;
    close(sess->epoll_ctx.fd);

    list_delete(&sess->list);

    free(sess->tx_buf);
    pthread_mutex_destroy(&sess->rx_mutex);
    ringbuf_free(&sess->rx_buf);
    free(sess);
}

static void iap2_ea_drain_rx(struct iap2_ea_session *sess, bool drain)
{
    if ( drain )
        sess->events |= EPOLLOUT;
    else
        sess->events &= ~EPOLLOUT;

    if ( iap2_update_fd_events(sess->link->epfd, &sess->epoll_ctx, sess->events) < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "updating epoll flags for EA socket failed");
    }
}

static void iap2_ea_enable_tx(struct iap2_ea_session *sess, bool enable)
{
    if ( enable )
    {
        sess->events |= EPOLLIN;
        sess->events |= EPOLLERR;
    }
    else
    {
        sess->events &= ~EPOLLIN;
        sess->events &= ~EPOLLERR;
    }

    if ( iap2_update_fd_events(sess->link->epfd, &sess->epoll_ctx, sess->events) < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "updating epoll flags for EA socket failed");
    }
}

static void _iap2_ea_send_data(struct iap2_ea_session *sess, bool retry, uint8_t *buf, size_t sendoff, uint32_t sendsz)
{
    struct iap2_link *link = sess->link;
    uint32_t actual;

    writeU16toBEpointer(sess->session_id, &buf[sendoff - 2]);

    pthread_mutex_lock(&link->iap2_mutex);
    IAP2RET iaprc = iAP2LinkQueueSendData(link->run_loop->link, buf + sendoff - 2, 2 + sendsz, IAP2_EAP_SESSION_ID, NULL, NULL, &actual);
    pthread_mutex_unlock(&link->iap2_mutex);

    if ( iaprc )
    {
        if ( iaprc == IAP2RET_TRYAGAIN )
        {
            hsk_print(E_LEVEL_DEBUG_RAW, "try again, sent: %d/%d", actual, sendsz);
            sess->tx_buf_pending_off = sendoff + actual;
            sess->tx_buf_pending_sz = sendsz - actual;

            if ( !retry )
                iap2_ea_enable_tx(sess, false);
        }
        else
        {
            hsk_print(E_LEVEL_ERROR, "FAILED TO QUEUE DATA: %d", iaprc);

            if ( retry )
            {
                // skip this buffer and continue sending new ones
                iap2_ea_enable_tx(sess, true);
            }
        }
    }
    else if ( retry )
    {
        // we can send more data now
        iap2_ea_enable_tx(sess, true);
    }
}

void iap2_ea_runloop_updated(struct iap2_ea_session *sess)
{
    struct iap2_link *link = sess->link;

    if ( sess->tx_buf_pending_sz && iAP2LinkQueueFreeSlots(link->run_loop->link, IAP2_EAP_SESSION_ID) )
    {
        uint8_t *buf = sess->tx_buf;
        size_t sendoff = sess->tx_buf_pending_off;
        uint32_t sendsz = sess->tx_buf_pending_sz;

        hsk_print(E_LEVEL_DEBUG_RAW, "got space now");

        sess->tx_buf_pending_off = 0;
        sess->tx_buf_pending_sz = 0;

        _iap2_ea_send_data(sess, true, buf, sendoff, sendsz);
    }
}

static void iap2_ea_userdata_in(struct iap2_ea_session *sess)
{
    struct iap2_link *link = sess->link;
    uint8_t *buf = sess->tx_buf;
    size_t max_size = link->iap_syn.maxPacketSize;

    ssize_t ret;
    restart:
    ret = recv(sess->epoll_ctx.fd, &buf[2], max_size - 2, 0);
    if ( ret < 0 )
    {
        if ( errno == EINTR )
            goto restart;

        hsk_print(E_LEVEL_ERROR, "recv failed");
        return;
    }

    if ( ret == 0 )
    {
        // remote closed the socket, mark the session as dead
        sess->alive = false;

        return;
    }

    _iap2_ea_send_data(sess, false, buf, 2, ret);
}

static void iap2_ea_userdata_out(struct iap2_ea_session *sess)
{
    pthread_mutex_lock(&sess->rx_mutex);
    while ( !ringbuf_is_empty(sess->rx_buf) )
    {
        ssize_t ret;
        ret = ringbuf_write(sess->epoll_ctx.fd, sess->rx_buf, ringbuf_bytes_used(sess->rx_buf));
        if ( ret < 0 )
        {
            if ( errno == EAGAIN || errno == EWOULDBLOCK )
            {
                // we cannot queue any more data into the socket for now, continue later
                goto out;
            }
            else
            {
                hsk_print(E_LEVEL_ERROR, "error while draining data into EA socket");
                goto out;
            }
        }
    }

    // no more data to send, unsubscribe from the EPOLLOUT event
    iap2_ea_drain_rx(sess, false);

    out:
    pthread_mutex_unlock(&sess->rx_mutex);
}

static void iap2_ea_userdata_cb(struct iap2_epoll *ctx, uint32_t events)
{
    struct iap2_ea_session *sess = (struct iap2_ea_session*)ctx->arg;

    if ( events & (EPOLLIN | EPOLLERR) )
        iap2_ea_userdata_in(sess);
    else if ( events & EPOLLOUT )
        iap2_ea_userdata_out(sess);
    else
        hsk_print(E_LEVEL_WARNING, "unexpected epoll event mask 0x%x", events);
}

static struct iap2_ea_session *iap2_ea_session_by_id(struct iap2_link *link, uint16_t session_id)
{
    struct iap2_ea_session *sess;

    list_for_every_entry(&link->ea_sessions, sess, struct iap2_ea_session, list)
    {
        if ( sess->session_id == session_id )
            return sess;
    }

    return NULL;
}

int iap2_ctrl_StartExternalAccessoryProtocolSession(struct iap2_link *link, const uint8_t *payload, size_t len, void *arg)
{
    struct iap2_accessory *acc = link->acc;

    enum
    {
        START_SESSION_PROTO_ID = 0,
        START_SESSION_SESSION_ID = 1,
        __START_SESSION_MAX
    };

    static const struct iap2_ctrl_policy start_session_policy[__START_SESSION_MAX] =
    {
        IAP2_CTRL_POLICY(START_SESSION_PROTO_ID, INT8),
        IAP2_CTRL_POLICY(START_SESSION_SESSION_ID, INT16)
    };

    struct iap2_ctrl_out_param params[__START_SESSION_MAX ];

    if ( iap2_ctrl_parse(params, start_session_policy, __START_SESSION_MAX, payload, len) < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "parsing StartExternalAccessoryProtocolSession message failed");
        return -1;
    }

    if ( !params[START_SESSION_PROTO_ID].present || !params[START_SESSION_SESSION_ID].present )
    {
        hsk_print(E_LEVEL_ERROR, "mandatory parameter missing");
        return -1;
    }

    uint8_t proto_id = (uint8_t)params[START_SESSION_PROTO_ID].data.num.v;
    uint16_t session_id = (uint16_t)params[START_SESSION_SESSION_ID].data.num.v;

    const char *proto = get_ea_protocol(acc, proto_id);
    if ( !proto )
    {
        hsk_print(E_LEVEL_ERROR, "Could not find external accessory protocol with id 0x%02x", proto_id);
        return -1;
    }

    struct iap2_ea_session *sess;
    sess = calloc(sizeof(*sess), 1);
    if ( !sess )
        return -1;

    sess->link = link;
    sess->proto_id = proto_id;
    sess->session_id = session_id;

    sess->tx_buf = calloc(link->iap_syn.maxPacketSize, 1);
    if ( !sess->tx_buf )
        goto err_sess;

    sess->rx_buf = ringbuf_new(RX_BUF_SIZE);
    if ( !sess->rx_buf )
        goto err_txbuf;

    if ( pthread_mutex_init(&sess->rx_mutex, NULL) < 0 )
        goto err_rxbuf;

    int socket_vector[2];
    if ( socketpair(AF_UNIX, SOCK_STREAM, 0, socket_vector) < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "creating socketpair failed");

        goto err_rx_mutex;
    }

    // claim index 0 for ourselves, index 1 goes to the user
    cloexec(socket_vector[0]);
    nonblock(socket_vector[0]);
    sess->events = EPOLLIN | EPOLLERR;
    if ( iap2_register_fd(link->epfd, socket_vector[0], sess->events, iap2_ea_userdata_cb, &sess->epoll_ctx, sess) < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "registering socketpair endpoint to epoll failed");
        goto err_socketpair;
    }

    list_add_tail(&link->ea_sessions, &sess->list);
    if ( acc->new_eap_session_cb(link, proto, socket_vector[1]) < 0 )
    {
        // session rejected

        // at this point, we still own the other fd, so we need to clean it up
        close(socket_vector[1]);

        // we only mark the session as dead here and don't clean it up
        // to avoid a special case when receiving the StopEASession
        // command from the device
        sess->alive = false;
        return 0;
    }

    sess->alive = true;

    return 0;

err_socketpair:
    close(socket_vector[0]);
    close(socket_vector[1]);

err_rx_mutex:
    pthread_mutex_destroy(&sess->rx_mutex);

err_rxbuf:
    ringbuf_free(&sess->rx_buf);

err_txbuf:
    free(sess->tx_buf);

err_sess:
    free(sess);
    return -1;
}

int iap2_ctrl_StopExternalAccessoryProtocolSession(struct iap2_link *link, const uint8_t *payload, size_t len, void *arg)
{
    enum
    {
        STOP_SESSION_SESSION_ID = 0,
        __STOP_SESSION_MAX
    };

    static const struct iap2_ctrl_policy stop_session_policy[__STOP_SESSION_MAX] = { IAP2_CTRL_POLICY(STOP_SESSION_SESSION_ID, INT16) };

    struct iap2_ctrl_out_param params[__STOP_SESSION_MAX ];

    if ( iap2_ctrl_parse(params, stop_session_policy, __STOP_SESSION_MAX, payload, len) < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "parsing StopExternalAccessoryProtocolSession message failed");
        return -1;
    }

    if ( !params[STOP_SESSION_SESSION_ID].present )
    {
        hsk_print(E_LEVEL_ERROR, "mandatory parameter missing");
        return -1;
    }

    uint16_t session_id = (uint16_t)params[STOP_SESSION_SESSION_ID].data.num.v;
    struct iap2_ea_session *sess = iap2_ea_session_by_id(link, session_id);
    if ( !sess )
    {
        hsk_print(E_LEVEL_WARNING, "device tried to close session 0x%02x, which does not exist", session_id);
        return 0;
    }

    iap2_ea_cleanup_session(sess);

    return 0;
}

void iap2_ea_device_data(struct iap2_link *link, uint8_t *data, size_t len)
{
    uint16_t session_id = readU16fromBEpointer(&data[0]);
    struct iap2_ea_session *sess = iap2_ea_session_by_id(link, session_id);
    if ( !sess )
    {
        hsk_print(E_LEVEL_WARNING, "received data for unknown session 0x%02x", session_id);
        return;
    }

    if ( !sess->alive )
        return;

    uint8_t *payload = &data[2];
    size_t payload_len = len - 2;

    // If data is queued, we mustn't send it out here but rather append it
    // to the queue to avoid reordering.

    pthread_mutex_lock(&sess->rx_mutex);
    if ( !ringbuf_is_empty(sess->rx_buf) )
    {
        ringbuf_memcpy_into(sess->rx_buf, payload, payload_len);
        goto out;
    }

    while ( 1 )
    {
        ssize_t ret;
restart:
        ret = send(sess->epoll_ctx.fd, payload, payload_len, 0);
        if ( ret < 0 )
        {
            if ( errno == EINTR )
                goto restart;

            if ( errno == EAGAIN || errno == EWOULDBLOCK )
                break;
        }

        payload += ret;
        payload_len -= ret;

        if ( payload_len == 0 )
            goto out;
    }

    // If we end up here, we weren't able to push all the data through the
    // socket. We need to queue it and let it drain via epoll.

    // if the ring buffer cannot contain the amount of data we want
    // to push into it, it will head-drop the existing data.
    // However, we want tail-drop semantics, so we just drop the
    // new data here.
    if ( ringbuf_bytes_free(sess->rx_buf) >= payload_len )
    {
        ringbuf_memcpy_into(sess->rx_buf, payload, payload_len);
        iap2_ea_drain_rx(sess, true);
    }

out:
    pthread_mutex_unlock(&sess->rx_mutex);
}

bool iap2_ea_is_configured(struct iap2_accessory *acc)
{
    return acc->new_eap_session_cb && vec_nmemb(acc->ea_protocols);
}
