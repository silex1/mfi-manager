#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <inttypes.h>
#include <errno.h>
#include <string.h>
#include <assert.h>
#include <sys/epoll.h>
#include <sys/timerfd.h>
#include <time.h>
#include <ctype.h>
#include <stdio.h>

#include <leica/leica_types.h>
#include <leica/hsk_debug_linux_if.h>

#include "iAP2Link.h"
#include "iAP2Packet.h"
#include "iAP2Log.h"

#include "iap2-priv.h"

#define EPOLL_MAX_EVENTS 10
#define IAP2_CONNECT_TIMEOUT 5

int iap2_register_fd(int epfd, int fd, uint32_t events, void (*cb)(struct iap2_epoll *ctx, uint32_t events), struct iap2_epoll *ctx, void *arg)
{
    struct epoll_event ev;

    ctx->fd = fd;
    ctx->cb = cb;
    ctx->arg = arg;

    ev.events = events;
    ev.data.ptr = ctx;

    if ( epoll_ctl(epfd, EPOLL_CTL_ADD, fd, &ev) < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "[iap2_register_fd] attaching fd to epoll failed");
        return -1;
    }

    return 0;
}

int iap2_unregister_fd(int epfd, int fd)
{
    // the event argument is always ignored, but older kernels fail if this is NULL
    if ( epoll_ctl(epfd, EPOLL_CTL_DEL, fd, (void*)1) < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "[iap2_unregister_fd] detaching fd from epoll failed");
        return -1;
    }

    return 0;
}

int iap2_update_fd_events(int epfd, struct iap2_epoll *ctx, uint32_t events)
{
    struct epoll_event ev;

    ev.events = events;
    ev.data.ptr = ctx;

    if ( epoll_ctl(epfd, EPOLL_CTL_MOD, ctx->fd, &ev) < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "updating epoll events failed");
        return -1;
    }

    return 0;
}

static int iap2_epoll_dispatch(int epfd)
{
    struct epoll_event events[EPOLL_MAX_EVENTS];
    int nfds;

    restart:
    hsk_print(E_LEVEL_INFO, "[iap2_epoll_dispatch] epoll_wait forever");
    nfds = epoll_wait(epfd, events, sizeof(events) / sizeof(*events), -1);
    hsk_print(E_LEVEL_INFO, "[iap2_epoll_dispatch] got events[%d]", nfds);
    if ( nfds < 0 )
    {
        if ( errno != EINTR )
        {
            hsk_print(E_LEVEL_ERROR, "epoll_wait");
            return -1;
        }

        goto restart;
    }

    for (int i = 0; i < nfds; i++)
    {
        struct iap2_epoll *ctx;

        ctx = (struct iap2_epoll*)events[i].data.ptr;
        ctx->cb(ctx, events[i].events);
    }

    return 0;
}

void *iap2_get_ctx(struct iap2_accessory *acc)
{
    return acc->user_ctx;
}

static void iap2_ll_send_packet(struct iap2_link *link, const uint8_t *data, size_t len)
{
    link->transport_ops->send_packet(link, data, len);
}

static void iap2_send_packet(iAP2Link_t *raw_link, iAP2Packet_t *packet)
{
    iAP2LinkRunLoop_t *run_loop = raw_link->context;
    struct iap2_link *link = run_loop->context;

    // this is called from the iAP2 library, so the mutex is already held
    uint8_t *buf = iAP2PacketGenerateBuffer(packet);
    iap2_ll_send_packet(link, buf, packet->packetLen);
}

static void iap2_send_detect(iAP2Link_t *raw_link, BOOL bBad)
{
    iAP2LinkRunLoop_t *run_loop = raw_link->context;
    struct iap2_link *link = run_loop->context;

    if ( bBad )
        iap2_ll_send_packet(link, kIap2PacketDetectBadData, kIap2PacketDetectBadDataLen);
    else
        iap2_ll_send_packet(link, kIap2PacketDetectData, kIap2PacketDetectDataLen);
}

static BOOL iap2_recv_packet(iAP2Link_t *raw_link, uint8_t* data, uint32_t dataLen, uint8_t session)
{
    iAP2LinkRunLoop_t *run_loop = raw_link->context;
    struct iap2_link *link = run_loop->context;
#if IAP2_DEBUG > 1
    hsk_print(E_LEVEL_INFO, "[iap2_recv_packet] got packet");
#endif

    if ( session == IAP2_CTRL_SESSION_ID ) {
#if IAP2_DEBUG > 1
    hsk_print(E_LEVEL_INFO, "[iap2_recv_packet] session id from ctrl session");
#endif
        iap2_ctrl_handle(link, data, dataLen);
    } else if ( session == IAP2_EAP_SESSION_ID ) {
#if IAP2_DEBUG > 1
    hsk_print(E_LEVEL_INFO, "[iap2_recv_packet] session id from eap session");
#endif
        iap2_ea_device_data(link, data, dataLen);
    } else
        hsk_print(E_LEVEL_ERROR, "[iap2_recv_packet] Unknown session 0x%x", session);

    return TRUE;
}

static void iap2_connected_cb(iAP2Link_t *raw_link, BOOL bConnected)
{
    iAP2LinkRunLoop_t *run_loop = raw_link->context;
    struct iap2_link *link = run_loop->context;

    /* We only consider a link to be connected once it has gone through the
     * authentification and identification phases, so we ignore the
     * "connected" case here. We are, however, interested in the
     * "disconnect" case, which the library signals in case of protocol
     * errors, for example.
     */

    if ( bConnected == FALSE )
        iap2_link_shutdown(link);
}

/// iAP2 Session Types see MFi Spec §51.3.1.1
enum iap2_session_types
{
    IAP2_SESSION_TYPE_CONTROL = 0, //!< Control session
    IAP2_SESSION_TYPE_FILE_TRANSFER = 1, //!< File Transfer session
    IAP2_SESSION_TYPE_EAP = 2, //!< External Accessory session
};

int iap2_run(struct iap2_accessory *acc)
{
    int ret = 0;

    acc->running = true;
    while ( acc->running )
    {
        if ( iap2_epoll_dispatch(acc->epfd) < 0 )
        {
            ret = -1;
            acc->running = false;

            break;
        }
    }

    return ret;
}

int iap2_stop(struct iap2_accessory *acc)
{
    acc->running = false;
    return 0;
}

static void iap2_link_user_new_cb(struct iap2_link *link)
{
    struct iap2_accessory *acc = link->acc;

    if ( acc->new_link_cb )
        acc->new_link_cb(link);
}

static void iap2_link_drop(struct refcnt *ref)
{
    struct iap2_link *link = containerof(ref, struct iap2_link, ref);

    /*
     * The link thread holds a reference to its link. Thus, this function
     * cannot be called unless it releases this reference, which it only
     * does after the link has been shut down.
     */

    iap2_loop_destroy(link);
    iap2_timer_destroy(link);
    pthread_mutex_destroy(&link->iap2_mutex);
    close(link->epfd);

    ref_down(&link->acc->ref);

    free(link);
}

struct iap2_link *iap2_link_new(struct iap2_accessory *acc, struct iap2_transport_ops *transport_ops,
                                struct iap2_transport *transport, void *transport_priv, bool use_callback)
{
    struct iap2_link *link = calloc(sizeof(*link), 1);

    if ( !link )
        return NULL;

    link->epfd = epoll_create1(EPOLL_CLOEXEC);
    if ( link->epfd < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "[iap2_link_new] epoll_create1 failed");
        goto err_link;
    }

    link->acc = acc;
    link->transport = transport;

    link->transport_ops = transport_ops;
    link->transport_priv = transport_priv;
    link->connect_timeout = IAP2_CONNECT_TIMEOUT;

    list_initialize(&link->ea_sessions);
    ref_init(link, struct iap2_link, ref, iap2_link_drop);

    if ( use_callback )
        link->connect_cb = iap2_link_user_new_cb;

    link->state = IAP2_LINK_IDLE;

    pthread_mutexattr_t attr;
    if ( pthread_mutexattr_init(&attr) < 0 )
        goto err_epfd;

    if ( pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE) < 0 )
    {
        pthread_mutexattr_destroy(&attr);
        goto err_epfd;
    }

    if ( pthread_mutex_init(&link->iap2_mutex, &attr) < 0 )
    {
        pthread_mutexattr_destroy(&attr);
        goto err_epfd;
    }

    if ( pthread_mutex_init(&link->state_mutex, &attr) < 0 )
    {
        pthread_mutexattr_destroy(&attr);
        goto err_iap2_mutex;
    }

    pthread_mutexattr_destroy(&attr);

    if ( iap2_timer_init(link) < 0 )
        goto err_state_mutex;

    if ( iap2_loop_init(link) < 0 )
        goto err_timer;

    ref_up(&acc->ref);

    return link;

err_timer:
    iap2_timer_destroy(link);

err_state_mutex:
    pthread_mutex_destroy(&link->state_mutex);

err_iap2_mutex:
    pthread_mutex_destroy(&link->iap2_mutex);

err_epfd:
    close(link->epfd);
    err_link:
    free(link);
    return NULL;
}

static void *iap2_link_thread(void *arg)
{
    struct iap2_link *link = (struct iap2_link*)arg;
    struct timespec start;

    if ( link->connect_timeout > 0 )
    {
        if ( clock_gettime(CLOCK_MONOTONIC_RAW, &start) == 0 )
        {
            hsk_print(E_LEVEL_ERROR, "[iap2_link_thread] Unable to get systemtime");
        }
    }

    hsk_print(E_LEVEL_DEBUG, "[iap2_link_thread] Link thread for link %p starting", link);

    iap2_link_set_state(link, IAP2_LINK_IDLE);
    while ( 1 )
    {
        enum iap2_link_state link_state = iap2_link_get_state(link);

        if ( link_state == IAP2_LINK_DEAD )
            break;
        else if ( link_state == IAP2_LINK_IDLE && link->connect_timeout > 0 )
        {
            struct timespec now;
            if ( clock_gettime(CLOCK_MONOTONIC_RAW, &now) == 0 )
            {
                hsk_print(E_LEVEL_WARNING, "[iap2_link_thread] Unable to get systemtime");
            }

            if ( now.tv_sec - start.tv_sec > link->connect_timeout )
            {
                hsk_print(E_LEVEL_ERROR, "[iap2_link_thread] link did not come up in %d seconds, hanging up", link->connect_timeout);
                iap2_link_shutdown(link);
                break;
            }
        }

        if ( iap2_epoll_dispatch(link->epfd) < 0 )
        {
            iap2_link_shutdown(link);
            break;
        }
    }

    iAP2LinkRunLoopDetached(link->run_loop);

    if ( link->transport_ops->cleanup )
        link->transport_ops->cleanup(link);

    struct iap2_ea_session *sess, *tmp;
    list_for_every_entry_safe(&link->ea_sessions, sess, tmp, struct iap2_ea_session, list)
        iap2_ea_cleanup_session(sess);

    ref_down(&link->ref);

    hsk_print(E_LEVEL_DEBUG, "[iap2_link_thread] link thread for link %p exiting", link);
    return NULL;
}

void iap2_link_ref(struct iap2_link *link)
{
    ref_up(&link->ref);
}

void iap2_link_unref(struct iap2_link *link)
{
    ref_down(&link->ref);
}

static void iap2_setup_sessions(struct iap2_accessory *acc, iAP2PacketSYNData_t *syn)
{
    int n = 0;

    // we always need a control session
    syn->sessionInfo[n].id = IAP2_CTRL_SESSION_ID;
    syn->sessionInfo[n].type = IAP2_SESSION_TYPE_CONTROL;
    syn->sessionInfo[n].version = 1;
    n++;

    if ( iap2_ea_is_configured(acc) )
    {
        syn->sessionInfo[n].id = IAP2_EAP_SESSION_ID;
        syn->sessionInfo[n].type = IAP2_SESSION_TYPE_EAP;
        syn->sessionInfo[n].version = 1;
        n++;
    }

    syn->numSessionInfo = n;
}

int iap2_link_start(struct iap2_link *link)
{
    link->iap_syn.version = 1;
    link->iap_syn.maxOutstandingPackets = 127;
    link->iap_syn.maxPacketSize = 0x400;
    link->iap_syn.retransmitTimeout = 1000;
    link->iap_syn.cumAckTimeout = 200;
    link->iap_syn.maxRetransmissions = 20;
    link->iap_syn.maxCumAck = 1;

    if ( link->transport_ops->setup_syn_params )
        link->transport_ops->setup_syn_params(link, &link->iap_syn);

    iap2_setup_sessions(link->acc, &link->iap_syn);

    pthread_mutex_lock(&link->iap2_mutex);
    link->run_loop = iAP2LinkRunLoopCreateAccessory(
                                                    &link->iap_syn,
                                                    link,
                                                    iap2_send_packet,
                                                    iap2_recv_packet,
                                                    iap2_connected_cb,
                                                    iap2_send_detect,
                                                    0, /* Don't validate SYN. This erroneously disallows SYN
                                                     zero-retransmit params for safe transports such as BT */
                                                    252, /* among other things, iAP2Link uses this + 2 for the length of the timeout list.
                                                     255=invalid, so 254 - 2 = 252. */
                                                    NULL
                                                    );

    iAP2LogEnable(kiAP2LogTypeLogDbg);
    iAP2LinkRunLoopAttached(link->run_loop);
    pthread_mutex_unlock(&link->iap2_mutex);

    ref_up(&link->ref);
    if ( pthread_create(&link->thread, NULL, iap2_link_thread, link) < 0 )
    {
        pthread_mutex_lock(&link->iap2_mutex);
        iAP2LinkRunLoopDelete(link->run_loop);
        pthread_mutex_unlock(&link->iap2_mutex);

        link->run_loop = NULL;

        iap2_link_shutdown(link);
        ref_down(&link->ref);

        return -1;
    }

    if ( pthread_detach(link->thread) == 0 )
    {
        hsk_print(E_LEVEL_ERROR, "[iap2_link_start] Detach of link thread failed");
    }

    return 0;
}

void iap2_link_shutdown(struct iap2_link *link)
{
    if ( iap2_link_get_state(link) == IAP2_LINK_DEAD )
    {
        hsk_print(E_LEVEL_DEBUG, "[iap2_link_shutdown] link shutdown called for dead link %p", link);
        return;
    }

    hsk_print(E_LEVEL_DEBUG, "[iap2_link_shutdown] shutting down link %p", link);

    iap2_link_set_state(link, IAP2_LINK_DEAD);

    // the loop could be blocked in epoll, wake it
    uint64_t token = 1;
    write(link->event_fd, &token, sizeof(token));
}

static void acc_drop(struct refcnt *ref)
{
    struct iap2_accessory *acc = containerof(ref, struct iap2_accessory, ref);

    vec_foreach(acc->ea_protocols, struct iap2_ea_protocol*, proto)
            free((void*)proto->name);

    vec_free(acc->ea_protocols);
    vec_free(acc->sent_msgs);
    vec_free(acc->ctrl_cb);
    pthread_mutex_destroy(&acc->auth_mutex);
    close(acc->epfd);
    free(acc);
}

struct iap2_accessory *iap2_new(struct iap2_accessory_description *desc, const char *auth_dev, void (*new_link)(struct iap2_link *link),
                                int (*new_eap_session)(struct iap2_link *link, const char *proto, int fd), void *ctx)
{
    struct iap2_accessory *acc;

    if ( strlen(auth_dev) > PATH_MAX - 1 )
        return NULL;

    acc = calloc(sizeof(*acc), 1);
    if ( !acc )
        return NULL;

    acc->description = desc;
    acc->user_ctx = ctx;
    acc->new_link_cb = new_link;
    acc->new_eap_session_cb = new_eap_session;

    ref_init(acc, struct iap2_accessory, ref, acc_drop);

    strncpy(acc->auth_dev, auth_dev, strlen(auth_dev));
    acc->auth_cert_size = 0;

    acc->epfd = epoll_create1(EPOLL_CLOEXEC);
    if ( !acc->epfd )
    {
        hsk_print(E_LEVEL_ERROR, "[iap2_new] epoll_create1 failed");
        goto err_acc;
    }

    if ( pthread_mutex_init(&acc->auth_mutex, NULL) < 0 )
        goto err_epoll;

    acc->ctrl_cb = vec_new(sizeof(struct iap2_ctrl_cb), 0);
    if ( !acc->ctrl_cb )
        goto err_auth_mutex;

    acc->sent_msgs = vec_new(sizeof(uint16_t), 0);
    if ( !acc->sent_msgs )
        goto err_ctrl_cb;

    acc->ea_protocols = vec_new(sizeof(struct iap2_ea_protocol), 0);
    if ( !acc->ea_protocols )
        goto err_sent_msgs;

    list_initialize(&acc->transports);

    return acc;

err_sent_msgs:
    vec_free(acc->sent_msgs);

err_ctrl_cb:
    vec_free(acc->ctrl_cb);

err_auth_mutex:
    pthread_mutex_destroy(&acc->auth_mutex);

err_epoll:
    close(acc->epfd);

err_acc:
    free(acc);
    return NULL;
}

void iap2_ref(struct iap2_accessory *acc)
{
    ref_up(&acc->ref);
}

void iap2_unref(struct iap2_accessory *acc)
{
    ref_down(&acc->ref);
}

struct iap2_accessory *iap2_link_get_accessory(struct iap2_link *link)
{
    return link->acc;
}

struct iap2_transport* iap2_link_get_transport(struct iap2_link *link)
{
    return link->transport;
}

enum iap2_link_state iap2_link_wait_state_change(struct iap2_link *link, enum iap2_link_state last_state)
{
    enum iap2_link_state new_state;

    pthread_mutex_lock(&link->state_mutex);

    while ( 1 )
    {
        new_state = link->state;

        if ( new_state != last_state )
            break;

        if ( pthread_cond_wait(&link->state_cond, &link->state_mutex) == 0 )
        {
            hsk_print(E_LEVEL_ERROR, "[iap2_link_wait_state_change] Wait condition of link thread failed");
        }
    }

    pthread_mutex_unlock(&link->state_mutex);

    return new_state;
}

void iap2_link_set_state(struct iap2_link *link, enum iap2_link_state state)
{
    pthread_mutex_lock(&link->state_mutex);

    link->state = state;
    pthread_cond_signal(&link->state_cond);

    pthread_mutex_unlock(&link->state_mutex);
}

enum iap2_link_state iap2_link_get_state(struct iap2_link *link)
{
    enum iap2_link_state state;

    pthread_mutex_lock(&link->state_mutex);
    state = link->state;
    pthread_mutex_unlock(&link->state_mutex);

    return state;
}

void iap2_link_runloop_updated(struct iap2_link *link)
{
    struct iap2_ea_session *sess, *tmp;
    list_for_every_entry_safe(&link->ea_sessions, sess, tmp, struct iap2_ea_session, list)
    {
        iap2_ea_runloop_updated(sess);
    }
}

int iap2_register_ea_protocol(struct iap2_accessory *acc, const char *name, enum iap2_ea_match_action match_action)
{
    struct iap2_ea_protocol proto;

    proto.name = strdup(name);
    proto.match = match_action;

    if ( vec_append(acc->ea_protocols, &proto) < 0 )
    {
        free((void*)proto.name);
        return -1;
    }

    return 0;
}

void iap2_register_transport(struct iap2_accessory *acc, struct iap2_transport *transport)
{
    transport->id = list_length(&acc->transports) + 1;
    list_add_tail(&acc->transports, &transport->node);

    hsk_print(E_LEVEL_INFO, "[iap2_register_transport] registered transport type %d", transport->type);
}

void iap2_remove_transport(struct iap2_accessory *acc, struct iap2_transport *transport)
{
    list_delete(&transport->node);
}

struct iap2_transport* iap2_get_transport_by_type(struct iap2_accessory *acc, enum iap2_transport_type type)
{
    struct iap2_transport *t;
    struct iap2_transport *ttmp;

    list_for_every_entry_safe(&acc->transports, t, ttmp, struct iap2_transport, node)
    {
        if ( t->type == type )
            return t;
    }

    return NULL;
}

uint16_t iap2_transport_get_id(struct iap2_transport *transport)
{
    return transport->id;
}

enum iap2_transport_type iap2_transport_get_type(struct iap2_transport *transport)
{
    return transport->type;
}

int iap2_bt_addr2str(const struct iap2_bt_addr *a, char *str, size_t maxlen)
{
    int rc;

    rc = snprintf(str, maxlen, "%02X:%02X:%02X:%02X:%02X:%02X",
                  a->b[0],
                  a->b[1], a->b[2], a->b[3], a->b[4], a->b[5]);
    if ( rc < 0 || (size_t)rc >= maxlen )
        return -1;

    return 0;
}

static int hex2int(char c)
{
    int ret = 0;

    c = tolower(c);

    if ( '0' <= c && c <= '9' )
        ret |= c - '0';
    else if ( 'a' <= c && c <= 'f' )
        ret |= c - 'a' + 10;
    else
        return -1;

    return ret;
}

int iap2_bt_str2addr(const char *str, struct iap2_bt_addr *a)
{
    size_t n = 0;

    if ( !str )
        return -1;

    for (;;)
    {
        char x1 = *(str++);
        if ( !x1 )
            return -1;

        char x2 = *(str++);
        if ( !x2 )
            return -1;

        char sep = *(str++);

        if ( !isxdigit(x1) || !isxdigit(x2) )
            return -1;

        if ( n < 5 && !sep )
            return -1;
        if ( n == 5 && sep )
            return -1;

        int i1 = hex2int(x1);
        if ( i1 < 0 )
            return -1;
        int i2 = hex2int(x2);
        if ( i2 < 0 )
            return -1;

        a->b[n] = (i1 << 4) | i2;

        if ( n == 5 )
            return 0;

        n++;
    }
    return -1;
}

int iap2_bt_get_peer_addr(struct iap2_link *link, struct iap2_bt_addr *addr)
{
    if ( link->transport->type != IAP2_TRANSPORT_TYPE_BT )
        return -1;

    if ( !link->transport_ops->bt_get_peer_addr )
        return -1;

    return link->transport_ops->bt_get_peer_addr(link, addr);
}

struct iap2_link *iap2_bt_connect(struct iap2_accessory *acc, const struct iap2_bt_addr *addr)
{
    struct iap2_transport *transport;

    transport = iap2_get_transport_by_type(acc, IAP2_TRANSPORT_TYPE_BT);
    if ( !transport )
        return NULL;

    return transport->bt_connect(transport, addr);
}
