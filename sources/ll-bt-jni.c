#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <stdarg.h>
#include <inttypes.h>
#include <stdbool.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <pthread.h>
#include <jni.h>

#include <mutils/jni.h>

#include <iap2/iap2.h>
#include <iap2/iap2-priv.h>
#include <iap2/ll-bt-jni.h>

struct jni_priv {
	JavaVM *vm;
	jclass connector_class;
	jobject connector;

	jmethodID m_send_packet, m_stop;

	struct iap2_bt_addr peer_addr;
};

struct iap2_jni_bt_listener {
	JavaVM *vm;
	struct iap2_accessory *acc;
	jobject listener;
	jmethodID m_stop;

	struct iap2_bt_addr local_addr;
	struct iap2_transport transport;
	struct iap2_ctrl_param identification[4];
};

#define BT_CONNECTOR "net/grandcentrix/linmfi/transport/BluetoothConnector"
#define BT_LISTENER "net/grandcentrix/linmfi/transport/BluetoothListener"

static void jni_send_packet(struct iap2_link *link, const uint8_t *data, size_t len)
{
	struct jni_priv *priv = iap2_ll_priv(link);
	JNIEnv *env;

	mlog(VERBOSE, "TX");
	hexdump(data, len);
	mlog(VERBOSE, "----------");

	env = mu_jni_get_env(priv->vm);

	jbyteArray jdata;
	jdata = (*env)->NewByteArray(env, len);
	if (!jdata) {
		mlog(ERR, "couldn't create Java bytearray");
		return;
	}

	(*env)->SetByteArrayRegion(env, jdata, 0, len, (const jbyte*)data);
	(*env)->CallVoidMethod(env, priv->connector, priv->m_send_packet, jdata);
	MU_JNI_EXCEPT_NOP(env);

	(*env)->DeleteLocalRef(env, jdata);
}

static int jni_provide_identification(struct iap2_transport *transport, struct iap2_ctrl_param *param)
{
	struct iap2_jni_bt_listener *listener = containerof(transport, struct iap2_jni_bt_listener, transport);

	iap2_ctrl_param_group(param, 17, listener->identification, sizeof(listener->identification)/sizeof(*listener->identification));

	return 0;
}

static void jni_free_priv(JNIEnv *env, struct jni_priv *priv)
{
	if (priv->connector)
		(*env)->DeleteGlobalRef(env, priv->connector);

	if (priv->connector_class)
		(*env)->DeleteGlobalRef(env, priv->connector_class);

	free(priv);
}

static void jni_cleanup(struct iap2_link *link)
{
	struct jni_priv *priv = iap2_ll_priv(link);
	JNIEnv *env;

	env = mu_jni_get_env(priv->vm);

	if (priv->m_stop) {
		(*env)->CallVoidMethod(env, priv->connector, priv->m_stop);
		MU_JNI_EXCEPT_NOP(env);
	}

	jni_free_priv(env, priv);
}

static int iap2_bt_jni_get_peer_addr(struct iap2_link *link, struct iap2_bt_addr *addr)
{
	mfi_assert(link->transport->type == IAP2_TRANSPORT_TYPE_BT);

	struct jni_priv *priv = iap2_ll_priv(link);
	memcpy(addr, &priv->peer_addr, sizeof(*addr));

	return 0;
}

static struct iap2_transport_ops jni_ops = {
	.send_packet = jni_send_packet,
	.cleanup = jni_cleanup,
	.bt_get_peer_addr = iap2_bt_jni_get_peer_addr
};

static int jstring2btaddr(JNIEnv *env, jstring jstr, struct iap2_bt_addr *addr)
{
	int ret = -1;

	if (!jstr) {
		mlog(ERR, "addr is NULL");
		goto out;
	}

	const char *chars = (*env)->GetStringUTFChars(env, jstr, NULL);
	if (!chars) {
		goto out;
	}

	ret = iap2_bt_str2addr(chars, addr);
	if (ret)
		mlog(ERR, "invalid address: %s", chars);

	(*env)->ReleaseStringUTFChars(env, jstr, chars);

out:
	return ret;
}

static int iap2_jni_get_bt_addr(JNIEnv *env, jclass clazz, struct iap2_bt_addr *addr)
{
	int ret = -1;

	jmethodID get_local_addr = (*env)->GetStaticMethodID(env, clazz, "getLocalAddr", "()Ljava/lang/String;");
	MU_JNI_EXCEPT_RET(env, -1);

	jobject jstr = (*env)->CallStaticObjectMethod(env, clazz, get_local_addr);
	MU_JNI_EXCEPT_GOTO(env, out);

	if (!jstr)
		return -1;

	ret = jstring2btaddr(env, jstr, addr);

	(*env)->DeleteLocalRef(env, jstr);

out:
	return ret;
}

static int iap2_jni_bt_setup_priv(struct jni_priv *priv, JNIEnv *env, const struct iap2_bt_addr *addr)
{
	jclass connector_class = (*env)->FindClass(env, BT_CONNECTOR);
	if (!connector_class) {
		mlog(ERR, "could not locate class '%s'", BT_CONNECTOR);
		return -1;
	}

	priv->connector_class = (*env)->NewGlobalRef(env, connector_class);
	if (!priv->connector_class) {
		mlog(ERR, "could not create global reference to connector class");
		goto err_connector_local;
	}

	priv->m_send_packet = (*env)->GetMethodID(env, priv->connector_class, "sendPacket", "([B)V");
	if (!priv->m_send_packet) {
		mlog(ERR, "required method \"sendPacket\" not found");

		MU_JNI_EXCEPT_NOP(env);
		goto err_connector;
	}

	priv->m_stop = (*env)->GetMethodID(env, priv->connector_class, "stop", "()V");
	if (!priv->m_stop) {
		// stop is optional

		(*env)->ExceptionClear(env);
	}

	if ((*env)->GetJavaVM(env, &priv->vm) < 0) {
		mlog(ERR, "getting Java VM failed");
		goto err_connector;
	}

	memcpy(&priv->peer_addr, addr, sizeof(priv->peer_addr));

	return 0;

err_connector:
	(*env)->DeleteGlobalRef(env, priv->connector_class);
	priv->connector_class = NULL;
err_connector_local:
	(*env)->DeleteLocalRef(env, connector_class);

	return -1;
}

static struct iap2_link *iap2_jni_bt_new_link(struct iap2_accessory *acc,
    struct iap2_transport *transport, JNIEnv *env, bool notify, const struct iap2_bt_addr *addr)
{
	struct iap2_link *link;
	struct jni_priv *priv;

	priv = calloc(sizeof(*priv), 1);
	if (!priv) {
		mlogerrno("allocating priv failed");
		return NULL;
	}

	if (iap2_jni_bt_setup_priv(priv, env, addr) < 0) {
		free(priv);

		return NULL;
	}

	link = iap2_link_new(acc, &jni_ops, transport, priv, notify);
	if (!link) {
		mlog(ERR, "creating link failed");

		jni_free_priv(env, priv);
		return NULL;
	}

	return link;
}

// takes ownership of the passed link
static int iap2_jni_bt_start_link(struct iap2_link *link, JNIEnv *env, jobject connector)
{
	struct jni_priv *priv = (struct jni_priv*) iap2_ll_priv(link);

	priv->connector = (*env)->NewGlobalRef(env, connector);
	if (!priv->connector) {
		mlog(ERR, "creating global reference to connector failed");
		goto err;
	}

	if (iap2_link_start(link) < 0) {
		mlog(ERR, "starting link failed");
		goto err;
	}

	// at this point, ownership of the transport resources passes over to
	// the link, so we don't need to clean them up anymore

	enum iap2_link_state state;
	state = iap2_link_get_state(link);

	while (state != IAP2_LINK_DEAD && state != IAP2_LINK_CONNECTED)
		state = iap2_link_wait_state_change(link, state);

	if (state == IAP2_LINK_DEAD) {
		mlog(ERR, "link died while starting");

		iap2_link_unref(link);
		return -1;
	}

	return 0;

err:
	iap2_link_shutdown(link);
	iap2_link_unref(link);
	jni_free_priv(env, priv);

	return -1;
}

static struct iap2_link *iap2_jni_bt_connect(struct iap2_transport *transport, const struct iap2_bt_addr *iapbtaddr)
{
	struct iap2_jni_bt_listener *listener = containerof(transport, struct iap2_jni_bt_listener, transport);
	JNIEnv *env;
	struct iap2_link *link;
	struct jni_priv *priv;
	char addrbuf[18];

	if (iap2_bt_addr2str(iapbtaddr, addrbuf, sizeof(addrbuf)) != 0) {
		mlog(ERR, "can't convert addr to string");
		return NULL;
	}

	env = mu_jni_get_env(listener->vm);

	link = iap2_jni_bt_new_link(listener->acc, transport, env, false, iapbtaddr);
	if (!link)
		return NULL;

	priv = (struct jni_priv*) iap2_ll_priv(link);

	jstring jaddr = (*env)->NewStringUTF(env, addrbuf);
	if (!jaddr) {
		mlog(ERR, "creating address string failed");
		MU_JNI_EXCEPT_NOP(env);

		goto err_link;
	}

	jmethodID connect = (*env)->GetStaticMethodID(env, priv->connector_class, "connect", "(JLjava/lang/String;)L" BT_CONNECTOR ";");
	if (!connect) {
		mlog(ERR, "could not locate connection helper method");
		MU_JNI_EXCEPT_NOP(env);

		goto err_string;
	}

	jobject instance = (*env)->CallStaticObjectMethod(env, priv->connector_class, connect, (jlong)link, jaddr);
	if (!instance) {
		mlog(ERR, "connecting to peer failed");
		MU_JNI_EXCEPT_NOP(env);

		goto err_string;
	}

	(*env)->DeleteLocalRef(env, jaddr);

	if (iap2_jni_bt_start_link(link, env, instance) < 0) {
		(*env)->DeleteLocalRef(env, instance);
		return NULL;
	}

	(*env)->DeleteLocalRef(env, instance);

	return link;

err_string:
	(*env)->DeleteLocalRef(env, jaddr);
err_link:
	iap2_link_shutdown(link);
	iap2_link_unref(link);
	free(priv);

	return NULL;
}

IAP2_EXPORT struct iap2_jni_bt_listener *iap2_jni_bt_listen(struct iap2_accessory *acc, JNIEnv *env)
{
	struct iap2_jni_bt_listener *listener;

	jclass connector_class = (*env)->FindClass(env, BT_CONNECTOR);
	if (!connector_class) {
		mlog(ERR, "could not locate class '%s'", BT_CONNECTOR);
		return NULL;
	}

	listener = calloc(sizeof(*listener), 1);
	if (!listener) {
		mlogerrno("failed to allocate listener struct");
		goto err_connector_local;
	}

	iap2_ref(acc);
	listener->acc = acc;
	listener->transport.type = IAP2_TRANSPORT_TYPE_BT;
	listener->transport.provide_identification = jni_provide_identification;
	listener->transport.bt_connect = iap2_jni_bt_connect;

	if ((*env)->GetJavaVM(env, &listener->vm) < 0) {
		mlog(ERR, "getting Java VM failed");
		goto err_listener;
	}

	if (iap2_jni_get_bt_addr(env, connector_class, &listener->local_addr) < 0) {
		mlog(ERR, "could not retrieve adapter Bluetooth address");
		goto err_listener;
	}

	jclass listener_class = (*env)->FindClass(env, BT_LISTENER);
	if (!listener_class) {
		mlog(ERR, "could not locate class '%s'", BT_LISTENER);
		goto err_listener;
	}

	jmethodID constructor = (*env)->GetMethodID(env, listener_class, "<init>", "(JJ)V");
	if (!constructor) {
		mlog(ERR, "could not locate constructor");
		MU_JNI_EXCEPT_NOP(env);

		goto err_class;
	}

	jmethodID start = (*env)->GetMethodID(env, listener_class, "start", "()V");
	if (!start) {
		mlog(ERR, "could not locate start method");
		MU_JNI_EXCEPT_NOP(env);

		goto err_class;
	}

	listener->m_stop = (*env)->GetMethodID(env, listener_class, "stop", "()V");
	if (!listener->m_stop) {
		mlog(ERR, "could not locate stop method");
		MU_JNI_EXCEPT_NOP(env);

		goto err_class;
	}

	jobject instance = (*env)->NewObject(env, listener_class, constructor, (jlong)acc, (jlong)listener);
	if (!instance) {
		mlog(ERR, "constructing listener failed");
		MU_JNI_EXCEPT_NOP(env);

		goto err_class;
	}

	listener->listener = (*env)->NewGlobalRef(env, instance);
	if (!listener->listener) {
		mlog(ERR, "creating global reference to connector failed");

		goto err_instance;
	}

	iap2_register_transport(acc, &listener->transport);
	iap2_ctrl_param_int(&listener->identification[0], 0, 2, listener->transport.id);
	iap2_ctrl_param_string(&listener->identification[1], 1, "bluetooth");
	iap2_ctrl_param_none(&listener->identification[2], 2);
	iap2_ctrl_param_blob(&listener->identification[3], 3, listener->local_addr.b, 6);

	(*env)->CallVoidMethod(env, listener->listener, start);
	MU_JNI_EXCEPT_GOTO(env, err_remove_transport);

	(*env)->DeleteLocalRef(env, instance);
	(*env)->DeleteLocalRef(env, listener_class);
	(*env)->DeleteLocalRef(env, connector_class);
	return listener;

err_remove_transport:
	iap2_remove_transport(acc, &listener->transport);
err_global_ref:
	(*env)->DeleteGlobalRef(env, listener->listener);
err_instance:
	(*env)->DeleteLocalRef(env, instance);
err_class:
	(*env)->DeleteLocalRef(env, listener_class);
err_listener:
	iap2_unref(listener->acc);
	free(listener);
err_connector_local:
	(*env)->DeleteLocalRef(env, connector_class);

	return NULL;
}

IAP2_EXPORT void iap2_jni_bt_listen_stop(JNIEnv *env, struct iap2_jni_bt_listener *listener)
{
	(*env)->CallVoidMethod(env, listener->listener, listener->m_stop);
	MU_JNI_EXCEPT_CLEAR(env);

	(*env)->DeleteGlobalRef(env, listener->listener);

	iap2_remove_transport(listener->acc, &listener->transport);
	iap2_unref(listener->acc);
	free(listener);
}

JNIEXPORT void JNICALL Java_net_grandcentrix_linmfi_Native_iap2_1ll_1submit_1data(JNIEnv *env, jclass clazz, jlong link_, jbyteArray jdata, jint jlen)
{
	struct iap2_link *link = (struct iap2_link*) link_;
	jbyte *data;
	jsize len;

	if (jlen <= 0)
		return;

	len = (*env)->GetArrayLength(env, jdata);
	if (jlen < len)
		len = jlen;

	data = (*env)->GetByteArrayElements(env, jdata, NULL);
	if (!data) {
		mlog(ERR, "retrieving bytes failed, not submitting anything");
		return;
	}

	mlog(VERBOSE, "RX");
	hexdump(data, len);
	mlog(VERBOSE, "----------");

	iap2_ll_submit_data(link, (uint8_t*)data, len);

	(*env)->ReleaseByteArrayElements(env, jdata, data, JNI_ABORT);
}

JNIEXPORT void JNICALL Java_net_grandcentrix_linmfi_Native_iap2_1link_1shutdown(JNIEnv *env, jclass clazz, jlong link_)
{
	struct iap2_link *link = (struct iap2_link*) link_;

	iap2_link_shutdown(link);
}

JNIEXPORT void JNICALL Java_net_grandcentrix_linmfi_Native_iap2_1new_1bt(JNIEnv *env, jclass clazz, jlong acc_, jlong listener_, jobject socket, jstring jaddr)
{
	struct iap2_accessory *acc = (struct iap2_accessory*) acc_;
	struct iap2_jni_bt_listener *listener = (struct iap2_jni_bt_listener*) listener_;
	struct iap2_link *link;
	struct jni_priv *priv;
	struct iap2_bt_addr addr;

	if (jstring2btaddr(env, jaddr, &addr)) {
		mlog(ERR, "can't convert peer address");
		return;
	}

	link = iap2_jni_bt_new_link(acc, &listener->transport, env, true, &addr);
	if (!link)
		return;

	priv = (struct jni_priv*) iap2_ll_priv(link);

	jmethodID constructor = (*env)->GetMethodID(env, priv->connector_class, "<init>", "(JLandroid/bluetooth/BluetoothSocket;)V");
	if (!constructor) {
		mlog(ERR, "could not locate constructor");
		MU_JNI_EXCEPT_NOP(env);

		goto err_link;
	}

	jobject instance = (*env)->NewObject(env, priv->connector_class, constructor, (jlong)link, socket);
	if (!instance) {
		mlog(ERR, "constructing connector failed");
		MU_JNI_EXCEPT_NOP(env);

		goto err_link;
	}

	iap2_jni_bt_start_link(link, env, instance);
	(*env)->DeleteLocalRef(env, instance);

	return;

err_link:
	iap2_link_shutdown(link);
	iap2_link_unref(link);
	jni_free_priv(env, priv);
}
