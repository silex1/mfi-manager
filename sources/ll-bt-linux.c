#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <stdarg.h>
#include <inttypes.h>
#include <stdbool.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <poll.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/rfcomm.h>

#include <leica/leica_types.h>
#include <leica/hsk_debug_linux_if.h>

#include "ll-bt-linux.h"
#include "iap2.h"
#include "iap2-dbus.h"
#include "iap2-priv.h"

#include "io.h"

#define BLUEZ_SERVICE "org.bluez"
#define BLUEZ_PROFILE_MANAGER_INTERFACE BLUEZ_SERVICE ".ProfileManager1"
#define BLUEZ_PROFILE_INTERFACE BLUEZ_SERVICE ".Profile1"
#define BLUEZ_AGENT_INTERFACE BLUEZ_SERVICE ".Agent1"

#define PROFILE_INTROSPECT_XML                                          \
    DBUS_INTROSPECT_1_0_XML_DOCTYPE_DECL_NODE                           \
    "<node>"                                                            \
    " <interface name=\"" BLUEZ_PROFILE_INTERFACE "\">"                 \
    "  <method name=\"Release\">"                                       \
    "  </method>"                                                       \
    "  <method name=\"RequestDisconnection\">"                          \
    "   <arg name=\"device\" direction=\"in\" type=\"o\"/>"             \
    "  </method>"                                                       \
    "  <method name=\"NewConnection\">"                                 \
    "   <arg name=\"device\" direction=\"in\" type=\"o\"/>"             \
    "   <arg name=\"fd\" direction=\"in\" type=\"h\"/>"                 \
    "   <arg name=\"opts\" direction=\"in\" type=\"a{sv}\"/>"           \
    "  </method>"                                                       \
    " </interface>"                                                     \
    " <interface name=\"org.freedesktop.DBus.Introspectable\">"         \
    "  <method name=\"Introspect\">"                                    \
    "   <arg name=\"data\" type=\"s\" direction=\"out\"/>"              \
    "  </method>"                                                       \
    " </interface>"                                                     \
    "</node>"

static const char *iap2_dbus_profile = "/Profile/iAP2";
static const char *iap2_dbus_agent = "/Agent";
static const char *iap2_dbus_uuid = "00000000-DECA-FADE-DECA-DEAFDECACAFF";

struct bt_priv
{
    int fd;
    struct iap2_epoll epoll_ctx;
    bdaddr_t peer_addr;
};

/// iAP BT Transport MFi Spec §56 (_Bluetooth_)
struct iap2_bt_listener
{
    DBusConnection *conn;
    struct iap2_dbus_connection *iap2dbus;
    struct iap2_accessory *acc;
    bdaddr_t local_addr;
    struct iap2_transport transport;
    struct iap2_ctrl_param identification[4];
};

static void iap2addr_to_bdaddr(bdaddr_t *dst, const struct iap2_bt_addr *src)
{
    dst->b[0] = src->b[5];
    dst->b[1] = src->b[4];
    dst->b[2] = src->b[3];
    dst->b[3] = src->b[2];
    dst->b[4] = src->b[1];
    dst->b[5] = src->b[0];
}

static void bdaddr_to_iap2addr(struct iap2_bt_addr *dst, const bdaddr_t *src)
{
    dst->b[0] = src->b[5];
    dst->b[1] = src->b[4];
    dst->b[2] = src->b[3];
    dst->b[3] = src->b[2];
    dst->b[4] = src->b[1];
    dst->b[5] = src->b[0];
}

static void bt_epoll_cb(struct iap2_epoll *epoll_ctx, uint32_t events)
{
    struct iap2_link *link = (struct iap2_link*)epoll_ctx->arg;
    uint8_t buf[128 * 1024];

    ssize_t len = recv(epoll_ctx->fd, buf, sizeof(buf), 0);

    if ( len < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "bt read");
        iap2_link_shutdown(link);

        return;
    }

//    mlog(VERBOSE, "RX");
//    hexdump(buf, len);
//    mlog(VERBOSE, "----------");

    iap2_ll_submit_data(link, buf, len);
}

static int rfcomm_tx(int fd, const uint8_t *data, size_t len)
{
    ssize_t ret;

    while ( len )
    {
        ret = send(fd, data, len, 0);
        if ( ret < 0 )
        {
            if ( errno == EINTR )
                continue;
            else if ( errno == EAGAIN || errno == EWOULDBLOCK )
            {
                ret = wait_for_poll_events(fd, POLLOUT, -1);
                if ( ret )
                    return -1;
                continue;
            }
            else
            {
                hsk_print(E_LEVEL_ERROR, "bt send");
                return -1;
            }
        }

        data += ret;
        len -= ret;
    }

    return 0;
}

static void iap2_bt_send_packet(struct iap2_link *link, const uint8_t *data, size_t len)
{
    struct bt_priv *priv = iap2_ll_priv(link);

//    mlog(VERBOSE, "TX");
//    hexdump(data, len);
//    mlog(VERBOSE, "----------");

    if ( rfcomm_tx(priv->fd, data, len) < 0 )
        iap2_link_shutdown(link);
}

static void iap2_bt_cleanup(struct iap2_link *link)
{
    struct bt_priv *priv = iap2_ll_priv(link);

    iap2_unregister_fd(link->epfd, priv->fd);
    shutdown(priv->fd, SHUT_RDWR);
    close(priv->fd);

    free(priv);
}

/** Provide BluetoothTransportComponent Information for IdentificationInformation.
 *
 * Used to provide information about the BT transport during accessory identification.
 *
 * See MFi Spec Table 90-9 and Table 90-17
 *
 * @param[in] transport iAP BT Transport
 * @param[out] slot BluetoothTransportComponent parameter group
 * @return 0 on success.
 */
static int iap2_bt_provide_identification(struct iap2_transport *transport, struct iap2_ctrl_param *slot)
{
    struct iap2_bt_listener *listener = containerof(transport, struct iap2_bt_listener, transport);
    iap2_ctrl_param_group(slot, 17, listener->identification, sizeof(listener->identification) / sizeof(*listener->identification));
    return 0;
}

static int iap2_bt_linux_get_peer_addr(struct iap2_link *link, struct iap2_bt_addr *addr)
{
    struct bt_priv *priv = iap2_ll_priv(link);
    bdaddr_to_iap2addr(addr, &priv->peer_addr);
    return 0;
}

static struct iap2_transport_ops bt_ops = {
                                           .send_packet = iap2_bt_send_packet,
                                           .cleanup = iap2_bt_cleanup,
                                           .bt_get_peer_addr = iap2_bt_linux_get_peer_addr
};

static struct iap2_link *iap2_bt_new_link(struct iap2_accessory *acc,
                                          struct iap2_transport *transport,
                                          int fd, bdaddr_t peer, bool notify)
{
    struct iap2_link *link;
    struct bt_priv *priv;

    // TODO: check if we're using the HCI we expect

    priv = calloc(sizeof(*priv), 1);
    if ( !priv )
    {
        hsk_print(E_LEVEL_ERROR, "allocating bt_priv failed");
        return NULL;
    }

    priv->fd = fd;
    bacpy(&priv->peer_addr, &peer);

    link = iap2_link_new(acc, &bt_ops, transport, priv, notify);
    if ( !link )
    {
        hsk_print(E_LEVEL_ERROR, "creating link failed");

        iap2_bt_cleanup(link);
        return NULL;
    }

    if ( iap2_register_fd(link->epfd, priv->fd, EPOLLIN, bt_epoll_cb, &priv->epoll_ctx, link) < 0 )
        goto err_link;

    return link;

err_link:
    iap2_link_unref(link);
    free(priv);

    return NULL;
}

static int iap2_bt_start_link(struct iap2_link *link)
{
    if ( iap2_link_start(link) < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "starting link failed");
        goto err;
    }

    hsk_print(E_LEVEL_DEBUG, "waiting for link %p to connect", link);

    enum iap2_link_state state;
    state = iap2_link_get_state(link);

    while ( state != IAP2_LINK_DEAD && state != IAP2_LINK_CONNECTED )
        state = iap2_link_wait_state_change(link, state);

    if ( state == IAP2_LINK_DEAD )
    {
        hsk_print(E_LEVEL_DEBUG, "link %p did not start properly", link);
        return -1;
    }

    return 0;

err:
    iap2_link_shutdown(link);
    iap2_bt_cleanup(link);

    return -1;
}

/** Attempts to establish a BT connection to a device.
 *
 * Attempts to connect to the device with the address @p iapbtaddr via the BT transport @p transport.
 *
 * @param[in] transport iAP BT transport
 * @param[in] iapbtaddr 48 bit hexadecimal BT address
 * @return Link to device on success, NULL otherwise.
 */
static struct iap2_link *iap2_bt_linux_connect(struct iap2_transport *transport, const struct iap2_bt_addr *iapbtaddr)
{
    struct iap2_bt_listener *listener = containerof(transport, struct iap2_bt_listener, transport);
    struct iap2_link *link;
    int fd;
    bdaddr_t bdaddr;

    hsk_print(E_LEVEL_INFO, "[iap2_bt_linux_connect]");

    iap2addr_to_bdaddr(&bdaddr, iapbtaddr);

    fd = socket(AF_BLUETOOTH, SOCK_STREAM | SOCK_CLOEXEC, BTPROTO_RFCOMM);
    if ( fd < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "[iap2_bt_linux_connect] bt socket()");
        return NULL;
    }

    struct sockaddr_rc addr;
    addr.rc_family = AF_BLUETOOTH;
    addr.rc_bdaddr = bdaddr;

    // TODO: do this via SDP
    addr.rc_channel = 1;

    if ( connect(fd, (struct sockaddr*)&addr, sizeof(addr)) < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "[iap2_bt_linux_connect] bt connect()");
        goto err_socket;
    }

    link = iap2_bt_new_link(listener->acc, transport, fd, bdaddr, false);
    if ( !link )
    {
        hsk_print(E_LEVEL_ERROR, "[iap2_bt_linux_connect] creating link failed");
        goto err_socket;
    }

    if ( iap2_bt_start_link(link) < 0 )
    {
        ref_down(&link->ref);
        return NULL;
    }

    return link;

err_socket:
    close(fd);
    hsk_print(E_LEVEL_ERROR, "[iap2_bt_linux_connect] socket error(%s)", strerror(errno));

    return NULL;
}

static int _dbus_convert_addr(void *data, void *arg)
{
    const char *straddr = (const char*)data;
    bdaddr_t *out = (bdaddr_t*)arg;

    if ( str2ba(straddr, out) )
    {
        hsk_print(E_LEVEL_ERROR, "invalid address: %s", straddr);
        return -1;
    }

    return 0;
}

static DBusMessage *profile_new_connection(struct iap2_bt_listener *listener, DBusConnection *c, DBusMessage *m)
{
    DBusMessage *r;
    DBusMessageIter arg_i;
    const char *sender;
    const char *path;
    const char *handler;
    struct iap2_link *link;
    int fd;

    if ( !dbus_message_iter_init(m, &arg_i) || strcmp(dbus_message_get_signature(m), "oha{sv}") )
    {
        hsk_print(E_LEVEL_ERROR, "Invalid signature found in NewConnection");
        goto out_ret;
    }

    handler = dbus_message_get_path(m);
    hsk_print(E_LEVEL_DEBUG, "handler: %s", handler);

    if ( dbus_message_iter_get_arg_type(&arg_i) == DBUS_TYPE_OBJECT_PATH )
    {
        hsk_print(E_LEVEL_ERROR, "Arg type is DBUS_TYPE_OBJECT_PATH");
    }
    dbus_message_iter_get_basic(&arg_i, &path);

    if ( dbus_message_iter_next(&arg_i) )
    {
        hsk_print(E_LEVEL_ERROR, "iter next failed");
    }

    if ( dbus_message_iter_get_arg_type(&arg_i) == DBUS_TYPE_UNIX_FD )
    {
        hsk_print(E_LEVEL_ERROR, "Arg type is DBUS_TYPE_UNIX_FD");
    }

    dbus_message_iter_get_basic(&arg_i, &fd);

    hsk_print(E_LEVEL_DEBUG, "dbus: NewConnection path=%s, fd=%d", path, fd);

    bdaddr_t peer_addr;
    if ( iap2_dbus_property_get(listener->conn, BLUEZ_SERVICE, path, "org.bluez.Device1", "Address", DBUS_TYPE_STRING, _dbus_convert_addr, &peer_addr) >= 0 )
    {
        char buf[18];
        ba2str(&peer_addr, buf);
        hsk_print(E_LEVEL_INFO, "new connection from %s", buf);
    }
    else
    {
        hsk_print(E_LEVEL_ERROR, "could not get peer address for peer %s", path);
        goto out_closefd;
    }

    sender = dbus_message_get_sender(m);
    hsk_print(E_LEVEL_DEBUG, "sender: %s", sender);

    link = iap2_bt_new_link(listener->acc, &listener->transport, fd, peer_addr, true);
    if ( !link )
    {
        hsk_print(E_LEVEL_ERROR, "creating link failed");
        goto out_closefd;
    }

    if ( iap2_bt_start_link(link) < 0 )
    {
        ref_down(&link->ref);
        goto out_closefd;
    }

    ref_down(&link->ref);

    r = dbus_message_new_method_return(m);
    assert(r);
    return r;

out_closefd:
    shutdown(fd, SHUT_RDWR);
    close(fd);

out_ret:
    r = dbus_message_new_error(m, "org.bluez.Error.InvalidArguments", "Unable to handle new connection");
    assert(r);
    return r;
}

static DBusHandlerResult profile_handler(DBusConnection *c, DBusMessage *m, void *userdata)
{
    dbus_bool_t dbusrc;
    DBusMessage *r = NULL;
    struct iap2_bt_listener *listener = userdata;

    if ( dbus_message_is_method_call(m, "org.freedesktop.DBus.Introspectable", "Introspect") )
    {
        const char *xml = PROFILE_INTROSPECT_XML;
        r = dbus_message_new_method_return(m);
        dbus_message_append_args(r, DBUS_TYPE_STRING, &xml, DBUS_TYPE_INVALID);
    }
    else if ( dbus_message_is_method_call(m, BLUEZ_PROFILE_INTERFACE, "NewConnection") )
    {
        hsk_print(E_LEVEL_DEBUG, "connection");
        r = profile_new_connection(listener, c, m);
    }
    else if ( dbus_message_is_method_call(m, BLUEZ_PROFILE_INTERFACE, "RequestDisconnection") )
    {
        hsk_print(E_LEVEL_DEBUG, "disconnection");
    }
    else
    {
        hsk_print(E_LEVEL_ERROR, "unsupported message");
        return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
    }

    if ( r )
    {
        dbusrc = dbus_connection_send(c, r, NULL);
        if ( !dbusrc )
        {
            hsk_print(E_LEVEL_ERROR, "can't send dbus message");
        }
        dbus_message_unref(r);
    }

    return DBUS_HANDLER_RESULT_HANDLED;
}

static DBusObjectPathVTable dbus_profile_vt = {
                                               .message_function = profile_handler
};

static DBusMessage* agent_authorize_service(DBusConnection *c, DBusMessage *m)
{
    const char *path;
    const char *uuid;
    DBusMessageIter arg_i;
    DBusMessage *r = NULL;

    if ( !dbus_message_iter_init(m, &arg_i) || strcmp(dbus_message_get_signature(m), "os") )
    {
        hsk_print(E_LEVEL_ERROR, "Invalid signature found in AuthorizeService");
        r = dbus_message_new_error(m, "org.bluez.Error.Rejected", "Invalid signature");
        assert(r);
        goto fail;
    }

    if ( dbus_message_iter_get_arg_type(&arg_i) == DBUS_TYPE_OBJECT_PATH )
    {
        hsk_print(E_LEVEL_ERROR, "Arg type is DBUS_TYPE_OBJECT_PATH");
    }

    dbus_message_iter_get_basic(&arg_i, &path);

    if ( dbus_message_iter_next(&arg_i) )
    {
        hsk_print(E_LEVEL_ERROR, "iter next failed");
    }
    if ( dbus_message_iter_get_arg_type(&arg_i) == DBUS_TYPE_STRING )
    {
        hsk_print(E_LEVEL_ERROR, "Arg type is DBUS_TYPE_STRING");
    }

    dbus_message_iter_get_basic(&arg_i, &uuid);

    hsk_print(E_LEVEL_DEBUG, "AuthorizeService: path=%s uuid=%s", path, uuid);

    // TODO: stop advertizing protocols which we don't support
    if ( 1 || !strcmp(uuid, iap2_dbus_uuid) )
        r = dbus_message_new_method_return(m);
    else
        r = dbus_message_new_error(m, "org.bluez.Error.Rejected", "UUID not supported");
    assert(r);

fail:
    return r;
}

static DBusHandlerResult agent_handler(DBusConnection *c, DBusMessage *m, void *userdata)
{
    dbus_bool_t dbusrc;
    DBusMessage *r = NULL;

    if ( dbus_message_is_method_call(m, BLUEZ_AGENT_INTERFACE, "AuthorizeService") )
    {
        r = agent_authorize_service(c, m);
    }
    else
    {
        hsk_print(E_LEVEL_ERROR, "unsupported message");
        return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
    }

    if ( r )
    {
        dbusrc = dbus_connection_send(c, r, NULL);
        if ( !dbusrc )
        {
            hsk_print(E_LEVEL_ERROR, "can't send dbus message");
        }
        dbus_message_unref(r);
    }

    return DBUS_HANDLER_RESULT_HANDLED;
}

static DBusObjectPathVTable dbus_agent_vt = {
                                             .message_function = agent_handler
};

static int iap2_bt_get_address(bdaddr_t *paddr)
{
    int ret = -1;
    DBusError error;
    DBusConnection *conn;

    dbus_error_init(&error);
    conn = dbus_bus_get_private(DBUS_BUS_SYSTEM, &error);
    if ( !conn )
    {
        hsk_print(E_LEVEL_ERROR, "opening dbus connection failed: %s", error.message);
        dbus_error_free(&error);
        return -1;
    }
    dbus_connection_set_exit_on_disconnect(conn, false);

    ret = iap2_dbus_property_get(conn, BLUEZ_SERVICE, "/org/bluez/hci0", "org.bluez.Adapter1", "Address", DBUS_TYPE_STRING, _dbus_convert_addr, paddr);

    dbus_connection_close(conn);
    dbus_connection_unref(conn);

    return ret;
}

static int iap2_bt_register_agent(DBusConnection *conn, const char *path, const char *capabilities)
{
    int ret = -1;
    DBusError error;
    DBusMessage *msg = NULL;
    DBusMessage *reply;
    DBusMessageIter iter;

    dbus_error_init(&error);

    msg = dbus_message_new_method_call(BLUEZ_SERVICE, "/org/bluez", "org.bluez.AgentManager1", "RegisterAgent");
    if ( !msg )
    {
        hsk_print(E_LEVEL_ERROR, "creating message failed");
        return -1;
    }

    dbus_message_iter_init_append(msg, &iter);
    dbus_message_iter_append_basic(&iter, DBUS_TYPE_OBJECT_PATH, &path);
    dbus_message_iter_append_basic(&iter, DBUS_TYPE_STRING, &capabilities);

    reply = dbus_connection_send_with_reply_and_block(conn, msg, DBUS_TIMEOUT_USE_DEFAULT, &error);
    if ( !reply )
    {
        hsk_print(E_LEVEL_ERROR, "Can't send RegisterAgent dbus message: %s", error.message);
        dbus_error_free(&error);
        goto err_msg_unref;
    }

    ret = 0;

    err_msg_unref:
    dbus_message_unref(msg);

    return ret;
}

static int iap2_bt_remove_agent(DBusConnection *conn, const char *path)
{
    int ret = -1;
    DBusError error;
    DBusMessage *msg = NULL;
    DBusMessage *reply;
    DBusMessageIter iter;

    dbus_error_init(&error);

    msg = dbus_message_new_method_call(BLUEZ_SERVICE, "/org/bluez", "org.bluez.AgentManager1", "UnregisterAgent");
    if ( !msg )
    {
        hsk_print(E_LEVEL_ERROR, "creating message failed");
        return -1;
    }

    dbus_message_iter_init_append(msg, &iter);
    dbus_message_iter_append_basic(&iter, DBUS_TYPE_OBJECT_PATH, &path);

    reply = dbus_connection_send_with_reply_and_block(conn, msg, DBUS_TIMEOUT_USE_DEFAULT, &error);
    if ( !reply )
    {
        hsk_print(E_LEVEL_ERROR, "Can't send RegisterAgent dbus message: %s", error.message);
        dbus_error_free(&error);
        goto err_msg_unref;
    }

    ret = 0;

err_msg_unref:
    dbus_message_unref(msg);

    return ret;
}

static int iap2_bt_request_default_agent(DBusConnection *conn)
{
    int ret = -1;
    DBusError error;
    DBusMessage *msg = NULL;
    DBusMessage *reply;
    DBusMessageIter iter;

    dbus_error_init(&error);

    msg = dbus_message_new_method_call(BLUEZ_SERVICE, "/org/bluez",
                                       "org.bluez.AgentManager1",
                                       "RequestDefaultAgent");
    if ( !msg )
    {
        hsk_print(E_LEVEL_ERROR, "creating message failed");
        return -1;
    }

    dbus_message_iter_init_append(msg, &iter);
    dbus_message_iter_append_basic(&iter, DBUS_TYPE_OBJECT_PATH, &iap2_dbus_agent);

    reply = dbus_connection_send_with_reply_and_block(conn, msg, DBUS_TIMEOUT_USE_DEFAULT, &error);
    if ( !reply )
    {
        hsk_print(E_LEVEL_ERROR, "Can't send RequestDefaultAgent dbus message: %s", error.message);
        dbus_error_free(&error);
        goto err_msg_unref;
    }

    ret = 0;

err_msg_unref:
    dbus_message_unref(msg);

    return ret;
}

struct iap2_bt_listener *iap2_bt_listen(struct iap2_accessory *acc, uint8_t chan)
{
    DBusConnection *conn;
    int rc;
    DBusError error;
    dbus_bool_t dbusrc;
    DBusMessageIter iter;
    DBusMessageIter d;
    DBusMessage *reply;
    struct iap2_bt_listener *listener;

    listener = calloc(sizeof(*listener), 1);
    if ( !listener )
    {
        hsk_print(E_LEVEL_ERROR, "allocating listener failed");
        return NULL;
    }

    iap2_ref(acc);
    listener->acc = acc;
    listener->transport.type = IAP2_TRANSPORT_TYPE_BT;
    listener->transport.provide_identification = iap2_bt_provide_identification;
    listener->transport.bt_connect = iap2_bt_linux_connect;

    bdaddr_t local_addr;
    rc = iap2_bt_get_address(&local_addr);
    if ( rc )
    {
        hsk_print(E_LEVEL_ERROR, "can't get local addr");
        goto err_free_listener;
    }

    // iAP2 uses a reversed byte order relative to what is customary in Linux
    baswap(&listener->local_addr, &local_addr);

    dbus_error_init(&error);
    conn = dbus_bus_get_private(DBUS_BUS_SYSTEM, &error);
    if ( !conn )
    {
        hsk_print(E_LEVEL_ERROR, "opening dbus connection failed: %s", error.message);
        dbus_error_free(&error);
        goto err_free_listener;
    }
    listener->conn = conn;

    dbusrc = dbus_connection_register_object_path(conn, iap2_dbus_agent, &dbus_agent_vt, listener);
    if ( !dbusrc )
    {
        hsk_print(E_LEVEL_ERROR, "registering agent object failed");
        goto err_close_conn;
    }

    rc = iap2_bt_register_agent(conn, iap2_dbus_agent, "NoInputNoOutput");
    if ( rc )
    {
        hsk_print(E_LEVEL_ERROR, "registering agent failed");
        goto err_unregister_obj_path_agent;
    }

    rc = iap2_bt_request_default_agent(conn);
    if ( rc )
    {
        hsk_print(E_LEVEL_ERROR, "requesting default agent failed");
        goto err_remove_agent;
    }

    dbusrc = dbus_connection_register_object_path(conn, iap2_dbus_profile, &dbus_profile_vt, listener);
    if ( !dbusrc )
    {
        hsk_print(E_LEVEL_ERROR, "registering profile object failed");
        goto err_remove_agent;
    }

    DBusMessage *msg = dbus_message_new_method_call("org.bluez", "/org/bluez", BLUEZ_PROFILE_MANAGER_INTERFACE, "RegisterProfile");
    if ( !msg )
    {
        hsk_print(E_LEVEL_ERROR, "creating message failed");
        goto err_unregister_obj_path_profile;
    }

    dbus_message_iter_init_append(msg, &iter);
    dbus_message_iter_append_basic(&iter, DBUS_TYPE_OBJECT_PATH, &iap2_dbus_profile);
    dbus_message_iter_append_basic(&iter, DBUS_TYPE_STRING, &iap2_dbus_uuid);
    dbus_message_iter_open_container(&iter, DBUS_TYPE_ARRAY, DBUS_DICT_ENTRY_BEGIN_CHAR_AS_STRING DBUS_TYPE_STRING_AS_STRING DBUS_TYPE_VARIANT_AS_STRING DBUS_DICT_ENTRY_END_CHAR_AS_STRING, &d);

    const char *role = "server";
    iap2_dbus_append_basic_variant_dict_entry(&d, "Role", DBUS_TYPE_STRING, &role);

    uint16_t channel = (uint16_t)chan;
    iap2_dbus_append_basic_variant_dict_entry(&d, "Channel", DBUS_TYPE_UINT16, &channel);
    dbus_message_iter_close_container(&iter, &d);

    reply = dbus_connection_send_with_reply_and_block(conn, msg, DBUS_TIMEOUT_USE_DEFAULT, &error);
    if ( !reply )
    {
        hsk_print(E_LEVEL_ERROR, "Can't send RegisterProfile dbus message: %s", error.message);
        dbus_error_free(&error);
        goto err_msg_unref;
    }

    listener->iap2dbus = iap2_dbus_register(acc->epfd, conn);
    if ( !listener->iap2dbus )
    {
        hsk_print(E_LEVEL_ERROR, "can't register dbus connection with iap2");
        goto err_msg_unref;
    }

    iap2_register_transport(acc, &listener->transport);
    iap2_ctrl_param_int(&listener->identification[0], 0, 2, listener->transport.id);
    iap2_ctrl_param_string(&listener->identification[1], 1, "bluetooth");
    iap2_ctrl_param_none(&listener->identification[2], 2);
    iap2_ctrl_param_blob(&listener->identification[3], 3, listener->local_addr.b, 6);

    dbus_message_unref(msg);
    hsk_print(E_LEVEL_INFO, "Listener initialized");
    return listener;

err_msg_unref:
    dbus_message_unref(msg);

err_unregister_obj_path_profile:
    dbus_connection_unregister_object_path(conn, iap2_dbus_profile);

err_remove_agent:
    iap2_bt_remove_agent(conn, iap2_dbus_agent);

err_unregister_obj_path_agent:
    dbus_connection_unregister_object_path(conn, iap2_dbus_agent);

err_close_conn:
    dbus_connection_close(conn);
    dbus_connection_unref(conn);

err_free_listener:
    iap2_unref(acc);
    free(listener);

    return NULL;
}

void iap2_bt_listen_stop(struct iap2_bt_listener *listener)
{
    iap2_remove_transport(listener->acc, &listener->transport);
    iap2_dbus_remove(listener->iap2dbus);
    dbus_connection_unregister_object_path(listener->conn, iap2_dbus_profile);
    iap2_bt_remove_agent(listener->conn, iap2_dbus_agent);
    dbus_connection_unregister_object_path(listener->conn, iap2_dbus_agent);
    dbus_connection_close(listener->conn);
    dbus_connection_unref(listener->conn);
    iap2_unref(listener->acc);
    free(listener);
}

int iap2_bt_enter_pairing(void)
{
    int ret = -1;
    int rc;
    DBusConnection *conn;
    DBusError error;

    dbus_error_init(&error);
    conn = dbus_bus_get_private(DBUS_BUS_SYSTEM, &error);
    if ( !conn )
    {
        hsk_print(E_LEVEL_ERROR, "opening dbus connection failed: %s", error.message);
        dbus_error_free(&error);
        return -1;
    }
    dbus_connection_set_exit_on_disconnect(conn, false);

    int powered = true;
    rc = iap2_dbus_property_set(conn, BLUEZ_SERVICE, "/org/bluez/hci0", "org.bluez.Adapter1", "Powered", DBUS_TYPE_BOOLEAN, &powered);
    if ( rc )
    {
        hsk_print(E_LEVEL_ERROR, "can't power on bt device");
        goto close_unref;
    }

    int discoverable = true;
    rc = iap2_dbus_property_set(conn, BLUEZ_SERVICE, "/org/bluez/hci0", "org.bluez.Adapter1", "Discoverable", DBUS_TYPE_BOOLEAN, &discoverable);
    if ( rc )
    {
        hsk_print(E_LEVEL_ERROR, "can't make discoverable");
        goto close_unref;
    }

    int pairable = true;
    rc = iap2_dbus_property_set(conn, BLUEZ_SERVICE, "/org/bluez/hci0", "org.bluez.Adapter1", "Pairable", DBUS_TYPE_BOOLEAN, &pairable);
    if ( rc )
    {
        hsk_print(E_LEVEL_ERROR, "can't make pairable");
        goto close_unref;
    }

    ret = 0;
    goto close_unref;

close_unref:
    dbus_connection_close(conn);
    dbus_connection_unref(conn);

    return ret;
}
