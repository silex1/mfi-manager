#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/eventfd.h>
#include <sys/syscall.h>
#include <endian.h>
#include <linux/aio_abi.h>
#include <linux/usb/functionfs.h>

#include <leica/leica_types.h>
#include <leica/hsk_debug_linux_if.h>

#define USB_SUBCLASS_MFI 0xf0
#define USB_PROTOCOL_MFI 0x00
#define USB_INTERFACE_STRING "iAP Interface"

#define BUF_LEN         8192

#include "iap2.h"
#include "iap2-priv.h"
#include "ll-usb-ffs.h"

#define MAX_PAYLOAD_SIZE 0xffff
#define USB_EP_SEND 1
#define USB_EP_RECV 2

#define IO_RX_BUFS 128
#define IO_TX_BUFS 128
#define IO_BUFS (IO_RX_BUFS + IO_TX_BUFS)

#define IO_RX_BUF(x) (x)
#define IO_TX_BUF(x) (IO_RX_BUFS + (x))

struct usb_io_buf
{
    struct iocb iocb;
    uint8_t buf[MAX_PAYLOAD_SIZE];
    bool pending;

    pthread_mutex_t mutex;
};

struct iap2_usb_listener
{
    char *ffs_dir;

    struct iap2_accessory *acc;
    struct iap2_link *link;

    struct iap2_transport transport;
    struct iap2_ctrl_param identification[3];

    int ep[3], eventfd;
    struct iap2_epoll ep0_ctx, eventfd_ctx;

    aio_context_t io_ctx;

    // RX bufs come first, then TX bufs
    struct usb_io_buf io_bufs[IO_BUFS];
};

#if __BYTE_ORDER == __BIG_ENDIAN
#define const_htole16(x) ((uint16_t)( \
	(((uint16_t)(x) & (uint16_t)0x00ffU) << 8) | \
	(((uint16_t)(x) & (uint16_t)0xff00U) >> 8)))
#define const_htole32(x) ((uint32_t)( \
	(((uint32_t)(x) & (uint32_t)0x000000ffUL) << 24) | \
	(((uint32_t)(x) & (uint32_t)0x0000ff00UL) <<  8) | \
	(((uint32_t)(x) & (uint32_t)0x00ff0000UL) >>  8) | \
	(((uint32_t)(x) & (uint32_t)0xff000000UL) >> 24)))
#else
#define const_htole32(x) (x)
#define const_htole16(x) (x)
#endif

/******************** Descriptors and Strings *******************************/

static const struct
{
    struct usb_functionfs_descs_head_v2 header;
    __le32 fs_count;
    __le32 hs_count;
    struct
    {
        struct usb_interface_descriptor intf;
        struct usb_endpoint_descriptor_no_audio bulk_sink;
        struct usb_endpoint_descriptor_no_audio bulk_source;
    }__attribute__ ((__packed__)) fs_descs, hs_descs;
}__attribute__ ((__packed__)) descriptors = {
                                             .header = {
                                                        .magic = const_htole32(FUNCTIONFS_DESCRIPTORS_MAGIC_V2),
                                                        .flags = const_htole32(FUNCTIONFS_HAS_FS_DESC |
                                                                               FUNCTIONFS_HAS_HS_DESC),
                                                        .length = const_htole32(sizeof(descriptors)),
                                             },
                                             .fs_count = const_htole32(3),
                                             .fs_descs = {
                                                          .intf = {
                                                                   .bLength = sizeof(descriptors.fs_descs.intf),
                                                                   .bDescriptorType = USB_DT_INTERFACE,
                                                                   .bNumEndpoints = 2,
                                                                   .bInterfaceClass = USB_CLASS_VENDOR_SPEC,
                                                                   .bInterfaceSubClass = USB_SUBCLASS_MFI,
                                                                   .bInterfaceProtocol = USB_PROTOCOL_MFI,
                                                                   .iInterface = 1,
                                                          },
                                                          .bulk_sink = {
                                                                        .bLength = sizeof(descriptors.fs_descs.bulk_sink),
                                                                        .bDescriptorType = USB_DT_ENDPOINT,
                                                                        .bEndpointAddress = 1 | USB_DIR_IN,
                                                                        .bmAttributes = USB_ENDPOINT_XFER_BULK,
                                                          },
                                                          .bulk_source = {
                                                                          .bLength = sizeof(descriptors.fs_descs.bulk_source),
                                                                          .bDescriptorType = USB_DT_ENDPOINT,
                                                                          .bEndpointAddress = 2 | USB_DIR_OUT,
                                                                          .bmAttributes = USB_ENDPOINT_XFER_BULK,
                                                          },
                                             },
                                             .hs_count = const_htole32(3),
                                             .hs_descs = {
                                                          .intf = {
                                                                   .bLength = sizeof(descriptors.hs_descs.intf),
                                                                   .bDescriptorType = USB_DT_INTERFACE,
                                                                   .bNumEndpoints = 2,
                                                                   .bInterfaceClass = USB_CLASS_VENDOR_SPEC,
                                                                   .bInterfaceSubClass = USB_SUBCLASS_MFI,
                                                                   .bInterfaceProtocol = USB_PROTOCOL_MFI,
                                                                   .iInterface = 1,
                                                          },
                                                          .bulk_sink = {
                                                                        .bLength = sizeof(descriptors.hs_descs.bulk_sink),
                                                                        .bDescriptorType = USB_DT_ENDPOINT,
                                                                        .bEndpointAddress = 1 | USB_DIR_IN,
                                                                        .bmAttributes = USB_ENDPOINT_XFER_BULK,
                                                                        .wMaxPacketSize = const_htole16(512),
                                                          },
                                                          .bulk_source = {
                                                                          .bLength = sizeof(descriptors.hs_descs.bulk_source),
                                                                          .bDescriptorType = USB_DT_ENDPOINT,
                                                                          .bEndpointAddress = 2 | USB_DIR_OUT,
                                                                          .bmAttributes = USB_ENDPOINT_XFER_BULK,
                                                                          .wMaxPacketSize = const_htole16(512),
                                                          },
                                             },
};

static const struct
{
    struct usb_functionfs_strings_head header;
    struct
    {
        __le16 code;
        const char str1[sizeof(USB_INTERFACE_STRING)];
    }__attribute__ ((__packed__)) lang0;
}__attribute__ ((__packed__)) strings = {
                                         .header = {
                                                    .magic = const_htole32(FUNCTIONFS_STRINGS_MAGIC),
                                                    .length = const_htole32(sizeof(strings)),
                                                    .str_count = const_htole32(1),
                                                    .lang_count = const_htole32(1),
                                         },
                                         .lang0 = {
                                                   const_htole16(0x0409), /* en-us */
                                                   USB_INTERFACE_STRING,
                                         },
};

static int io_setup(unsigned nr_events, aio_context_t *ctx_idp)
{
    return (int)syscall(SYS_io_setup, (long)nr_events, (long)ctx_idp);
}

static int io_destroy(aio_context_t ctx_id)
{
    return (int)syscall(SYS_io_destroy, (long)ctx_id);
}

static int io_submit(aio_context_t ctx_id, long nr, struct iocb **iocbpp)
{
    return (int)syscall(SYS_io_submit, (long)ctx_id, (long)nr, (long)iocbpp);
}

static int io_cancel(aio_context_t ctx_id, struct iocb *iocb, struct io_event *result)
{
    return (int)syscall(SYS_io_cancel, (long)ctx_id, (long)iocb, (long)result);
}

static int io_getevents(aio_context_t ctx_id, long min_nr, long nr, struct io_event *events, struct timespec *timeout)
{
    return (int)syscall(SYS_io_getevents, (long)ctx_id, (long)min_nr, (long)nr,
                        (long)events,
                        (long)timeout);
}

static void usb_submit(struct iap2_usb_listener *usb, struct usb_io_buf *iobuf, int ep, uint16_t opcode, size_t len)
{
    memset(&iobuf->iocb, 0, sizeof(iobuf->iocb));
    iobuf->iocb.aio_fildes = usb->ep[ep];
    iobuf->iocb.aio_lio_opcode = opcode;
    iobuf->iocb.aio_buf = (uint64_t)(uintptr_t)iobuf->buf;
    iobuf->iocb.aio_nbytes = len;
    iobuf->iocb.aio_flags |= IOCB_FLAG_RESFD;
    iobuf->iocb.aio_resfd = usb->eventfd;

    struct iocb *iocbp = &iobuf->iocb;
    if ( io_submit(usb->io_ctx, 1, &iocbp) < 0 )
        hsk_print(E_LEVEL_ERROR, "io_submit");
    else
        iobuf->pending = true;
}

static void usb_submit_read(struct iap2_usb_listener *usb, struct usb_io_buf *iobuf)
{
    usb_submit(usb, iobuf, USB_EP_RECV, IOCB_CMD_PREAD, sizeof(iobuf->buf));
}

static void usb_submit_write(struct iap2_usb_listener *usb, struct usb_io_buf *iobuf, size_t len)
{
    usb_submit(usb, iobuf, USB_EP_SEND, IOCB_CMD_PWRITE, len);
}

static void cancel_transfers(struct iap2_usb_listener *usb)
{
    // all of these may fail (for example if they just
    // completed), but here, we just want to make sure
    // everything is back to an idle state again

    for (int i = 0; i < IO_RX_BUFS; i++)
    {
        struct io_event ev;
        io_cancel(usb->io_ctx, &usb->io_bufs[IO_RX_BUF(i)].iocb, &ev);
    }

    for (int i = 0; i < IO_TX_BUFS; i++)
    {
        struct io_event ev;
        io_cancel(usb->io_ctx, &usb->io_bufs[IO_TX_BUF(i)].iocb, &ev);
    }
}

static void usb_send_packet(struct iap2_link *link, const uint8_t *data, size_t len)
{
    struct iap2_usb_listener *usb = iap2_ll_priv(link);

#if DB_DBG_USB > 1
    uint8_t *ptr = data;
    char buf[80];
    for (int i = 0; i < len && i < 80; i+=2,ptr++) {
        char hinibble = ((*ptr & 0xF0) >> 4);
        char lonibble = *ptr & 0x0F;
        hinibble = hinibble < 10 ? (hinibble + 0x30) : ((hinibble-10) + 0x41);
        lonibble = lonibble < 10 ? (lonibble + 0x30) : ((lonibble-10) + 0x41);
        buf[i] = hinibble;
        buf[i+1] = lonibble;
    }

    hsk_print(E_LEVEL_DEBUG, "[usb_send_packet] TX");
    hsk_print(E_LEVEL_DEBUG, "<%s>",buf);
    //hexdump(data, len);
    hsk_print(E_LEVEL_DEBUG, "----------");
#endif

    if ( len > MAX_PAYLOAD_SIZE )
    {
#if DB_DBG_USB > 0
        hsk_print(E_LEVEL_ERROR, "[usb_send_packet] USB TX data too long");
#endif
        return;
    }

    for (int i = 0; i < IO_TX_BUFS; i++)
    {
        struct usb_io_buf *buf = &usb->io_bufs[IO_TX_BUF(i)];

        pthread_mutex_lock(&buf->mutex);
        if ( buf->pending )
        {
#if DB_DBG_USB > 2
            hsk_print(E_LEVEL_WARNING, "[usb_send_packet] Buffer<%d> pending",i);
#endif
            pthread_mutex_unlock(&buf->mutex);
            continue;
        }

        memcpy(buf->buf, data, len);
        usb_submit_write(usb, buf, len);

        pthread_mutex_unlock(&buf->mutex);
        return;
    }

    // all TX buffers are busy, discard the message
#if DB_DBG_USB > 0
    hsk_print(E_LEVEL_ERROR, "[usb_send_packet] DROP PACKET");
#endif
}

static int usb_provide_identification(struct iap2_transport *transport, struct iap2_ctrl_param *param)
{
    struct iap2_usb_listener *usb = containerof(transport, struct iap2_usb_listener, transport);

    iap2_ctrl_param_group(param, 16, usb->identification,
                          sizeof(usb->identification) / sizeof(*usb->identification));

    return 0;
}

static void usb_cleanup_link(struct iap2_link *link)
{
    return;
}

static struct iap2_transport_ops usb_link_ops = {
                                                 .send_packet = usb_send_packet,
                                                 .cleanup = usb_cleanup_link,
};

static const char *ffsevent2str(uint8_t type)
{
    switch ( type )
    {
        case FUNCTIONFS_BIND:       return "FUNCTIONFS_BIND";
        case FUNCTIONFS_UNBIND:     return "FUNCTIONFS_UNBIND";
        case FUNCTIONFS_ENABLE:     return "FUNCTIONFS_ENABLE";
        case FUNCTIONFS_DISABLE:    return "FUNCTIONFS_DISABLE";
        case FUNCTIONFS_SETUP:      return "FUNCTIONFS_SETUP";
        case FUNCTIONFS_SUSPEND:    return "FUNCTIONFS_SUSPEND";
        case FUNCTIONFS_RESUME:     return "FUNCTIONFS_RESUME";
        default:                    return "unknown";
    }
}

static const char *epollevent2str(uint32_t type)
{
    switch ( type )
    {
        case EPOLLIN:       return "EPOLLIN";
        case EPOLLPRI:      return "EPOLLPRI";
        case EPOLLOUT:      return "EPOLLOUT";
        case EPOLLRDNORM:   return "EPOLLRDNORM";
        case EPOLLRDBAND:   return "EPOLLRDBAND";
        case EPOLLWRNORM:   return "EPOLLWRNORM";
        case EPOLLWRBAND:   return "EPOLLWRBAND";
        case EPOLLMSG:      return "EPOLLMSG";
        case EPOLLERR:      return "EPOLLERR";
        case EPOLLHUP:      return "EPOLLHUP";
        case EPOLLRDHUP:    return "EPOLLRDHUP";
        case EPOLLONESHOT:  return "EPOLLONESHOT";
        case EPOLLET:       return "EPOLLET";
        default:    return "UNKNOWN";
    }
}

static void usb_ep0_cb(struct iap2_epoll *epoll_ctx, uint32_t events)
{
    struct iap2_usb_listener *usb = (struct iap2_usb_listener*)epoll_ctx->arg;
    struct usb_functionfs_event event;
    ssize_t nbytes;
    char ep_path[PATH_MAX];
#if DB_DBG_USB > 0
    hsk_print(E_LEVEL_INFO, "[usb_ep0_cb] got event >> %s", epollevent2str(events));
#endif

    if ( events & EPOLLIN )
    {
        nbytes = read(epoll_ctx->fd, &event, sizeof(event));
        if ( nbytes < 0 || (size_t)nbytes != sizeof(event) )
        {
#if DB_DBG_USB > 0
            hsk_print(E_LEVEL_ERROR, "[usb_ep0_cb] unable to read event from ep0");
#endif
            return;
        }
#if DB_DBG_USB > 0
        hsk_print(E_LEVEL_DEBUG, "[usb_ep0_cb] %s", ffsevent2str(event.type));
#endif

        switch ( event.type )
        {
            case FUNCTIONFS_SETUP:
#if DB_DBG_USB > 0
                hsk_print(E_LEVEL_INFO, "[usb_ep0_cb] setup");
#endif

                if ( event.u.setup.bRequestType & USB_DIR_IN )
                    write(epoll_ctx->fd, NULL, 0);
                else
                    read(epoll_ctx->fd, NULL, 0);
                break;

            case FUNCTIONFS_ENABLE:
#if DB_DBG_USB > 0
                hsk_print(E_LEVEL_INFO, "[usb_ep0_cb] enable");
#endif

                for (int i = 1; i < 3; ++i)
                {
                    snprintf(ep_path, sizeof(ep_path), "%s/ep%d", usb->ffs_dir, i);
                    usb->ep[i] = open(ep_path, O_RDWR);
                    if ( usb->ep[i] < 0 )
                    {
#if DB_DBG_USB > 0
                        hsk_print(E_LEVEL_ERROR, "[usb_ep0_cb] unable to open ep%d: %s\n", i,
                             strerror(errno));
#endif
                        goto err_eps;
                    }
                }

                usb->link = iap2_link_new(usb->acc, &usb_link_ops, &usb->transport, usb, true);

                for (int i = 0; i < IO_RX_BUFS; i++)
                {
                    struct usb_io_buf *buf = &usb->io_bufs[IO_RX_BUF(i)];

                    pthread_mutex_lock(&buf->mutex);
                    usb_submit_read(usb, buf);
                    pthread_mutex_unlock(&buf->mutex);
                }

//                hsk_print(E_LEVEL_INFO, "[usb_ep0_cb] send testdata on bulkOut");
//
//                for (int i = 0; i < 10; i++) {
//                    char tmp[] = "dasisteintest!!!";
//                    usb_send_packet(usb->link, tmp, 16);
//                }

                if ( iap2_link_start(usb->link) < 0 )
                {
#if DB_DBG_USB > 0
                    hsk_print(E_LEVEL_ERROR, "[usb_ep0_cb] starting link failed");
#endif

                    ref_down(&usb->link->ref);
                    usb->link = NULL;
                }

                break;

            case FUNCTIONFS_SUSPEND:
#if DB_DBG_USB > 0
                hsk_print(E_LEVEL_INFO, "[usb_ep0_cb] suspend");
#endif

                case FUNCTIONFS_DISABLE:
                if ( usb->link )
                {
                    cancel_transfers(usb);

                    iap2_link_shutdown(usb->link);
                    ref_down(&usb->link->ref);
                    usb->link = NULL;
                }

                for (int i = 1; i < 3; i++)
                {
                    if ( usb->ep[i] >= 0 )
                    {
                        close(usb->ep[i]);
                        usb->ep[i] = -1;
                    }
                }

                break;

            default:
#if DB_DBG_USB > 0
                hsk_print(E_LEVEL_INFO, "[usb_ep0_cb] do nothing special");
#endif

                break;
        }
    }

    return;

    err_eps:
    for (int i = 1; i < 3; i++)
    {
        if ( usb->ep[i] >= 0 )
        {
            close(usb->ep[i]);
            usb->ep[i] = -1;
        }
    }
}

static void usb_eventfd_cb(struct iap2_epoll *epoll_ctx, uint32_t events)
{
    struct iap2_usb_listener *usb = (struct iap2_usb_listener*)epoll_ctx->arg;
#if DB_DBG_USB > 0
    hsk_print(E_LEVEL_INFO, "[usb_eventfd_cb] got event >> %s", epollevent2str(events));
#endif
    int ret;
    ssize_t nbytes;

    uint64_t evfd_events;
    nbytes = read(epoll_ctx->fd, &evfd_events, sizeof(evfd_events));
    if ( nbytes < 0 || (size_t)nbytes != sizeof(evfd_events) )
    {
#if DB_DBG_USB > 0
        hsk_print(E_LEVEL_ERROR, "[usb_eventfd_cb] reading from eventfd failed");
#endif
        return;
    }

    struct io_event ioe[IO_BUFS];
    ret = io_getevents(usb->io_ctx, 1, IO_BUFS, ioe, NULL);
    if ( ret < 0 )
    {
#if DB_DBG_USB > 0
        hsk_print(E_LEVEL_ERROR, "[usb_eventfd_cb] io_getevents");
#endif
        return;
    }
#if DB_DBG_USB > 2
    hsk_print(E_LEVEL_INFO, "[usb_eventfd_cb] amount=%d", ret);
#endif

    for (int i = 0; i < ret; i++)
    {
        struct usb_io_buf *iobuf = (struct usb_io_buf*)(uintptr_t)ioe[i].obj;

        pthread_mutex_lock(&iobuf->mutex);

        iobuf->pending = false;

        int res = (int64_t)ioe[i].res;
        int res2 = (int64_t)ioe[i].res2;

        if ( res < 0 )
        {
#if DB_DBG_USB > 2
            hsk_print(E_LEVEL_ERROR, "[usb_eventfd_cb]<e%d> io on fd %d failed, res=%ld res2=%ld(%s)", i, iobuf->iocb.aio_fildes, res, res2, strerror(res*-1));
#endif

            pthread_mutex_unlock(&iobuf->mutex);
            continue;
        }

        int ioFiles = (int)iobuf->iocb.aio_fildes;
#if DB_DBG_USB > 2
        hsk_print(E_LEVEL_INFO, "[usb_eventfd_cb] io files(%d) RX(%d) TX(%d)", ioFiles, usb->ep[USB_EP_RECV], usb->ep[USB_EP_SEND]);
#endif

#if DB_DBG_USB > 1
        uint8_t *ptr = iobuf->buf;
        char buf[80];

        for (int i = 0; ptr && i < 80; i+=2,ptr++) {
            char hinibble = ((*ptr & 0xF0) >> 4);
            char lonibble = *ptr & 0x0F;
            hinibble = hinibble < 10 ? (hinibble + 0x30) : ((hinibble-10) + 0x41);
            lonibble = lonibble < 10 ? (lonibble + 0x30) : ((lonibble-10) + 0x41);
            buf[i] = hinibble;
            buf[i+1] = lonibble;
        }
#endif

        if ( ioFiles == usb->ep[USB_EP_RECV] )
        {
#if DB_DBG_USB > 1
            hsk_print(E_LEVEL_DEBUG, "[usb_eventfd_cb] RX");
            hsk_print(E_LEVEL_DEBUG, "<%s>",buf);
//            hexdump(iobuf->buf, ioe[i].res);
            hsk_print(E_LEVEL_DEBUG, "----------");
#endif

            iap2_ll_submit_data(usb->link, iobuf->buf, ioe[i].res);
            usb_submit_read(usb, iobuf);
        }
#if DB_DBG_USB > 1
        else if ( ioFiles == usb->ep[USB_EP_SEND] )
        {
            hsk_print(E_LEVEL_DEBUG, "[usb_eventfd_cb] TX");
            hsk_print(E_LEVEL_DEBUG, "<%s>",buf);
            hsk_print(E_LEVEL_DEBUG, "----------");
        } else {
            hsk_print(E_LEVEL_INFO, "[usb_eventfd_cb] other origin");
        }
#endif

        pthread_mutex_unlock(&iobuf->mutex);
    }
}

struct iap2_usb_listener *iap2_usb_listen(struct iap2_accessory *acc, const char *ffs_dir)
{
    struct iap2_usb_listener *listener;
    int _;

    if ( strlen(ffs_dir) + strlen("/epX") + 1 > PATH_MAX )
    {
#if DB_DBG_USB > 2
        hsk_print(E_LEVEL_ERROR, "ffs_dir too long");
#endif
        return NULL;
    }
#if DB_DBG_USB > 0
    hsk_print(E_LEVEL_INFO, "[iap2_usb_listen] creating usb listener");
#endif

    listener = calloc(sizeof(*listener), 1);
    listener->acc = acc;
    listener->ffs_dir = strdup(ffs_dir);

    listener->transport.type = IAP2_TRANSPORT_TYPE_USB;
    listener->transport.provide_identification = usb_provide_identification;

    listener->eventfd = eventfd(0, EFD_CLOEXEC);
    if ( listener->eventfd < 0 )
    {
#if DB_DBG_USB > 0
        hsk_print(E_LEVEL_ERROR, "unable to open eventfd");
#endif
        goto err_listener;
    }

    if ( iap2_register_fd(acc->epfd, listener->eventfd, EPOLLIN, usb_eventfd_cb, &listener->eventfd_ctx, listener) < 0 )
    {
#if DB_DBG_USB > 0
        hsk_print(E_LEVEL_ERROR, "registering eventfd failed");
#endif
        goto err_eventfd;
    }
#if DB_DBG_USB > 0
    hsk_print(E_LEVEL_INFO, "[iap2_usb_listen] init endpoints");
#endif
    // initialize the FDs to a guard value we can use when bulk-closing
    // them in the error path below
    for (int i = 0; i < 3; i++)
        listener->ep[i] = -1;

    char ep_path[PATH_MAX];
    snprintf(ep_path, sizeof(ep_path), "%s/ep0", ffs_dir);
    listener->ep[0] = open(ep_path, O_RDWR);
    if ( listener->ep[0] < 0 )
    {
#if DB_DBG_USB > 0
        hsk_print(E_LEVEL_ERROR, "unable to open %s: %s", ep_path, strerror(errno));
#endif
        goto err_eventfd_reg;
    }

    if ( write(listener->ep[0], &descriptors, sizeof(descriptors)) < 0 )
    {
#if DB_DBG_USB > 0
        hsk_print(E_LEVEL_ERROR, "unable do write descriptors");
#endif
        goto err_eps;
    }

    if ( write(listener->ep[0], &strings, sizeof(strings)) < 0 )
    {
#if DB_DBG_USB > 0
        hsk_print(E_LEVEL_ERROR, "unable to write strings");
#endif
        goto err_eps;
    }

    if ( iap2_register_fd(acc->epfd, listener->ep[0], EPOLLIN, usb_ep0_cb, &listener->ep0_ctx, listener) < 0 )
    {
#if DB_DBG_USB > 0
        hsk_print(E_LEVEL_ERROR, "registering ep0 fd failed");
#endif
        goto err_eps;
    }

    if ( io_setup(IO_BUFS, &listener->io_ctx) < 0 )
    {
#if DB_DBG_USB > 0
        hsk_print(E_LEVEL_ERROR, "creating aio context failed");
#endif
        goto err_ep0;
    }

    for (int i = 0; i < IO_BUFS; i++)
    {
        _ = pthread_mutex_init(&listener->io_bufs[i].mutex, NULL);
        if ( _ < 0 )
        {
#if DB_DBG_USB > 0
            hsk_print(E_LEVEL_ERROR, "initializing buffer mutex failed: %s", strerror(_));
#endif

            for (int j = 0; j < i; j++)
                pthread_mutex_destroy(&listener->io_bufs[i].mutex);

            goto err_io_ctx;
        }
    }

    iap2_register_transport(acc, &listener->transport);
    iap2_ctrl_param_int(&listener->identification[0], 0, 2, listener->transport.id);
    iap2_ctrl_param_string(&listener->identification[1], 1, "usb");
    iap2_ctrl_param_none(&listener->identification[2], 2);

    return listener;

    err_io_ctx:
    io_destroy(listener->io_ctx);
    err_ep0:
    iap2_unregister_fd(acc->epfd, listener->ep[0]);
    err_eps:
    for (int i = 0; i < 3; i++)
        if ( listener->ep[i] >= 0 )
        {
            close(listener->ep[i]);
            listener->ep[i] = -1;
        }
    err_eventfd_reg:
    iap2_unregister_fd(acc->epfd, listener->eventfd);
    err_eventfd:
    close(listener->eventfd);
    err_listener:
    free(listener);

    return NULL;
}

void iap2_usb_close(struct iap2_usb_listener *listener)
{
    cancel_transfers(listener);
    io_destroy(listener->io_ctx);

    for (int i = 0; i < IO_BUFS; i++)
        pthread_mutex_destroy(&listener->io_bufs[i].mutex);

    for (int i = 0; i < 3; ++i)
    {
        iap2_unregister_fd(listener->acc->epfd, listener->ep[i]);
        close(listener->ep[i]);
    }

    iap2_unregister_fd(listener->acc->epfd, listener->eventfd);
    close(listener->eventfd);

    free(listener->ffs_dir);
    free(listener);
}
