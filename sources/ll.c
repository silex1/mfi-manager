#include <stdio.h>
#include <stdint.h>

#include <leica/leica_types.h>
#include <leica/hsk_debug_linux_if.h>

#include "iap2.h"
#include "iap2-priv.h"

void iap2_ll_submit_data(struct iap2_link *link, const uint8_t *data, size_t len)
{
    const uint8_t *p = data;

    if ( iap2_link_get_state(link) == IAP2_LINK_DEAD )
        return;

    hsk_print(E_LEVEL_INFO, "[iap2_ll_submit_data] send data");


    pthread_mutex_lock(&link->iap2_mutex);
    while ( len )
    {
        if ( !link->current_packet )
            link->current_packet = iAP2PacketCreateEmptyRecvPacket(link->run_loop->link);

        BOOL bDetect;
        uint32_t n_parsed = iAP2PacketParseBuffer(p, len, link->current_packet, 0, &bDetect, NULL, NULL);

        if ( bDetect || iAP2PacketIsComplete(link->current_packet) )
        {
            iAP2LinkRunLoopRunOnce(link->run_loop, link->current_packet);

            // iAP2LinkRunLoopRunOnce takes care of returning the packet to the pool
            link->current_packet = NULL;
        }

        len -= n_parsed;
        p += n_parsed;
    }
    pthread_mutex_unlock(&link->iap2_mutex);
}

void iap2_ll_signal_connected(struct iap2_link *link, bool connected)
{
    if ( iap2_link_get_state(link) == IAP2_LINK_DEAD )
        return;
    hsk_print(E_LEVEL_INFO, "[iap2_ll_signal_connected] connected");

    pthread_mutex_lock(&link->iap2_mutex);

    if ( connected )
        iAP2LinkRunLoopAttached(link->run_loop);
    else
        iAP2LinkRunLoopDetached(link->run_loop);

    pthread_mutex_unlock(&link->iap2_mutex);
}
