/**
*       @ingroup    con_mfi_grp
*       @brief      MFI Manager
*       @details     This file cointains the main entry functions for the MFI Manager.
*
*       @file        main.cpp
*       @date        11.03.2020
*       @author      J. Walter
*       @version     0.1
*
*       @b Copyright:
*          Leica Camera AG (Wetzlar)
*
*       @b Change history:
*
*     - 11.03.2020   J. Walter       File created.
**/


/**************************************************************************/
//INCLUDES
/**************************************************************************/

#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>

#include <leica/leica_core_if.h>
#include <leica/hsk_debug_linux_if.h>
#include <leica/linux_message_queue.h>
#include <leica/ludwig_messaging_if_sys.h>

#include "MfiManager.h"

void register_signals(void);
void SignalHandler(int _sig);

int main(int argc, char** argv)
{
    if ( (argc > 1) && ((strcmp(argv[1], "-v") == 0) || (strcmp(argv[1], "--version") == 0)) )
    {
#if defined(D_GIT_CA7LINUX_BRANCH) && defined(D_GIT_CA7LINUX_COMMIT)
        printf("%s: %s %s\n", argv[0], D_GIT_CA7LINUX_BRANCH, D_GIT_CA7LINUX_COMMIT);
#else
        printf("%s: No version available. Haendischer Zaehler (%d)(%d)\n", argv[0], 20201202, 1);
#endif
        exit(0);
    }

    char strModule[] = {"MFI"};
    bool bResult = hsk_debug_RegisterPrinter(strModule, E_LIN_DEBUG_PRINTER_TYPE_SYSLOG, LC_TRUE, NULL);
    if ( bResult == false )
    {
        perror("hsk_debug_RegisterPrinter(E_LIN_DEBUG_PRINTER_TYPE_SYSLOG) failed\n");
    }
    hsk_debug_SetDebugLevel(E_LEVEL_INFO);

    bResult = hsk_debug_RegisterPrinter(strModule, E_LIN_DEBUG_PRINTER_TYPE_STDOUT, LC_TRUE, NULL);
    if ( bResult == false )
    {
        perror("hsk_debug_RegisterPrinter(E_LIN_DEBUG_PRINTER_TYPE_STDOUT) failed\n");
    }
    hsk_debug_SetDebugLevel(E_LEVEL_INFO);

    try
    {
        hsk_print(E_LEVEL_INFO, "%s: argc(%d)", argv[0], argc);
        hsk_print(E_LEVEL_WARNING, "MFI manager starting...");

        register_signals();

        MfiManager *d = new MfiManager();

        d->initialise();
        d->run();

        delete d;
    }
    catch ( std::exception& e )
    {
        /* If the daemon fails to initialize, ignore the error but
         * make sure to propagate the message and return with an
         * error return code.
         */
        hsk_print(E_LEVEL_ERROR, "Could not start the MFI manager: (%s)", e.what());
    }

    hsk_print(E_LEVEL_INFO, "%s: Exit process pid(%d)", argv[0], getpid());
    exit(0);
}

void register_signals()
{
    signal(SIGCHLD, SIG_IGN); /* ignore child */
    signal(SIGTSTP, SIG_IGN); /* ignore tty signals */
    signal(SIGTTOU, SIG_IGN);
    signal(SIGTTIN, SIG_IGN);
    signal(SIGINT, SignalHandler);
    signal(SIGKILL, SignalHandler);
    signal(SIGHUP, SignalHandler);
    signal(SIGTERM, SignalHandler);
}

void SignalHandler(int _sig)
{
    hsk_print(E_LEVEL_INFO, "<%s> Got signal(%s)", __func__, strsignal(_sig));
    MfiManager::SignalHandler(_sig);
    exit(1);
}
