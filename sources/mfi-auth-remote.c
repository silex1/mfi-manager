#include <stdlib.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <errno.h>

#include <leica/leica_core_if.h>
#include <leica/hsk_debug_linux_if.h>

#include "mfi-auth.h"

static ssize_t recv_sync(int sockfd, void *buf, size_t len, int flags)
{
    size_t ret = 0;

    while ( len )
    {
        ssize_t rc = recv(sockfd, buf, len, flags);
        if ( rc == -1 )
        {
            if ( errno == EAGAIN || errno == EWOULDBLOCK )
            {
                continue;
            }
            return rc;
        }
        if ( rc == 0 )
            return rc;

        buf = (uint8_t *)buf + rc;
        len -= rc;
        ret += rc;
    }

    return (ssize_t)ret;
}

static ssize_t mfi_do_transfer(int sock, uint32_t cmdid, const void *out, uint32_t outlen, void *in, size_t maxinlen)
{
    struct T_MFI_CMD_HDR hdr;
    ssize_t rc = -1;

    hdr.cmdid = cmdid;
    hdr.len = outlen;

    ssize_t nbytes;
    nbytes = send(sock, &hdr, sizeof(hdr), 0);
    if ( nbytes != sizeof(hdr) )
    {
        hsk_print(E_LEVEL_ERROR, "can't send hdr");
        goto out_close;
    }

    if ( outlen )
    {
        nbytes = send(sock, out, outlen, 0);
        if ( nbytes != (ssize_t)outlen )
        {
            hsk_print(E_LEVEL_ERROR, "can't send data");
            goto out_close;
        }
    }

    uint32_t rsplen = 0;
    nbytes = recv_sync(sock, &rsplen, sizeof(rsplen), 0);
    if ( nbytes != sizeof(rsplen) )
    {
        hsk_print(E_LEVEL_ERROR, "can't recv rsplen");
        goto out_close;
    }

    if ( rsplen > maxinlen )
    {
        hsk_print(E_LEVEL_ERROR, "rsplen is too big rsplen=%08x maxlen=%08zx", rsplen, maxinlen);
        goto out_close;
    }

    nbytes = recv_sync(sock, in, rsplen, 0);
    if ( nbytes != (ssize_t)rsplen )
    {
        hsk_print(E_LEVEL_ERROR, "can't recv resp");
        goto out_close;
    }

    rc = rsplen;

out_close:
    close(sock);
    return rc;
}

int mfi_auth_open_remote(const char *peer)
{
    char addr_buf[16];
    const char *addr;
    uint16_t port = 8888;

    char *p = strchr(peer, ':');
    if ( p )
    {
        if ( (size_t)(p - peer) > sizeof(addr_buf) - 1 )
        {
            hsk_print(E_LEVEL_ERROR, "address too long in '%s'", peer);
            return -1;
        }

        memcpy(addr_buf, peer, p - peer);
        addr_buf[p - peer] = 0;
        addr = addr_buf;

        port = atoi(p + 1);
    }
    else
    {
        addr = peer;
    }

    struct sockaddr_in sa;
    memset(&sa, 0, sizeof(sa));

    sa.sin_family = AF_INET;
    sa.sin_port = htons(port);

    if ( inet_pton(AF_INET, addr, &sa.sin_addr) != 1 )
    {
        hsk_print(E_LEVEL_ERROR, "parsing address '%s' failed", addr);
        return -1;
    }

    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if ( sock < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "could not create socket");
        return -1;
    }

    if ( connect(sock, (struct sockaddr*)&sa, sizeof(sa)) < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "Could not connect to server %s:%d: %s", addr, port, strerror(errno));
        return -1;
    }

    return sock;
}

ssize_t mfi_get_cert_remote(int fd, uint8_t *cert)
{
    return mfi_do_transfer(fd, E_MFI_CMD_ID_CERT, NULL, 0, cert, MFI_MAX_CERT_SIZE);
}

int mfi_auth_remote(int fd, const uint8_t *challenge, uint8_t *response)
{
    ssize_t nbytes = mfi_do_transfer(fd, E_MFI_CMD_ID_AUTH, challenge, MFI_CHALLENGE_SIZE, response, MFI_RESPONSE_SIZE);
    if ( nbytes <= 0 )
    {
        return -1;
    }

    return 0;
}
