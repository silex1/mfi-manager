#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <limits.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>

#include <leica/leica_types.h>
#include <leica/hsk_debug_linux_if.h>

#include "mfi-auth.h"

#define MFI_ADDRESS 0x10

enum auth_type
{
    AUTH_LOCAL_I2C,
    AUTH_REMOTE
};

struct auth_handle
{
    enum auth_type type;
    int fd;
};

struct auth_handle *mfi_auth_open(const char *path)
{
    struct auth_handle *handle = NULL;

    handle = calloc(1, sizeof(*handle));
    if ( handle == NULL )
        goto err;

    if ( !strncasecmp(path, "tcp:", 4) )
    {
        handle->fd = mfi_auth_open_remote(path + 4);
        if ( handle->fd < 0 )
            goto err;

        handle->type = AUTH_REMOTE;

        return handle;
    }

    handle->type = AUTH_LOCAL_I2C;
    handle->fd = -1;

    if ( (handle->fd = open(path, O_RDWR)) < 0 )
        goto err;

    unsigned long funcs;
    if ( (ioctl(handle->fd, I2C_FUNCS, &funcs)) < 0 )
        goto err;

    if ( !(funcs & I2C_FUNC_I2C) )
        goto err;

    if ( ioctl(handle->fd, I2C_SLAVE, MFI_ADDRESS) < 0 )
        goto err;

    return handle;

err:
    if ( handle->fd >= 0 )
        close(handle->fd);

    free(handle);

    return NULL;
}

void mfi_auth_close(struct auth_handle *auth)
{
    close(auth->fd);
    free(auth);
}

#define MFI_RETRIES 100

// times are in microseconds
#define MFI_RETRY_SLEEP 500
#define MFI_T_CERT ( 10 * 1000)
#define MFI_T_AUTH (500 * 1000)

enum mfi_reg
{
    MFI_REG_CTRL = 0x10,
    MFI_REG_RESPONSE = 0x12,
    MFI_REG_CHALLENGE = 0x21,
    MFI_REG_CERT_LEN = 0x30,
    MFI_REG_CERT0 = 0x31,
    MFI_REG_CERT5 = 0x35,
    MFI_REG_SERIAL = 0x4e
};

/* This is a kludge to split i2c reads into chunks of I2C_MAX_READ bytes, as
 * some adapters cannot handle arbitrarily sized transactions.  64 bytes should
 * be safe enough for now. We might want to make this dynamically configurable
 * in the future.
 */
#define I2C_MAX_READ 64

static int mfi_read(int fd,
                    enum mfi_reg reg,
                    uint8_t *rbuf, size_t rlen,
                    unsigned long delay)
{
    int retries = MFI_RETRIES;
    uint8_t reg8 = reg;
    int ret;

    // the v3 chip requires a stop after selecting the register, so we need two transactions here

    restart:
    ret = write(fd, &reg8, 1);
    if ( ret != 1 )
    {
        if ( retries-- )
        {
            usleep(MFI_RETRY_SLEEP);
            goto restart;
        }
        else
            return -1;
    }

    if ( delay )
        usleep(delay);

    restart2:
    while ( rlen )
    {
        size_t to_read = rlen;

        if ( to_read > I2C_MAX_READ )
            to_read = I2C_MAX_READ;

        ret = read(fd, rbuf, to_read);
        if ( ret < 0 || (size_t)ret != to_read )
        {
            if ( retries-- )
            {
                usleep(MFI_RETRY_SLEEP);
                goto restart2;
            }
            else
                return -1;
        }

        rbuf += ret;
        rlen -= ret;
    }

    return 0;
}

static ssize_t mfi_get_cert_local(int fd, uint8_t *cert)
{
    uint8_t buf[2];

    if ( mfi_read(fd, MFI_REG_CERT_LEN, buf, 2, MFI_T_CERT) < 0 )
        return -1;

    uint16_t len = buf[0] << 8 |
                   buf[1];

    uint8_t *p = cert;
    uint16_t remaining = len;
    enum mfi_reg reg = MFI_REG_CERT0;

    while ( remaining )
    {
        int to_read = 128;

        if ( reg > MFI_REG_CERT5 )
            return -1;

        if ( remaining < 128 )
            to_read = remaining;

        if ( mfi_read(fd, reg, p, to_read, MFI_T_CERT) < 0 )
            return -1;

        p += to_read;
        remaining -= to_read;

        reg++;
    }

    return len;
}

ssize_t mfi_get_cert(struct auth_handle *auth, uint8_t *cert)
{
    ssize_t retVal = -1;
    if ( auth->type == AUTH_LOCAL_I2C )
    {
        retVal = mfi_get_cert_local(auth->fd, cert);
    }
    else if ( auth->type == AUTH_REMOTE )
    {
        retVal = mfi_get_cert_remote(auth->fd, cert);
    }
    else
    {
        hsk_print(E_LEVEL_ERROR, "Invalid auth type (%d)", auth->type);
    }
    return retVal;
}

static int mfi_write(int fd, uint8_t *buf, size_t len)
{
    int retries = MFI_RETRIES;

    do
    {
        ssize_t ret = write(fd, buf, len);

        if ( ret < 0 )
            continue;

        if ( (size_t)ret == len )
            return 0;
    }
    while ( retries-- );

    return -1;
}

static int mfi_auth_local(int fd, const uint8_t *challenge, uint8_t *response)
{
    uint8_t buf[MFI_CHALLENGE_SIZE + 1];

    buf[0] = MFI_REG_CHALLENGE;
    memcpy(&buf[1], challenge, MFI_CHALLENGE_SIZE);
    hsk_print(E_LEVEL_INFO, "Write Reg[%d] Chellenge",MFI_REG_CHALLENGE);
    if ( mfi_write(fd, buf, MFI_CHALLENGE_SIZE + 1) < 0 )
        return -1;

    buf[0] = MFI_REG_CTRL;
    buf[1] = 1;
    hsk_print(E_LEVEL_INFO, "Write Reg[%d] Control",MFI_REG_CTRL);
    if ( mfi_write(fd, buf, 2) < 0 )
        return -1;

    usleep(MFI_T_AUTH);

    hsk_print(E_LEVEL_INFO, "Read Response");
    if ( mfi_read(fd, MFI_REG_RESPONSE, response, MFI_RESPONSE_SIZE, MFI_T_CERT) < 0 )
        return -1;

    return 0;
}

int mfi_auth(struct auth_handle *auth, const uint8_t *challenge, uint8_t *response)
{
    ssize_t retVal = -1;
    if ( auth->type == AUTH_LOCAL_I2C )
    {
        hsk_print(E_LEVEL_INFO, "Local I2C Auth");
        retVal = mfi_auth_local(auth->fd, challenge, response);
    }
    else if ( auth->type == AUTH_REMOTE )
    {
        hsk_print(E_LEVEL_INFO, "Remote Auth");
        retVal = mfi_auth_remote(auth->fd, challenge, response);
    }
    else
    {
        hsk_print(E_LEVEL_ERROR, "Invalid auth type (%d)", auth->type);
    }
    return retVal;
}

static int mfi_get_serial_local(int fd, char *out, size_t n)
{
    size_t len = (n < MFI_MAX_SERIAL_SIZE) ? n : MFI_MAX_SERIAL_SIZE;

    if ( mfi_read(fd, MFI_REG_SERIAL, (uint8_t*)out, len - 1, MFI_T_CERT) < 0 )
        return -1;

    out[len - 1] = 0;

    return 0;
}

int mfi_get_serial(struct auth_handle *auth, char *out, size_t n)
{
    int retVal = -1;
    if ( auth->type == AUTH_LOCAL_I2C )
    {
        retVal = mfi_get_serial_local(auth->fd, out, n);
    }
    else
    {
        hsk_print(E_LEVEL_ERROR, "Invalid auth type (%d)", auth->type);
    }
    return retVal;
}
