#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <stdarg.h>
#include <inttypes.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/timerfd.h>
#include <sys/eventfd.h>
#include <errno.h>
#include <assert.h>

#include <leica/hsk_debug_linux_if.h>

#include "iAP2LinkRunLoop.h"
#include "iAP2TimeImplementation.h"

#include "iap2-priv.h"


/*
 * loop stub implementations
 */

void iAP2LinkRunLoopInitImplementation(iAP2LinkRunLoop_t* linkRunLoop)
{
}

void iAP2LinkRunLoopCleanupImplementation(iAP2LinkRunLoop_t* linkRunLoop)
{
}

void iAP2LinkRunLoopSignal(iAP2LinkRunLoop_t* linkRunLoop, void* arg)
{
    struct iap2_link *link = linkRunLoop->context;
    uint64_t token = 1;

    write(link->event_fd, &token, sizeof(token));
}

BOOL iAP2LinkRunLoopWait(iAP2LinkRunLoop_t* linkRunLoop)
{
    return TRUE;
}

BOOL iAP2LinkRunLoopProtectedCall(iAP2LinkRunLoop_t* linkRunLoop,
                                  void* arg,
                                  BOOL (*func)(iAP2LinkRunLoop_t* linkRunLoop, void* arg))
{
    return func(linkRunLoop, arg);
}

void iAP2LinkRunLoopSetEventMaskBit(iAP2LinkRunLoop_t* linkRunLoop,
                                    iAP2LinkRunLoopEventMask_t bit)
{
    linkRunLoop->eventMask |= bit;
}

uint32_t iAP2LinkRunLoopGetResetEventMask(iAP2LinkRunLoop_t* linkRunLoop)
{
    uint32_t mask = linkRunLoop->eventMask;

    linkRunLoop->eventMask = kiAP2LinkRunLoopEventMaskNone;

    return mask;
}

static void event_epoll_cb(struct iap2_epoll *ctx, uint32_t events)
{
    struct iap2_link *link = (struct iap2_link*)ctx->arg;
    uint64_t count;

    read(ctx->fd, &count, sizeof(count));

    pthread_mutex_lock(&link->iap2_mutex);
    iAP2LinkRunLoopRunOnce(link->run_loop, NULL);
    pthread_mutex_unlock(&link->iap2_mutex);

    iap2_link_runloop_updated(link);
}

int iap2_loop_init(struct iap2_link *link)
{
    link->event_fd = eventfd(0, EFD_CLOEXEC);
    if ( link->event_fd < 0 )
        return -1;

    if ( iap2_register_fd(link->epfd, link->event_fd, EPOLLIN, event_epoll_cb, &link->event_ctx, link) < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "registering eventfd with epoll failed.");
        goto err;
    }

    return 0;

err:
    close(link->event_fd);
    return -1;
}

void iap2_loop_destroy(struct iap2_link *link)
{
    iap2_unregister_fd(link->epfd, link->event_fd);
    close(link->event_fd);
}

/*
 * timer stub implementations
 */

struct iap2_timer
{
    struct iap2_epoll epoll;

    int fd;
    iAP2Timer_t *timer;
    iAP2TimeCB_t cb;
};

static void timer_epoll_cb(struct iap2_epoll *ctx, uint32_t events)
{
    struct iap2_link *link = (struct iap2_link*)ctx->arg;
    uint64_t expns;

    if ( read(ctx->fd, &expns, sizeof(expns)) != sizeof(expns) )
    {
        // discard spurious wakeups
        if ( errno == EAGAIN || errno == EWOULDBLOCK )
            return;

        hsk_print(E_LEVEL_INFO, "timerfd read.");
    }
    else
    {
        if ( expns != 1 )
        {
            hsk_print(E_LEVEL_WARNING, "Timer expired more than once (%lld times) before handler got called.", expns);
        }

        if ( !link->timer->cb )
        {
            hsk_print(E_LEVEL_WARNING, "Timer fired but no callback registered.");
            return;
        }

        pthread_mutex_lock(&link->iap2_mutex);
        link->timer->cb(link->timer->timer, iAP2TimeGetCurTimeMsInt64());
        pthread_mutex_unlock(&link->iap2_mutex);
    }
}

int iap2_timer_init(struct iap2_link *link)
{
    struct iap2_timer *ctx = calloc(sizeof(*ctx), 1);
    if ( !ctx )
    {
        hsk_print(E_LEVEL_ERROR, "Creating timer context failed.");
        return -1;
    }

    ctx->fd = timerfd_create(CLOCK_MONOTONIC, TFD_CLOEXEC | TFD_NONBLOCK);
    if ( ctx->fd < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "timerfd_create failed.");
        goto err;
    }

    ctx->timer = NULL;
    ctx->cb = NULL;

    if ( iap2_register_fd(link->epfd, ctx->fd, EPOLLIN, timer_epoll_cb, &ctx->epoll, link) < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "Registering timerfd with epoll failed.");
        goto err_fd;
    }

    link->timer = ctx;

    return 0;

    err_fd:
    close(ctx->fd);
    err:
    free(ctx);
    return -1;
}

void iap2_timer_destroy(struct iap2_link *link)
{
    iap2_unregister_fd(link->epfd, link->timer->fd);

    close(link->timer->fd);
    free(link->timer);
}

BOOL _iAP2TimeCallbackAfter(iAP2Timer_t* timer,
                            uint32_t delayMs,
                            iAP2TimeCB_t callback)
{
    iAP2Link_t *raw_link = timer->link;
    iAP2LinkRunLoop_t *run_loop = raw_link->context;
    struct iap2_link *link = run_loop->context;

    struct itimerspec its;
    its.it_interval.tv_sec = its.it_interval.tv_nsec = 0;
    its.it_value.tv_sec = delayMs / 1000;
    its.it_value.tv_nsec = (delayMs % 1000) * 1000ull * 1000ull;

    if ( timerfd_settime(link->timer->fd, 0, &its, NULL) < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "timerfd_settime.");
        return FALSE;
    }

    link->timer->timer = timer;
    link->timer->cb = callback;

    return TRUE;
}

void _iAP2TimeCancelCallback(iAP2Timer_t* timer)
{
    iAP2Link_t *raw_link = timer->link;
    iAP2LinkRunLoop_t *run_loop = raw_link->context;
    struct iap2_link *link = run_loop->context;

    if ( !link->timer->cb )
    {
        hsk_print(E_LEVEL_WARNING, "Tried to cancel noninitialized timer.");
        return;
    }

    struct itimerspec its;
    its.it_interval.tv_sec = its.it_interval.tv_nsec = 0;
    its.it_value.tv_sec = its.it_value.tv_nsec = 0;

    if ( timerfd_settime(link->timer->fd, 0, &its, NULL) < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "Timerfd_settime.");
        return;
    }

    link->timer->timer = NULL;
    link->timer->cb = NULL;
}

void _iAP2TimeCleanupCallback(iAP2Timer_t* timer)
{
    /* NOP, as the iAP2 library will already have cancelled the timer at
     * this point and the timerfd itself lives as long as the library link
     * context */
}
