/**
 *       @brief      MFI Testapp demo common
 *       @details     Common functions for test programs.
 *
 *       @file        demo-common.c
 *       @date        11.03.2020
 *       @author      J. Walter
 *       @version     0.1
 *
 *       @b Copyright:
 *          Leica Camera AG (Wetzlar)
 *
 *       @b Change history:
 *
 *     - 11.03.2020   J. Walter       File created.
 **/
#pragma once

/*check if the compiler is of C++*/
#ifdef __cplusplus
extern "C" {
#endif

struct iap2_accessory *create_demo(const char *auth_path, const char *cameradir, void (*new_link_cb)(struct iap2_link *link));
void common_new_link(struct iap2_link *link);
void common_new_link_init(struct iap2_link *link);


/*check if the compiler is of C++*/
#ifdef __cplusplus
}
#endif
