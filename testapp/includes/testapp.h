/**
 *       @brief      MFI Testapp
 *       @details     Used for MFI tests.
 *
 *       @file        testapp.h
 *       @date        11.03.2020
 *       @author      J. Walter
 *       @version     0.1
 *
 *       @b Copyright:
 *          Leica Camera AG (Wetzlar)
 *
 *       @b Change history:
 *
 *     - 11.03.2020   J. Walter       File created.
 **/

/**************************************************************************/
//INCLUDES
/**************************************************************************/

#pragma once

#include <stdint.h>
#include <sys/types.h>

/**************************************************************************/
//ENUMERATIONS
/**************************************************************************/
