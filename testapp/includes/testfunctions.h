/**
 *       @brief      MFI Test functions
 *       @details     Test functions used by testapp. It is the c-interface for the cpp-app.
 *
 *       @file        testfunctions.h
 *       @date        11.03.2020
 *       @author      J. Walter
 *       @version     0.1
 *
 *       @b Copyright:
 *          Leica Camera AG (Wetzlar)
 *
 *       @b Change history:
 *
 *     - 11.03.2020   J. Walter       File created.
 **/

/*check if the compiler is of C++*/
#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************/
//INCLUDES
/**************************************************************************/

#pragma once

/**************************************************************************/
//ENUMERATIONS
/**************************************************************************/

typedef enum
{
    E_TestCmd_Auth,
    E_TestCmd_MFI,
    E_TestCmd_MFI_Chip
} E_TestCmd;

typedef struct
{
    E_TestCmd test_command;
    int test_parameter;
    int test_challenge_len;
    // Test dev
    int port;
    const char *auth_dev;
    // Test Challenge
    const char *test_challenge;
    // Test mfi
    const char *bt_client_addr;
    const char *ffs_dir;
    const char *udc_path;
    const char *udc_name;
} T_CmdLineArgs;

int test_mfi(T_CmdLineArgs *_pCmdLineArgs);
int test_mfi_auth(T_CmdLineArgs *_pCmdLineArgs);
int test_mfi_chip_connection(T_CmdLineArgs *_pCmdLineArgs);

/*check if the compiler is of C++*/
#ifdef __cplusplus
}
#endif
