/**
 *       @brief      MFI Testapp
 *       @details     Used for MFI tests.
 *
 *       @file        testapp.cpp
 *       @date        11.03.2020
 *       @author      J. Walter
 *       @version     0.1
 *
 *       @b Copyright:
 *          Leica Camera AG (Wetzlar)
 *
 *       @b Change history:
 *
 *     - 11.03.2020   J. Walter       File created.
 **/

/**************************************************************************/
//INCLUDES
/**************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <errno.h>
#include <getopt.h>
#include <string.h>

#include <leica/leica_types.h>
#include <leica/leica_core_if.h>
#include <leica/hsk_debug_linux_if.h>
#include <leica/ludwig_messaging_if_sys.h>

#include "iap2.h"
#include "ll-bt-linux.h"
#include "ll-usb-ffs.h"

#include "testfunctions.h"
#include "testapp.h"

static int parseArgs(int argc, char **argv, T_CmdLineArgs *_pCmdLineArgs);
static void show_usage(const char *path);

static struct option long_options[] = {
                                       {"tcmd", required_argument, 0, 0},
                                       {"tpara", required_argument, 0, 0},
                                       {"tcha", required_argument, 0, 0},
                                       {"auth-dev", required_argument, 0, 0},
                                       {"port", required_argument, 0, 0},
                                       {"bt-client", required_argument, 0, 0},
                                       {"usb-ffs", required_argument, 0, 0},
                                       {"usb-udc-path", required_argument, 0, 0},
                                       {"usb-udc-name", required_argument, 0, 0},
                                       {"help", no_argument, 0, 0},
                                       {0, 0, 0, 0}
};

int main(int argc, char **argv)
{
#ifdef JW_TEST
    if ( (argc > 1) && ((strcmp(argv[1], "-v") == 0) || (strcmp(argv[1], "--version") == 0)) )
    {
#if defined(D_GIT_CA7LINUX_BRANCH) && defined(D_GIT_CA7LINUX_COMMIT)
        printf("%s: %s %s\n", argv[0], D_GIT_CA7LINUX_BRANCH, D_GIT_CA7LINUX_COMMIT);
#else
        printf("%s: No version available\n", argv[0]);
#endif
        exit(0);
    }
#endif

    int retVal = EXIT_FAILURE;
    T_CmdLineArgs cmdLineArgs;
    memset(&cmdLineArgs, 0x00, sizeof(T_CmdLineArgs));

    // ------------ Initialise debug printer -------------------
    char strModule[] = {"MFI-TEST"};
    bool bResult = hsk_debug_RegisterPrinter(strModule, E_LIN_DEBUG_PRINTER_TYPE_SYSLOG, LC_TRUE, NULL);
    if ( bResult == false )
    {
        perror("hsk_debug_RegisterPrinter(E_LIN_DEBUG_PRINTER_TYPE_SYSLOG) failed\n");
    }
    hsk_debug_SetDebugLevel(E_LEVEL_DEBUG);

    bResult = hsk_debug_RegisterPrinter(strModule, E_LIN_DEBUG_PRINTER_TYPE_STDOUT, LC_TRUE, NULL);
    if ( bResult == false )
    {
        perror("hsk_debug_RegisterPrinter(E_LIN_DEBUG_PRINTER_TYPE_STDOUT) failed\n");
    }
    hsk_debug_SetDebugLevel(E_LEVEL_DEBUG);

    // Parse command line arguments
    parseArgs(argc, argv, &cmdLineArgs);

    // Call according test function
    switch (cmdLineArgs.test_command)
    {
        case E_TestCmd_Auth: test_mfi_auth(&cmdLineArgs); break;
        case E_TestCmd_MFI: test_mfi(&cmdLineArgs); break;
        case E_TestCmd_MFI_Chip: test_mfi_chip_connection(&cmdLineArgs); break;
    }
    return retVal;
}

int parseArgs(int argc, char **argv, T_CmdLineArgs *_pCmdLineArgs)
{
    int retVal = 0;
    bool bNextArg = true;
    bool bHaveCmd = false;
    _pCmdLineArgs->test_parameter = -1;

    do
    {
        int option_index = 0;
        int c = getopt_long(argc, argv, "", long_options, &option_index);

        switch ( c )
        {
            case 0:
            {
                if ( long_options[option_index].flag != 0 )
                {
                    break;
                }

                const char *name = long_options[option_index].name;

                if ( !strcmp(name, "tcmd") )                     // Test command
                {
                    hsk_print(E_LEVEL_INFO, "We have test cmd option <%s>", optarg);

                    if ( !strcmp(optarg, "dev") )
                    {
                        _pCmdLineArgs->test_command = E_TestCmd_Auth;
                    }
                    else if ( !strcmp(optarg, "mfi") )
                    {
                        _pCmdLineArgs->test_command = E_TestCmd_MFI;
                    }
                    else
                    {
                        _pCmdLineArgs->test_command = E_TestCmd_MFI_Chip;
                    }
                    bHaveCmd = true;
                }
                else if ( !strcmp(name, "tpara") )               // Test mfi parameter
                {
                    _pCmdLineArgs->test_parameter = atoi(optarg);
                    bHaveCmd = true;
                }
                else if ( !strcmp(name, "tcha") )               // Test mfi parameter
                {
                    _pCmdLineArgs->test_challenge = optarg;
                    int argLen = strlen(_pCmdLineArgs->test_challenge);
                    _pCmdLineArgs->test_challenge_len = argLen;
                    bHaveCmd = true;
                }
                else if ( !strcmp(name, "auth-dev") )               // Test mfi parameter
                {
                    _pCmdLineArgs->auth_dev = optarg;
                    bHaveCmd = true;
                }
                else if ( !strcmp(name, "bt-client") )
                {
                    _pCmdLineArgs->bt_client_addr = optarg;
                    bHaveCmd = true;
                }
                else if ( !strcmp(name, "usb-ffs") )
                {
                    _pCmdLineArgs->ffs_dir = optarg;
                    bHaveCmd = true;
                }
                else if ( !strcmp(name, "usb-udc-path") )
                {
                    _pCmdLineArgs->udc_path = optarg;
                    bHaveCmd = true;
                }
                else if ( !strcmp(name, "usb-udc-name") )
                {
                    _pCmdLineArgs->udc_name = optarg;
                    bHaveCmd = true;
                }
                else if ( !strcmp(name, "port") )
                {
                    _pCmdLineArgs->port = atoi(optarg);
                    bHaveCmd = true;
                }
                else if ( !strcmp(name, "help") )
                {
                    show_usage(argv[0]);
                    retVal = -3;
                    bNextArg = false;
                }
                else
                {
                    hsk_print(E_LEVEL_ERROR, "Error parsing arguments.");
                }
                break;
            }
            case -1:
            {
                retVal = -2;
                bNextArg = false;
                if (!bHaveCmd) {
                    hsk_print(E_LEVEL_ERROR, "Error (-1) at parsing arguments.");
                }
                break;
            }
            case '?':
            default:
            {
                retVal = -1;
                bNextArg = false;
                hsk_print(E_LEVEL_ERROR, "Error parsing arguments.");
            }
        }
    }
    while ( bNextArg );

    if ( optind < argc )
    {
        hsk_print(E_LEVEL_WARNING, "unsupported arguments: ");
        while ( optind < argc )
        {
            hsk_print(E_LEVEL_WARNING, "\t%s", argv[optind++]);
        }
        retVal = -4;
    }

    return retVal;
}

void show_usage(const char *path)
{
    hsk_print(E_LEVEL_ERROR, "Usage: %s [OPTION]...", path);
    hsk_print(E_LEVEL_ERROR, "");
    hsk_print(E_LEVEL_ERROR, "--cmd             Select sub-command:");
    hsk_print(E_LEVEL_ERROR, "    dev           Connect specified to device");
    hsk_print(E_LEVEL_ERROR, "    mfi           Do whatever");
    hsk_print(E_LEVEL_ERROR, "");
    hsk_print(E_LEVEL_ERROR, "Parameter command dev:");
    hsk_print(E_LEVEL_ERROR, "--port            Server is listening on port");
    hsk_print(E_LEVEL_ERROR, "");
    hsk_print(E_LEVEL_ERROR, "Parameter command mfi:");
    hsk_print(E_LEVEL_ERROR, "--auth            path to i2c auth dev");
    hsk_print(E_LEVEL_ERROR, "--bt-client       iPhone MAC address, this enables the bluetooth client");
    hsk_print(E_LEVEL_ERROR, "--usb-ffs         path to usb ffs, this enables USB");
    hsk_print(E_LEVEL_ERROR, "--usb-udc-path    path to UDC selection file");
    hsk_print(E_LEVEL_ERROR, "--usb-udc-name    name of the UDC device to use");
    hsk_print(E_LEVEL_ERROR, "--help            print this help");
}
