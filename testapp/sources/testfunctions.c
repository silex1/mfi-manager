/**
 *       @brief      MFI Test functions
 *       @details     Test functions used by testapp. It is the c-interface for the cpp-app.
 *
 *       @file        testfunctions.c
 *       @date        11.03.2020
 *       @author      J. Walter
 *       @version     0.1
 *
 *       @b Copyright:
 *          Leica Camera AG (Wetzlar)
 *
 *       @b Change history:
 *
 *     - 11.03.2020   J. Walter       File created.
 **/

/**************************************************************************/
//INCLUDES
/**************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <errno.h>
#include <getopt.h>
#include <string.h>

#include <leica/leica_types.h>
#include <leica/leica_core_if.h>
#include <leica/hsk_debug_linux_if.h>
#include <leica/ludwig_messaging_if_sys.h>

#include "iap2.h"
#include "ll-bt-linux.h"
#include "ll-usb-ffs.h"

#include "demo-common.h"
#include "mfi-auth.h"
#include "testfunctions.h"


static void new_link(struct iap2_link *link);
static ssize_t recv_sync(int sockfd, void *buf, size_t len, int flags);

void new_link(struct iap2_link *link)
{
    common_new_link(link);
}

ssize_t recv_sync(int sockfd, void *buf, size_t len, int flags)
{
    size_t ret = 0;

    while ( len )
    {
        ssize_t rc = recv(sockfd, buf, len, flags);
        if ( rc == -1 )
        {
            if ( errno == EAGAIN || errno == EWOULDBLOCK )
            {
                continue;
            }
            return rc;
        }
        if ( rc == 0 )
            return rc;

        buf = (uint8_t *)buf + rc;
        len -= rc;
        ret += rc;
    }

    return (ssize_t)ret;
}

int test_mfi(T_CmdLineArgs *_pCmdLineArgs)
{
    int retVal = 0;
    struct iap2_accessory *acc;
    struct iap2_link *link;
    struct iap2_bt_listener *listener_bt;
    struct iap2_usb_listener *listener_usb;

    if (!_pCmdLineArgs->auth_dev) {
        hsk_print(E_LEVEL_ERROR, "auth-dev required");
        return EXIT_FAILURE;
    }

    if (!!_pCmdLineArgs->udc_name != !!_pCmdLineArgs->udc_path) {
        hsk_print(E_LEVEL_ERROR, "usb-udc-path and usb-udc-name have to be set in combination");
        return EXIT_FAILURE;
    }

    if (_pCmdLineArgs->udc_path && !_pCmdLineArgs->ffs_dir) {
        hsk_print(E_LEVEL_ERROR, "usb-udc-path and usb-udc-name only work when usb-ffs is set");
        return EXIT_FAILURE;
    }

    acc = create_demo(_pCmdLineArgs->auth_dev, ".", new_link);
    if (!acc)
        return EXIT_FAILURE;

    if (_pCmdLineArgs->bt_client_addr) {
        hsk_print(E_LEVEL_INFO,"starting bt listener");
        listener_bt = iap2_bt_listen(acc, 0);
        if (!listener_bt) {
            hsk_print(E_LEVEL_ERROR, "starting listener failed");
            return EXIT_FAILURE;
        }

        hsk_print(E_LEVEL_INFO,"starting bt client");
        struct iap2_bt_addr addr;
        if (iap2_bt_str2addr(_pCmdLineArgs->bt_client_addr, &addr)) {
            hsk_print(E_LEVEL_ERROR, "invalid bluetooth addr: %s", _pCmdLineArgs->bt_client_addr);
            return EXIT_FAILURE;
        }
        do {
            link = iap2_bt_connect(acc, &addr);
            if (!link) {
                hsk_print(E_LEVEL_WARNING, "bluetooth connection failed, retrying");
                sleep(1);
            }
        } while (!link);

        common_new_link(link);
        iap2_link_unref(link);

        retVal = iap2_bt_enter_pairing();
        if (retVal) {
            hsk_print(E_LEVEL_ERROR, "can't enter pairing mode");
            return EXIT_FAILURE;
        }
    }

    if (_pCmdLineArgs->ffs_dir) {
        hsk_print(E_LEVEL_INFO,"starting usb listener");

        listener_usb = iap2_usb_listen(acc, _pCmdLineArgs->ffs_dir);
        if (!listener_usb) {
            hsk_print(E_LEVEL_ERROR, "can't listen on USB transport");
            return EXIT_FAILURE;
        }

        hsk_print(E_LEVEL_INFO,"open udc device");
        FILE *f = fopen(_pCmdLineArgs->udc_path, "w");
        if (!f) {
            hsk_print(E_LEVEL_ERROR, "fopen on UDC path");
            iap2_usb_close(listener_usb);
            return EXIT_FAILURE;
        }

        fputs(_pCmdLineArgs->udc_name, f);
        fclose(f);
    }

    hsk_print(E_LEVEL_INFO,"run iap2");
    retVal = iap2_run(acc);

    iap2_unref(acc);

    return retVal;
}

int test_mfi_auth(T_CmdLineArgs *_pCmdLineArgs)
{
    int retVal = 0;
    int client_fd = 0;
    struct auth_handle *auth_handle = NULL;
    struct sockaddr_in server;

    if ( (_pCmdLineArgs->auth_dev == NULL) && (_pCmdLineArgs->port == 0) )
    {
        hsk_print(E_LEVEL_ERROR, "Missing arguments");
        return retVal;
    }

    int server_fd = socket(AF_INET, SOCK_STREAM, 0);
    if ( server_fd < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "can't not create socket");
        return retVal;
    }

    server.sin_family = AF_INET;
    server.sin_port = htons(_pCmdLineArgs->port);
    server.sin_addr.s_addr = htonl(INADDR_ANY);

    int opt_val = 1;
    setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &opt_val, sizeof(opt_val));

    int err = bind(server_fd, (struct sockaddr *)&server, sizeof(server));
    if ( err < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "Can't not bind socket");
        return retVal;
    }

    err = listen(server_fd, 128);
    if ( err < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "Can't not listen on socket");
        return retVal;
    }

    hsk_print(E_LEVEL_INFO,"Server is listening on port %d", _pCmdLineArgs->port);

    struct T_MFI_CMD_HDR hdr;
    struct sockaddr_in client;
    socklen_t client_len = sizeof(client);
    client_fd = accept(server_fd, (struct sockaddr *)&client, &client_len);

    if ( client_fd >= 0 )
    {
        hsk_print(E_LEVEL_INFO,"New client");

        auth_handle = mfi_auth_open(_pCmdLineArgs->auth_dev);
        if ( auth_handle != NULL )
        {
            ssize_t read = recv_sync(client_fd, &hdr, sizeof(hdr), 0);
            if ( read == sizeof(hdr) )
            {
                if ( hdr.cmdid == E_MFI_CMD_ID_AUTH )
                {
                    uint8_t *challenge = (uint8_t *)malloc(hdr.len);
                    if ( challenge != NULL )
                    {
                        read = recv_sync(client_fd, challenge, hdr.len, 0);
                        if ( read == hdr.len )
                        {
                            static uint8_t response[MFI_RESPONSE_SIZE];
                            ssize_t ret = mfi_auth(auth_handle, challenge, response);
                            if ( ret >= 0 )
                            {
                                uint32_t len = MFI_RESPONSE_SIZE;
                                err = send(client_fd, &len, sizeof(len), 0);
                                if ( err >= 0 )
                                {
                                    err = send(client_fd, response, len, 0);
                                    if ( err >= 0 )
                                    {
                                        hsk_print(E_LEVEL_INFO,"-- Reponse to client successfully --");
                                        retVal = EXIT_SUCCESS;
                                    }
                                    else
                                    {
                                        hsk_print(E_LEVEL_ERROR, "Can't send reponse to client");
                                    }
                                }
                                else
                                {
                                    hsk_print(E_LEVEL_ERROR, "Can't send response length to client");
                                }
                            }
                            else
                            {
                                hsk_print(E_LEVEL_ERROR, "Failed to get response to challenge");
                            }
                        }
                        else
                        {
                            hsk_print(E_LEVEL_ERROR, "Can't read challenge");
                        }
                    }
                    else
                    {
                        hsk_print(E_LEVEL_ERROR, "Malloc of (%d) bytes for challenge failed", hdr.len);
                    }
                }
                else if ( hdr.cmdid == E_MFI_CMD_ID_CERT )
                {
                    static uint8_t cert[MFI_MAX_CERT_SIZE];
                    int ret = mfi_get_cert(auth_handle, cert);
                    if ( ret >= 0 )
                    {
                        uint32_t len = ret;
                        err = send(client_fd, &len, sizeof(len), 0);
                        if ( err >= 0 )
                        {
                            err = send(client_fd, cert, len, 0);
                            if ( err >= 0 )
                            {
                                hsk_print(E_LEVEL_INFO,"-- Cert sent to client successfully --");
                            }
                            else
                            {
                                hsk_print(E_LEVEL_ERROR, "Can't send cert to client");
                            }
                        }
                        else
                        {
                            hsk_print(E_LEVEL_ERROR, "Can't send length to client");
                        }
                    }
                    else
                    {
                        hsk_print(E_LEVEL_ERROR, "Failed to get mfi cert");
                    }
                }
                else
                {
                    hsk_print(E_LEVEL_ERROR, "Invalid cmdid: 0x%08x", hdr.cmdid);
                }
            }
            else
            {
                hsk_print(E_LEVEL_ERROR, "Can't read hdr");
            }

            mfi_auth_close(auth_handle);
        }
        else
        {
            hsk_print(E_LEVEL_ERROR, "Failed to open mfi device");
        }

        close(client_fd);
    }
    else
    {
        hsk_print(E_LEVEL_ERROR, "Can't establish new connection");
    }

    return retVal;
}

int test_mfi_chip_connection(T_CmdLineArgs *_pCmdLineArgs)
{
    int retVal = 0;
    struct auth_handle *auth_handle = NULL;
    struct T_MFI_CMD_HDR hdr;

    if ( (_pCmdLineArgs->auth_dev == NULL) )
    {
        hsk_print(E_LEVEL_ERROR, "missing arguments");
        return retVal;
    }
    if ( _pCmdLineArgs->test_parameter < 0 )
    {
        hsk_print(E_LEVEL_ERROR, "ubknown parameter");
        return retVal;
    }
    if ( _pCmdLineArgs->test_challenge_len <= 0 ^ _pCmdLineArgs->test_challenge_len > 32 )
    {
        hsk_print(E_LEVEL_ERROR, "wrong challenge size");
        return retVal;
    }

    auth_handle = mfi_auth_open(_pCmdLineArgs->auth_dev);
    if ( auth_handle != NULL )
    {
        hsk_print(E_LEVEL_INFO, "MFI Chip open handle...");
        hdr.cmdid = _pCmdLineArgs->test_parameter;
        hdr.len = _pCmdLineArgs->test_challenge_len;

        int retVal;
        char serial[MFI_MAX_SERIAL_SIZE];

        retVal = mfi_get_serial(auth_handle, serial, MFI_MAX_SERIAL_SIZE);

        if ( retVal == 0 ) {
            hsk_print(E_LEVEL_INFO, "MFI Chip serial:[%s]",serial);

            if ( hdr.cmdid == E_MFI_CMD_ID_AUTH )
            {
                hsk_print(E_LEVEL_INFO, "CMD-Auth Challenge/Response");

                uint8_t *challenge = (uint8_t *)malloc(hdr.len);
                if ( challenge != NULL )
                {
                    hsk_print(E_LEVEL_INFO, "Setup Challange");
                    memcpy(challenge, _pCmdLineArgs->test_challenge, _pCmdLineArgs->test_challenge_len);

                    static uint8_t response[MFI_RESPONSE_SIZE];
                    ssize_t ret = mfi_auth(auth_handle, challenge, response);
                    if ( ret >= 0 )
                    {
                        hsk_print(E_LEVEL_INFO, "Collect respoinse from Chip");
                        hsk_print(E_LEVEL_INFO, "Response_len: %d", ret);
                    }
                    else
                    {
                        hsk_print(E_LEVEL_ERROR, "Failed to get response to challenge");
                    }
                }
                else
                {
                    hsk_print(E_LEVEL_ERROR, "Malloc of (%d) bytes for challenge failed", hdr.len);
                }
            }
            else if ( hdr.cmdid == E_MFI_CMD_ID_CERT )
            {
                hsk_print(E_LEVEL_INFO, "CMD-Cert");

                static uint8_t cert[MFI_MAX_CERT_SIZE];
                int ret = mfi_get_cert(auth_handle, cert);
                if ( ret >= 0 )
                {
                    hsk_print(E_LEVEL_INFO,"-- Cert for to client successfully --");
                    hsk_print(E_LEVEL_INFO,"----BEGIN CERTIFICATE----");
                    for (int i = 0; i < ret; i++) {
                        printf("%02X", cert[i]);
                    }
                    printf("\n");
                    hsk_print(E_LEVEL_INFO,"----END CERTIFICATE__----");
                    printf("\n\n\n\n\n\n");
                    hsk_print(E_LEVEL_INFO,"----BEGIN CERTIFICATE----");
                    for (int i = 0; i < ret; i++) {
                        printf("%c", cert[i]);
                    }
                    printf("\n");
                    hsk_print(E_LEVEL_INFO,"----END CERTIFICATE__----");
                }
                else
                {
                    hsk_print(E_LEVEL_ERROR, "Failed to get mfi cert");
                }
            }
            else
            {
                hsk_print(E_LEVEL_ERROR, "Invalid cmdid: 0x%08x", hdr.cmdid);
            }
        } else {
            hsk_print(E_LEVEL_ERROR, "Can't get MFI serial number");
        }
    }
    else
    {
        hsk_print(E_LEVEL_ERROR, "Can't read hdr");
    }

    mfi_auth_close(auth_handle);

}
